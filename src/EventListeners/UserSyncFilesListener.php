<?php
namespace App\EventListeners;

use App\Entity\User;
use App\Entity\Files;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\EntityManagerInterface;

class UserSyncFilesListener
{

    public function __construct(EntityManagerInterface $em){
        $this->em = $em;
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if (!$entity instanceof User) {
            return;
        }

        $token = $entity->getToken();
        if(!empty($token)){
            $user_files = $this->em->getRepository(Files::class)->getAllFilesByToken($token,get_class($entity));
            if(count($user_files) > 0){
                foreach($user_files as $file){
                    $file->setEntityId($entity->getId());
                    $file->setToken(null);
                }
            }
           
        }
        $entity->setToken(null);
        $this->em->flush();
       
    }
}