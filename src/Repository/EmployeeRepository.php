<?php

namespace App\Repository;

use App\Entity\Employee;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Doctrine\Common\Collections\Criteria;

/**
 * @method Employee|null find($id, $lockMode = null, $lockVersion = null)
 * @method Employee|null findOneBy(array $criteria, array $orderBy = null)
 * @method Employee[]    findAll()
 * @method Employee[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EmployeeRepository extends ServiceEntityRepository
{
    private $paginator;
    
    public function __construct(RegistryInterface $registry,PaginatorInterface $paginator)
    {
        $this->paginator = $paginator;
        parent::__construct($registry, Employee::class);
    }

    // /**
    //  * @return Employee[] Returns an array of Employee objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Employee
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function findAllPaginated(int $page = 1, int $perPage = 10){

        $query = $this->createQueryBuilder('e')
                    ->leftJoin("e.position",'p')
                    ->leftJoin("e.departments","d")
                    ->leftJoin("e.user","u")
                    ->addSelect("e","p","d","u")
                    ->orderBy("e.last_name","ASC")
                    ->getQuery();

        return $this->paginator->paginate($query,$page,$perPage);

    }

    // public function getAddressByType(Employee $emp,int $type){
    //     $em = $this->getEntityManager();
    //     $query = $em->createQuery('SELECT a FROM App:Employee e 
    //                                 INNER JOIN App:Address a WITH e.id = a.employee WHERE a.type = :type AND e.id = :emp_id')
    //                 ->setParameter("type",$type)
    //                 ->setParameter("emp_id",$emp->getId());
        
    //     return $query->getResult();
    // }
}
