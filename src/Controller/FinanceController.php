<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use App\Entity\Category;
use App\Service\ReportsHelper;
use Carbon\Carbon;
use App\Entity\Warehouse;
use App\Repository\WarehouseRepository;
use App\Service\ApiHelper;
use App\Repository\OrderRepository;
use App\Repository\ProductRepository;
use App\Entity\Finance;
use App\Repository\FinanceRepository;
use App\Form\FinanceFormType;
use AndreaSprega\Bundle\BreadcrumbBundle\Annotation\Breadcrumb;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @IsGranted("ROLE_ADMIN_FINANCE")
 */
class FinanceController extends AbstractController
{


     /**
     * @Route("/finance/{page}", name="finance", defaults={"page":"1"}, requirements={"page":"\d+"})
     */
    public function index(Request $request, 
                         FinanceRepository $financeRepo,
                         ValidatorInterface $validator,
                        $page
                        )
    {
        $filters = $this->getFiltersFromRequest($request,$validator);
      
        $finances = $financeRepo->findAllPaginated($page,$this->getParameter('default_per_page'),$filters);

        return $this->render('finance/index.html.twig', [
            'page_title' => 'Income / Expenses list',
            'finances'=>$finances
        ]);
    }

    /**
     * @Route("/finance/add", name="finance_add")
     * @Breadcrumb({"label" = "Income / Expense list", "route" = "finance" })
     */
    public function add(Request $request,
                        EntityManagerInterface $em,
                        $tr){

        try{
            $finance = new Finance();
            $form = $this->createForm(FinanceFormType::class,$finance);
           
            $form->handleRequest($request);
            $createdAt = $request->request->get("finance_form")['created_at']??null;

            
            if($form->isSubmitted() && $form->isValid()){
                
                if($createdAt){
                    $finance->setCreatedAt(new \DateTime($createdAt));
                }
                
                $em->persist($finance);
                $em->flush();

                $this->addFlash("success",$tr->trans("Finance saved"));
                return $this->redirectToRoute("finance");
            }
        }catch(\Exception $e){
           
            $this->addFlash("error",$e->getMessage());
            return $this->redirectToRoute("finance");
        }
        

        return $this->render('finance/add.html.twig',[
            'page_title'=>'Add income / expense',
            'form'=>$form->createView()
        ]);
    }

     /**
     * @Route("/finance/edit/{id}", name="finance_edit", requirements={"id":"\d+"})
     * @Breadcrumb({"label" = "Income / Expense list", "route" = "finance" })
     */
    public function edit(Request $request,
                        EntityManagerInterface $em,
                        Finance $finance,
                        $tr){

        try{
            
            $form = $this->createForm(FinanceFormType::class,$finance);

            $form->handleRequest($request);
            $createdAt = $request->request->get("finance_form")['created_at']??null;
            
            if($form->isSubmitted() && $form->isValid()){

                if($createdAt){
                    $finance->setCreatedAt(new \DateTime($createdAt));
                }
              
                $em->persist($finance);
                $em->flush();

                $this->addFlash("success",$tr->trans("Finance edited"));
                return $this->redirectToRoute("finance_edit",[
                    "id"=>$finance->getId()
                ]);
            }
        }catch(\Exception $e){
           
            $this->addFlash("error",$e->getMessage());
            return $this->redirectToRoute("finance");
        }
        

        return $this->render('finance/add.html.twig',[
            'page_title'=>'Add income / expense',
            'form'=>$form->createView(),
            'finance'=>$finance
        ]);
    }

  

    /**
     * @Route("/finance/delete/{id}", name="finance_delete", requirements={"id":"\d+"})
     */
    public function delete(Request $request,
                           EntityManagerInterface $em,
                           Finance $finance,
                           $tr
                           ){

        try{
            
            $em->remove($finance);
            $em->flush();

            $this->addFlash("success",$tr->trans("Finance deleted"));
            return $this->redirectToRoute("finance");

        }catch(\Exception $e){

            $this->addFlash("error",$e->getMessage());
            return $this->redirectToRoute("finance");
        }          

    }

    /**
     * @Route("/finance/reports", name="finance_reports")
     * @Breadcrumb({"label" = "Reports", "route" = "finance_reports" })
     */
    public function reports(
                            Request $request,
                            EntityManagerInterface $em,
                            ReportsHelper $reportsHelper,
                            WarehouseRepository $warehouseRepo,
                            ApiHelper $apiHelper,
                            OrderRepository $orderRepo,
                            ProductRepository $productRepo
                            )
    {

        $startOfMonth = Carbon::now()->startOfMonth();
        $endOfMonth = Carbon::now()->endOfMOnth();

        $startOfYear = Carbon::now()->startOfYear();
        $endOfYear = Carbon::now()->endOfYear();

        $validMonths = range(1,12);
        $validYears = range(2019,date("Y") + 10);
       
        $reportsHelper->incomeVsExpenseDetailed($startOfMonth,$endOfMonth);
        $apiToken = $apiHelper->getApiToken();

        return $this->render('finance/reports.html.twig', [
            'page_title' => 'Reports',
            'apiToken'=>$apiToken,
            'startOfMonth'=>$startOfMonth,
            'endOfMonth'=>$endOfMonth,
            'startOfYear'=>$startOfYear,
            'endOfYear'=>$endOfYear,
            'validMonths'=>$validMonths,
            'validYears'=>$validYears
     
        ]);
    }

        /**
     * @Route("/finance/filtersAjax", name="finance_filters_ajax")
     */
    public function filtersAjax(Request $request,
                                ValidatorInterface $validator){
        
        try{
            
            $data['success'] = true;
            $data['url'] = "";
                            
            $filters = $this->getFiltersFromRequest($request,$validator);
                                       
            $filters = array_filter($filters,function($elem){
                            return !is_null($elem) && $elem !== "";
                    });
                            
            $data['url'] = http_build_query($filters);
                                        
            }catch(\Exception $e){
                $data['success'] = false;
                $data['url'] = "";
            }                            
                                    
                            
            return $this->json($data);

    }

    private function getFiltersFromRequest(Request $request,ValidatorInterface $validator){

        $filters = [
            "name"=>$request->query->get("name",null),
            "type"=>$request->query->get("type",null),
            "recurrent"=>$request->query->get("recurrent",null),
            "s_date"=>$request->query->get("s_date",null),
            "e_date"=>$request->query->get("e_date",null)
        ];
        $em = $this->getDoctrine()->getManager();
       
        $rules['name'] = [
            new Assert\Type([
                "type"=>"string"
            ]),
            new Assert\Length([
                "min"=>2,
                "max"=>200
            ]),
        //    new Assert\GreaterThanOrEqual([
        //        "value"=>$firstNum
        //    ]),
        //    new Assert\LessThanOrEqual([
        //        "value"=>$lastNum
        //    ])
        ];
        $rules['type'] = [
            new Assert\Type([
                "type"=>"string"
            ]),
            new Assert\Length([
                "min"=>2,
                "max"=>200
            ]),
            new Assert\Choice([
                "choices"=>[
                    "income",
                    "expense"
                ]
            ])
        ];
        $rules['recurrent'] = [
            new Assert\Type([
                "type"=>"numeric"
            ]),
            new Assert\Choice([
                "choices"=>["0","1",0,1]
            ])
        ];
        
        $rules['s_date'] = [
            new Assert\DateTime([
                "format"=>"Y-m-d"
            ])
        ];
        $rules['e_date'] = [
            new Assert\DateTime([
                "format"=>"Y-m-d"
            ])
        ];
        $constraint = new Assert\Collection($rules);
        $violations = $validator->validate($filters,$constraint);
        if($violations->count() > 0){
            foreach($violations as $violation){
               $key = trim(str_replace(["[","]"],["",""],$violation->getPropertyPath()));
               if(isset($filters[$key])){
                   $filters[$key] = null;
               }
            }
        }

        if(empty($filters['s_date']) || empty($filters['e_date'])){
            $filters['s_date'] = null;
            $filters['e_date'] = null;
        }

        


        return $filters;
    }
}
