<?php

namespace App\Form;

use App\Entity\Address;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type as Type;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints as Assert;

class AddressFormType extends AbstractType
{

    private $tr;
    
    public function __construct($tr){
        $this->tr = $tr;
    }

  
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        $billing_type = !empty($options['data']) && $options['data']->getType() == Address::BILLING_TYPE;
        if($billing_type){
            $options['address_type'] = Address::BILLING_TYPE;
        }

        $constraints = array_combine(["street","number","postcode","city","country"],[[],[],[],[],[]]);
      
        
        if( in_array($options['address_type'],[Address::BILLING_TYPE]) ){
            $constraints['street'] = [
               
                new Assert\Length([
                    "min"=>2,
                    "max"=>200,
                    "minMessage" => "Your street must be at least {{ limit }} characters long",
                    "maxMessage" => "Your street cannot be longer than {{ limit }} characters"
                ])
            ];

            $constraints['number'] = [
                // new Assert\Regex([
                //     "pattern"=>"#\d+#"
                // ])
                
            ];

            $constraints['postcode'] = [
                
                new Assert\Length([
                    "min"=>2,
                    "max"=>10,
                    "minMessage" => "Your postcode must be at least {{ limit }} characters long",
                    "maxMessage" => "Your postcode cannot be longer than {{ limit }} characters"
                ])
            ];

            $constraints['city'] = [
               
                new Assert\Length([
                    "min"=>2,
                    "max"=>30,
                    "minMessage" => "Your city must be at least {{ limit }} characters long",
                    "maxMessage" => "Your city cannot be longer than {{ limit }} characters"
                ])
            ];

            $constraints['country'] = [
                
                new Assert\Length([
                    "min"=>2,
                    "max"=>30,
                    "minMessage" => "Your country must be at least {{ limit }} characters long",
                    "maxMessage" => "Your country cannot be longer than {{ limit }} characters"
                ])
            ];
            
        }else{
            $constraints['street'] = [
                new Assert\NotBlank(),
                new Assert\Length([
                    "min"=>2,
                    "max"=>200,
                    "minMessage" => "Your street must be at least {{ limit }} characters long",
                    "maxMessage" => "Your street cannot be longer than {{ limit }} characters"
                ])
            ];

            $constraints['number'] = [
                new Assert\NotBlank()
                
            ];

            $constraints['postcode'] = [
                new Assert\NotBlank(),
                new Assert\Length([
                    "min"=>2,
                    "max"=>10,
                    "minMessage" => "Your postcode must be at least {{ limit }} characters long",
                    "maxMessage" => "Your postcode cannot be longer than {{ limit }} characters"
                ])
            ];

            $constraints['city'] = [
                new Assert\NotBlank(),
                new Assert\Length([
                    "min"=>2,
                    "max"=>30,
                    "minMessage" => "Your city must be at least {{ limit }} characters long",
                    "maxMessage" => "Your city cannot be longer than {{ limit }} characters"
                ])
            ];

            $constraints['country'] = [
                new Assert\NotBlank(),
                new Assert\Length([
                    "min"=>2,
                    "max"=>30,
                    "minMessage" => "Your country must be at least {{ limit }} characters long",
                    "maxMessage" => "Your country cannot be longer than {{ limit }} characters"
                ])
            ];
        }
       
        $builder
            ->add('street',Type\TextareaType::class,[
                'label'=>$this->tr->trans('Street'),
                'required'=>true,
                'empty_data'=>'',
                'constraints'=>$constraints['street']
            ])
            ->add('number',Type\TextType::class,[
                'label'=>$this->tr->trans('Number'),
                'required'=>true,
                'empty_data'=>'',
                'constraints'=>$constraints['number']
            ])
            ->add('postcode',Type\TextType::class,[
                'label'=>$this->tr->trans('Postcode'),
                'required'=>true,
                'empty_data'=>'',
                'constraints'=>$constraints['postcode']
            ])
            ->add('city',Type\TextType::class,[
                'label'=>$this->tr->trans('City'),
                'required'=>true,
                'empty_data'=>'',
                'constraints'=>$constraints['city']
            ])
            ->add('country',Type\TextType::class,[
                'label'=>$this->tr->trans('Country'),
                'required'=>true,
                'empty_data'=>'',
                'constraints'=>$constraints['country']
            ])
            
            // ->add('type',Type\ChoiceType::class,[
            //     'required'=>true,
            //     'choices'=>[
            //         'Shipping address'=>Address::SHIPPING_TYPE,
            //         'Billing address'=>Address::BILLING_TYPE
            //     ]

            // ])
            
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults([
            'data_class' => Address::class,
            'address_type'=>Address::SHIPPING_TYPE
        ]);
    }

}
