<?php 

namespace App\Form\DataObjects;
use App\Validator\UniqueUser;
use Symfony\Component\Validator\Constraints as Assert;

class OrderItemMovement{

     
    public $fromWarehouse;

     /**
     * @Assert\NotNull(message="Please select a product")
     */
    public $product;

    /**
     * @Assert\NotBlank(message="Qty field is mandatory")
     * @Assert\Type(
     *     type="numeric",
     *     message="The qty field {{ value }} is not a valid {{ type }}."
     * )
     */
    public $qty;
}