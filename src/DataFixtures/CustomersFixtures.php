<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Customer;

class CustomersFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for($i = 0; $i < 2; $i++){
            $customer = new Customer();
            $customer->setFirstName("John {$i}");
            $customer->setLastName("Smith {$i}");
            $customer->setPhone("1323123");
            $customer->setEmail("test{$i}@tst.com"); 
            $customer->setIdentification("123213213");
            $customer->setIdCard("XC21321{$i}");
            $customer->setShippingAddress("asdsad asdsad asdasd");   

            $manager->persist($customer);
        }

        $manager->flush();
    }
}
