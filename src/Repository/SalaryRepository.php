<?php

namespace App\Repository;

use App\Entity\Salary;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Knp\Component\Pager\PaginatorInterface;
use Carbon\Carbon;

/**
 * @method Salary|null find($id, $lockMode = null, $lockVersion = null)
 * @method Salary|null findOneBy(array $criteria, array $orderBy = null)
 * @method Salary[]    findAll()
 * @method Salary[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SalaryRepository extends ServiceEntityRepository
{
    private $paginator;
    
    public function __construct(RegistryInterface $registry,PaginatorInterface $paginator)
    {
        $this->paginator = $paginator;
        parent::__construct($registry, Salary::class);
    }

    // /**
    //  * @return Salary[] Returns an array of Salary objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Salary
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function findAllPaginated(int $page = 1, int $perPage = 10){

        $query = $this->createQueryBuilder('s')
                    ->orderBy("s.date_created","DESC")
                    ->getQuery();

        return $this->paginator->paginate($query,$page,$perPage);

    }

    public function getSalariesPerPeriod(Carbon $start,Carbon $end){
        
        $qb = $this->createQueryBuilder('s');
        $qb->andWhere('s.date_created BETWEEN :start AND :end')
            ->setParameters([
                "start"=>$start->copy()->startOfMonth()->format("Y-m-d"),
                "end"=>$end->copy()->endOfMonth()->format("Y-m-d")
            ]);
        return $qb->orderBy("s.date_created","DESC")
                  ->getQuery()
                  ->getResult();

    }
}
