<?php

namespace App\Form;

use App\Entity\Invoice;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type as Type;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\CallbackTransformer;

class InvoiceFormType extends AbstractType
{
    private $tr;
    
    public function __construct($tr){
        $this->tr = $tr;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('series',Type\TextType::class,[
                'label'=>'Series',
                'empty_data'=>null,
                'help'=>$this->tr->trans('If left blank default invoice series will be used')
            ])
            ->add('due_date',Type\TextType::class,[
                'label'=>$this->tr->trans('Due date'),
                'mapped'=>false,
                'constraints'=>[
                    new Assert\NotBlank([
                        "message"=>$this->tr->trans("Please set a due date")
                    ]),
                    new Assert\Date([
                        "message"=>$this->tr->trans("Invalid date type")
                    ])
                ],
                'empty_data'=>null
                   
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Invoice::class,
        ]);
    }
}
