<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Warehouse;

class WarehousesFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for($i = 0;$i < 3;$i++){
            $warehouse = new Warehouse();
            $warehouse->setName("Warehouse {$i}");
            $warehouse->setDetails("dasdasd");
            $warehouse->setAddress("asdsadsad sadsad asdsad");

            $manager->persist($warehouse);
        }

        $manager->flush();
    }
}
