<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Form\DataObjects\StockMovement;
use Symfony\Component\Form\Extension\Core\Type as Type;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Entity\Warehouse;
use App\Repository\ProductRepository;
use App\Entity\Product;
use Doctrine\ORM\EntityRepository;

class StockMovementFormType extends AbstractType
{
    private $tr;
    
    public function __construct($tr){
        $this->tr = $tr;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('qty',Type\TextType::class,[
                'label'=>$this->tr->trans('Qty to move')
            ])
            ->add('fromWarehouse',EntityType::class,[
                'class'=>Warehouse::class,
                'choice_label'=>'name',
                'placeholder'=>$this->tr->trans("From warehouse"),
                'label'=>$this->tr->trans('From warehouse'),
                'help'=>$this->tr->trans('Leave empty for moving from not assigned qty'),
                'empty_data'=>null,
                'query_builder'=>function(EntityRepository $er){
                       
                        return $er->createQueryBuilder("w")
                                 ->andWhere("w.deleted_at IS NULL");
                                 
                                 
                                  
                    }
                  
            ])
            ->add('toWarehouse',EntityType::class,[
                'class'=>Warehouse::class,
                'choice_label'=>'name',
                'placeholder'=>$this->tr->trans("To warehouse"),
                'label'=>$this->tr->trans('To warehouse'),
                'query_builder'=>function(EntityRepository $er){
                       
                    return $er->createQueryBuilder("w")
                             ->andWhere("w.deleted_at IS NULL");
                             
                             
                              
                }
                  
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => StockMovement::class,
        ]);
    }
}
