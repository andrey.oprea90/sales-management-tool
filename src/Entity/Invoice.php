<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\InvoiceRepository")
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity(
 *     fields={"number"},
 *     message="Order number already exists"
 * )
 */
class Invoice
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Type(
     *     type="numeric",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    private $number;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $due_date;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default":0})
     */
    private $partial;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, nullable=true, options={"default":0})
     */
    private $total_paid;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Order", inversedBy="invoices")
     * @ORM\JoinColumn(nullable=false)
     */
    private $ord;

    /**
     * @ORM\Column(type="string", length=255, options={"default":"INV"})
     * @Assert\Type(
     *     type="alpha",
     *     message="Please use only letters."
     * )
     */
    private $series;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumber(): ?string
    {
        return $this->number;
    }

    public function setNumber(string $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(?\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getDueDate(): ?\DateTimeInterface
    {
        return $this->due_date;
    }

    public function setDueDate(?\DateTimeInterface $due_date): self
    {
        $this->due_date = $due_date;

        return $this;
    }

    public function getPartial(): ?bool
    {
        return $this->partial;
    }

    public function setPartial(?bool $partial): self
    {
        $this->partial = $partial;

        return $this;
    }

    public function getTotalPaid()
    {
        return $this->total_paid;
    }

    public function setTotalPaid($total_paid): self
    {
        $this->total_paid = $total_paid;

        return $this;
    }

    public function getOrd(): ?Order
    {
        return $this->ord;
    }

    public function setOrd(?Order $ord): self
    {
        $this->ord = $ord;

        return $this;
    }

    public function getSeries(): ?string
    {
        return $this->series;
    }

    public function setSeries(string $series): self
    {
        $this->series = $series;

        return $this;
    }

     /**
     * @ORM\PrePersist
     */
    public function setDefaultValues(){

        $this->date_created = new \DateTime();

        if(!$this->due_date){
            $this->due_date = new \DateTime("+1 Month");
        }

        if(!$this->total_paid){
            $this->total_paid = 0;
        }

        if(!$this->partial){
            $this->partial = false;
        }

        if($this->series){
            $this->series = strtoupper(preg_replace("#[^a-zA-Z]#","",$this->series));
        }

    }
}
