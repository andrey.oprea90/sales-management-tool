<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use App\Repository\AclRoleRepository;
use AndreaSprega\Bundle\BreadcrumbBundle\Annotation\Breadcrumb;

/**
 * @IsGranted("ROLE_ADMIN_ACL")
 */
class AclController extends AbstractController
{
    /**
     * @Route("/permissions/{page}", name="permissions", methods="GET", defaults={"page":"1"})
     */
    public function index(
        Request $request,
        EntityManagerInterface $em,
        AclRoleRepository $aclRoleRepo,
        $page
        )
    {
        $roles = $aclRoleRepo->findAllPaginated($page,$this->getParameter("default_per_page"));

        return $this->render('acl/index.html.twig', [
            'page_title' => 'Acl api roles',
            'roles'=>$roles
        ]);
    }
}
