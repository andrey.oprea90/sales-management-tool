# Smart Sales Management

# Short description

* Simple sales management tool built in Symfony 4.2.
* You can manage users, employees, customers, products, warehouses, stocks, sales, income, expenses, make orders, generate reports, invoices and much more.
* Free to use.

# Requirements
* General Symfony 4.2 requirements - `https://symfony.com/doc/4.2/reference/requirements.html`
* If you have database that doesn't support native json type yet (MySql < 5.7, some versions of MariaDb) edit <project_dir>/config/packages/doctrine.yaml and under the
* doctrine - dbal - server_version variable add your db version: (for example server_version: 'mariadb-10.1.38')

# Install Instructions

* 1.Download code.
* 2.Edit the .env file and edit the database credentials according to official Symfony documentation.
* 3.Run composer install
* 4.Create database with command php bin/console doctrine:database:create
* 5.Execute migrations with command php bin/console doctrine:migrations:migrate
* 6.Load fixtures with command php bin/console doctrine:fixtures:load.
* 7.For admin user use the following credentials:
        * Username: admin_user
        * Password: testpass
* Optional:
* 8.Execute the following commands to generate the public/private ceritifcates for jwt auth
* openssl genrsa -out config/jwt/private.pem -aes256 4096
* openssl rsa -pubout -in config/jwt/private.pem -out config/jwt/public.pem
* 9.If you want the token verification to be enabled please edit config/packages/security.yaml and uncomment the commented section under firewalls
* If you want a demo database please skip steps 4,5,6 and just import the db_bck_demo_02_07_2019.sql
* Enjoy.


