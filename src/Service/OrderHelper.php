<?php 

namespace App\Service;

use App\Entity\Product;
use App\Entity\WarehouseProduct;
use App\Entity\Warehouse;
use App\Repository\ProductRepository;
use App\Repository\WarehouseProductRepository;
use App\Repository\WarehouseRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Order;
use App\Entity\OrderItem;
use App\Repository\OrderRepository;
use App\Repository\OrderItemRepository;
use App\Service\StockMovementHelper;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use App\Repository\SettingRepository;
use App\Entity\Client;
use App\Entity\Customer;
use App\Repository\InvoiceRepository;
use App\Entity\Invoice;

class OrderHelper{

    const STARTING_NUMBER = 10000;
    const STARTING_INVOICE_NUMBER = 100;

    private $em;

    private $stockHelper;

    private $orderRepo;

    private $orderItemRepo;

    private $params;

    private $setting;

    private $invoiceRepo;

    private $tr;

    public function __construct(EntityManagerInterface $em,
                                StockMovementHelper $stockHelper,
                                OrderRepository $orderRepo,
                                OrderItemRepository $orderItemRepo,
                                ParameterBagInterface $params,
                                SettingRepository $settingRepository,
                                InvoiceRepository $invoiceRepository,
                                $tr
                                ){

        $this->em = $em;
        $this->stockHelper = $stockHelper;
        $this->orderRepo = $orderRepo;
        $this->orderItemRepo = $orderItemRepo;
        $this->params = $params;
        $this->setting = $settingRepository->getFirstOrNull();
        $this->invoiceRepo = $invoiceRepository;
        $this->tr = $tr;                            

    }


    public function createOrderItem(Order $order,Product $product,?Warehouse $warehouse = null):OrderItem{
        $orderItem = new OrderItem();
        $orderItem->setProduct($product);
        $orderItem->setWarehouse($warehouse);
        $orderItem->setProductName($product->getTitle());
        $orderItem->setProductSku($product->getSku());
        $orderItem->setOrd($order);
        $order->addItem($orderItem);

        return $orderItem;

    }

    public function getOrderItemOrNew(Order $order,Product $product,?Warehouse $warehouse = null):OrderItem{
        $item = $this->orderItemRepo->getItemOrNull($order,$product,$warehouse);
        if(!$item){
           $item = $this->createOrderItem($order,$product,$warehouse);
           $this->em->persist($item);
           $this->em->flush();
        }
      
        return $item;
    }

    public function resetOrderItem(OrderItem $orderItem,?int $newQty = null):OrderItem{
        if($orderItem->getCanceled()){
            return $orderItem;
        }
       
        $product = $orderItem->getProduct();
        $oldQty = $orderItem->getQtyOrdered();

        if(!$product){
            throw new \Exception($this->tr->trans("Fatal error, missing product"));
        }
        $checkExisting = !is_null($newQty) && is_numeric($newQty);
        $warehouse = $orderItem->getWarehouse();

        if($checkExisting){
            $this->stockHelper->setProduct($product);
            $sData = $this->stockHelper->getQtyPerWarehouseData($warehouse);
            $available = 0;
            if($warehouse){
               $available = $sData['total_in_current'] + $oldQty;
            }else{
                $available = $sData['total_on_prod'] + $oldQty;
            }
            if($available < $newQty){
                throw new \Exception($this->tr->trans("Available qty to move")." "."{$available}."."".$this->tr->trans("You tried to move")." {$newQty}");
            }
        }

        $product->setQty($product->getQty()+$oldQty);
        $this->em->flush();

       
        if($warehouse){
            $this->stockHelper->setProduct($product);
            $this->stockHelper->moveStock(null,$warehouse,$oldQty);
        }

        $orderItem->setTaxPercent(0);
        $orderItem->setQtyOrdered(0);
        $orderItem->setPrice(0);
        $orderItem->setTotal(0);
        $orderItem->setPriceInclTax(0);
        $orderItem->setTotalInclTax(0);

        $this->em->flush();

        return $orderItem;
    }

    public function cancelOrderItem(OrderItem $orderItem):OrderItem{

        if($orderItem->getCanceled()){
            return $orderItem;
        }
        
        $product = $orderItem->getProduct();
        $oldQty = $orderItem->getQtyOrdered();

        if(!$product){
            throw new \Exception($this->tr->trans("Fatal error, missing product"));
        }

        $warehouse = $orderItem->getWarehouse();

        $product->setQty($product->getQty()+$oldQty);
        $this->em->flush();

       
        if($warehouse){
            $this->stockHelper->setProduct($product);
            $this->stockHelper->moveStock(null,$warehouse,$oldQty);
        }

        $orderItem->setCanceled(1);
        $this->em->flush();

        return $orderItem;
    }

    public function updateOrderTotals(Order $order):Order{
       if($order->getStatus() == Order::STATUS_CANCELED){
           return $order;
       } 
       $items = $order->getItems();
       $invoices = $order->getInvoices();
       if($items->count() < 1){
         $order->setTotalExclTax(0);
         $order->setTax(0);
         $order->setTotalInclTax(0);
         $order->setTotalBuy(0);

         
       }else{
           $tax = 0;
           $totalInclTax = 0;
           $totalExclTax = 0;
           $totalBuyPrice = 0;
           foreach($items as $item){
               $product = $item->getProduct();
               $buyPrice = $product->getBuyPrice()?$product->getBuyPrice():0;
               $itemTax = $item->getTotalInclTax() - $item->getTotal();
               $tax+=$itemTax;
               $totalInclTax+=$item->getTotalInclTax();
               $totalExclTax+=$item->getTotal();
               $totalBuyPrice+=$buyPrice * $item->getQtyOrdered();
           }
           $order->setTotalExclTax($totalExclTax);
           $order->setTotalInclTax($totalInclTax);
           $order->setTax($tax);
           $order->setTotalBuy($totalBuyPrice);

           
       }

       foreach($invoices as $invoice){
           $invoice->setTotalPaid($order->getTotalInclTax());
       }

       $this->em->flush();

      return $order;
    }

    public function populateOrderItem(OrderItem $orderItem,$qty = 0,$reset = false):OrderItem{
        if($orderItem->getCanceled()){
            return $orderItem;
        }
        $product = $orderItem->getProduct();

        $price = $product->getSpecialPrice() > 0?$product->getSpecialPrice():$product->getPrice();
        $taxPercent = 0;
        if($product->getTax() && false){
            $taxPercent = $product->getTax()->getPercent();

        }else{
            if($this->setting){
               if(is_numeric($this->setting->getStandardTaxRate())){
                   $taxPercent = $this->setting->getStandardTaxRate();
                  
               }
            }
        }
        if($taxPercent == 0){
            $taxPercent = $this->params->get("default_tax_percent");
        }

        if(!$reset){
            $product->setQty($product->getQty() - $qty);
        } 
        $orderItem->setTaxPercent($taxPercent);
        $orderItem->setQtyOrdered($qty);
        $orderItem->setPrice($price);
        $orderItem->setTotal( ($price * $qty) );
        $orderItem->setPriceInclTax( ($price * (1 + ($taxPercent/100))) );
        $orderItem->setTotalInclTax( ($orderItem->getTotal() * (1 + ($taxPercent/100))) );
        $this->em->flush();

        return $orderItem;
    }

    public function getResetData(OrderItem $orderItem,array $stocksData,?Warehouse $warehouse = null):array{
      try{
        $result = $stocksData;
        $result['warehouse'] = $warehouse;
        $result['order_item_qty'] = $orderItem->getQtyOrdered();
        return $result;
      }catch(\Throwable $e){
          
          return [

          ];
      }

    }

    public function resetData(OrderItem $orderItem,array $resetData):bool{
        if($orderItem->getCanceled()){
            return false;
        }
        try{
             $totalOnProd = $resetData['total_in_warehouse'] + $resetData['total_on_prod'];
             $product = $orderItem->getProduct();
             $product->setQty($totalOnProd);
            
             $warehouse = $orderItem->getWarehouse();
             if($warehouse){
                 $oldStock = $resetData['warehouses'][$warehouse->getId()] ?? 0;
                 $this->stockHelper->setProduct($product);
                 $this->stockHelper->deleteStockFromWarehouse($warehouse);
                 if($oldStock > 0){
                     $this->stockHelper->moveStock(null,$warehouse,$oldStock);
                 }
             }
             
             $this->populateOrderItem($orderItem,$resetData['order_item_qty'],true);
             $this->updateOrderTotals($orderItem->getOrd());
             return true;
          }catch(\Throwable $e){
            
             return false;
          
          }
}

 
    public function updateOrderItem(OrderItem $orderItem,int $qty):OrderItem{
        if($orderItem->getOrd()->getStatus() == Order::STATUS_CANCELED){
            throw new \Exception($this->tr->trans("Cannot update a canceled order"));
        }
        if($orderItem->getCanceled()){
            throw new \Exception($this->tr->trans("Cannot update a canceled order item"));
        }

        if($qty < 1){
            return $orderItem;
        }
        $product = $orderItem->getProduct();

        if(!$product){
            throw new \Exception($this->tr->trans("Fatal error, missing product"));
        }

        $warehouse = $orderItem->getWarehouse();

        $this->stockHelper->setProduct($product);
        $stocksData = $this->stockHelper->getQtyPerWarehouseData($warehouse);
      
        $resetData = $this->getResetData($orderItem,$stocksData,$warehouse);
        $this->em->transactional(function($em) use(
            $orderItem,
            $qty,
            $warehouse,
            $stocksData,
            $resetData,
            $product)
        {
            if($orderItem->getQtyOrdered() > 0){
                $orderItem = $this->resetOrderItem($orderItem,$qty);
             }
             $stocksData = $this->stockHelper->getQtyPerWarehouseData($warehouse);
             if(!$warehouse){
                 
                 if($stocksData['total_on_prod'] < $qty){
                    
                     if(!empty($resetData)){
                         $this->resetData($orderItem,$resetData);
                     }
                     throw new \Exception($this->tr->trans("Available qty to move")." {$stocksData['total_on_prod']}.".$this->tr->trans("You tried to move")." {$qty}");
                 }
             }
             if($warehouse){
                 $availableInWarehouse = $stocksData['total_in_current'];
                 if($availableInWarehouse < $qty){
                     throw new \Exception($this->tr->trans("Available qty to move")." {$availableInWarehouse}.".$this->tr->trans("You tried to move")." {$qty}");
                 }
                 $this->stockHelper->removeStockFromWarehouse($warehouse,$qty);
             }else{
                 // $available = $stocksData['total_on_prod'] + $qty;
     
                 // if($available < $qty){
                 //     if(!empty($resetData)){
                 //         $this->resetData($orderItem,$resetData);
                 //     }
                 //     throw new \Exception("Available qty to move {$available}.You tried to move {$qty}");
                 // }
             }
            
             
           
             $this->populateOrderItem($orderItem,$qty);
             $order = $this->updateOrderTotals($orderItem->getOrd());
             $emptyItems = $this->orderItemRepo->getEmptyItems($order);
             
             foreach($emptyItems as $ei){
                 $em->remove($ei);
             }
             $em->flush();
     
            
        });

        return $orderItem;
    }

    public function removeOrderItem(OrderItem $orderItem):bool{
        $order = $orderItem->getOrd();

        if(!$orderItem->getCanceled()){
            $this->resetOrderItem($orderItem);
            $this->updateOrderTotals($order);
        }
        
        $this->em->remove($orderItem);
        $this->em->flush();

       return true;
    }

    public function cancelOrder(Order $order):bool{

        
        foreach($order->getItems() as $item){
           $this->cancelOrderItem($item);
        }
        $order->setStatus(Order::STATUS_CANCELED);
        $this->em->flush();

        return true;

    }

    public function generateUniqueOrderNumber():string{
        $newNumber = (string) static::STARTING_NUMBER;
        $lastNumber = $this->orderRepo->getLastOrderNumber();
       
        if($lastNumber){
            $newNumber = $lastNumber->getNumber();
        }
        $newNumber+=1;
        $checkExisting = $this->orderRepo->getOrderByNumber((string) $newNumber);
        while($checkExisting){
            $newNumber+=1;
            $checkExisting = $this->orderRepo->getOrderByNumber((string) $newNumber);
        }
        return $newNumber;
    }

    public function generateUniqueInvoiceNumber():string{
        $newNumber = (string) static::STARTING_INVOICE_NUMBER;
        $lastNumber = $this->invoiceRepo->getLastInvoiceNumber();
       
        if($lastNumber){
            $newNumber = $lastNumber->getNumber();
        }
        $newNumber+=1;
        $checkExisting = $this->invoiceRepo->getInvoiceByNumber((string) $newNumber);
        while($checkExisting){
            $newNumber+=1;
            $checkExisting = $this->invoiceRepo->getInvoiceByNumber((string) $newNumber);
        }
        return $newNumber;
    }

    public function generateInvoiceData(Order $order,Invoice $invoice):array{
        if(!in_array($order->getStatus(),["complete"])){
            throw new \Exception($this->tr->trans("Cannot generate invoices for orders that are not complete"));
        }
        $data = [
            "company"=>[
                "name"=>"",
                "address"=>"",
                "email"=>"",
                "phone"=>"",
                "vat"=>""
            ],
            "customer"=>[
                "name"=>"",
                "address"=>"",
                "vat"=>"",
                "email"=>"",
                "phone"
            ],
            "invoice"=>[
                "number"=>"",
                "created_at"=>"",
                "due_date"=>""
            ],
            "items"=>[

            ],
            "order"=>[
                "tax"=>0,
                "total_incl_tax"=>0,
                "total_excl_tax"=>0
            ]
        ];
        $validItems = $this->orderItemRepo->getInvoiceItems($order);
        $client = $order->getClient();
        $customer = $order->getCustomer();

        if(!$client && !$customer){
            throw new \Exception($this->tr->trans("Missing customer"));
        }

        $cl = $client ?? $customer;

        $data['customer'] = $cl->getInvoiceData();
        if($this->setting){
            $data['company'] = $this->setting->getInvoiceData();
        }

       $data['invoice']['number'] = $invoice->getSeries()." ".$invoice->getNumber()."-1";
       $data['invoice']['created_at'] = (new \DateTime())->format("d.m.Y");
       $data['invoice']['due_date'] = (new \DateTime("+1 Month"))->format("d.m.Y");

    //    if(empty($validItems)){
    //        throw new \Exception("No valid items to invoice");
    //    }
       foreach($validItems as $item){
         
        $data['items'][$item->getProduct()->getId()] = empty($data['items'][$item->getProduct()->getId()])?
            [
                "name"=>$item->getProductName(),
                "qty_ordered"=>0,
                "price_per_item"=>$item->getPriceInclTax(),
                "total"=>0
            ]:$data['items'][$item->getProduct()->getId()];

           $data['items'][$item->getProduct()->getId()]['qty_ordered']+=$item->getQtyOrdered();
           $data['items'][$item->getProduct()->getId()]['total']+=$item->getTotalInclTax();

       }
       $data['order']['tax'] = $order->getTax();
       $data['order']['total_incl_tax'] = $order->getTotalInclTax();
       $data['order']['total_excl_tax'] = $order->getTotalExclTax();
        
        return $data;
    }

}