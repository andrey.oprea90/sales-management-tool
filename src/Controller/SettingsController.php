<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use App\Entity\Setting;
use App\Repository\SettingRepository;
use App\Form\SettingsFormType;
use AndreaSprega\Bundle\BreadcrumbBundle\Annotation\Breadcrumb;

/**
 * @IsGranted("ROLE_ADMIN_SETTINGS")
 */
class SettingsController extends AbstractController
{
    /**
     * @Route("/settings", name="settings")
     */
    public function index(Request $request,
                         EntityManagerInterface $em,
                         SettingRepository $settingRepo,
                         $tr
                         )
    {
       try{

            $setting = $settingRepo->getFirstOrNew();
            
            $form = $this->createForm(SettingsFormType::class,$setting);
            $form->handleRequest($request);

            if($form->isSubmitted() && $form->isValid()){

                $em->persist($setting);
                $em->flush();

                $this->addFlash("success",$tr->trans("Settings saved"));

            }

            return $this->render('settings/index.html.twig', [
                'page' => 'Settings',
                'setting'=>$setting,
                'form'=>$form->createView()
            ]);
       }catch(\Exception $e){

           $this->addFlash("error",$e->getMessage());
           return $this->redirectToRoute("dashboard");
       }
        
    }
}
