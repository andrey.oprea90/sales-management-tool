<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use AndreaSprega\Bundle\BreadcrumbBundle\Annotation\Breadcrumb;
use App\Entity\AttributeSet;
use App\Repository\AttributeSetRepository;
use App\Entity\Attribute;
use App\Repository\AttributeRepository;
use App\Form\AttributeSetFormType;
use App\Repository\ProductRepository;


/**
 * @IsGranted("ROLE_ADMIN_ATTRIBUTES")
 * @Breadcrumb({"label" = "Attribute sets", "route" = "attribute_set" })
 */
class AttributeSetController extends AbstractController
{
    /**
     * @Route("/attribute-set/{page}", name="attribute_set", defaults={"page":"1"}, requirements={"page":"\d+"})
     */
    public function index(Request $request,
                         EntityManagerInterface $em,
                         AttributeSetRepository $attrSetRepo,
                         $page   
                         )
    {
        $attrSets = $attrSetRepo->findAllPaginated($page,$this->getParameter('default_per_page'));
        return $this->render('attribute_set/index.html.twig', [
            'page_title' => 'Attribute Sets',
            'attrSets'=>$attrSets
        ]);
    }

     /**
     * @Route("/attribute-set/add", name="attribute_set_add")
     */
    public function add(Request $request,
                        EntityManagerInterface $em,
                        $tr){

        try{
            $attributeSet = new AttributeSet();
            $form = $this->createForm(AttributeSetFormType::class,$attributeSet);

            $form->handleRequest($request);

            if($form->isSubmitted() && $form->isValid()){

                $em->persist($attributeSet);
                $em->flush();

                $this->addFlash("success",$tr->trans("Attribute set saved"));
                return $this->redirectToRoute("attribute_set_edit",[
                    "id"=>$attributeSet->getId()
                ]);
            }
        }catch(\Exception $e){
           
            $this->addFlash("error",$e->getMessage());
            return $this->redirectToRoute("attribute_set");
        }
        

        return $this->render('attribute_set/add.html.twig',[
            'page_title'=>'Add attribute set',
            'form'=>$form->createView()
        ]);
    }

    /**
     * @Route("/attribute-set/edit/{id}", name="attribute_set_edit", requirements={"id":"\d+"})
     */
    public function edit(Request $request,
                        EntityManagerInterface $em,
                        AttributeSet $attributeSet,
                        $tr){

        try{
           
            $form = $this->createForm(AttributeSetFormType::class,$attributeSet);
            $oldAttributes = $attributeSet->getAttributes()->toArray();
          
            $form->handleRequest($request);

            if($form->isSubmitted() && $form->isValid()){
                $em->transactional(function($em) use ($attributeSet,$oldAttributes){
                   foreach($oldAttributes as $oldAttr){
                       $oldAttr->removeAttributeSet($attributeSet);
                   }
                   $attributes = $attributeSet->getAttributes();
                    if($attributes->count()){
                        foreach($attributes as $attr){
                            $attr->addAttributeSet($attributeSet);
                        }
                    }

                    $em->persist($attributeSet);
                    $em->flush();
                });
                

                $this->addFlash("success",$tr->trans("Attribute set edited"));
                return $this->redirectToRoute("attribute_set_edit",[
                    "id"=>$attributeSet->getId()
                ]);
            }
        }catch(\Exception $e){
           
            $this->addFlash("error",$e->getMessage());
            return $this->redirectToRoute("attribute_set");
        }
        

        return $this->render('attribute_set/edit.html.twig',[
            'page_title'=>'Edit attribute set',
            'form'=>$form->createView()
        ]);
    }

    /**
     * @Route("/attribute-set/delete/{id}", name="attribute_set_delete", requirements={"id":"\d+"})
     */
    public function delete(Request $request,
                           EntityManagerInterface $em,
                           AttributeSet $attributeSet,
                           ProductRepository $productRepo,
                           $tr
                           ){

        try{
            $attachedProducts = $productRepo->findProductsByAttributeSet($attributeSet);
           
            foreach($attachedProducts as $attachedPr){
                $attachedPr->setAttributeSet(null);
            }
            $em->remove($attributeSet);
            $em->flush();

            $this->addFlash("success",$tr->trans("Attribute set deleted"));
            return $this->redirectToRoute("attribute_set");

        }catch(\Exception $e){

            $this->addFlash("error",$e->getMessage());
            return $this->redirectToRoute("attribute_set");
        }          

    }
}
