<?php

namespace App\Repository;

use App\Entity\Category;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @method Category|null find($id, $lockMode = null, $lockVersion = null)
 * @method Category|null findOneBy(array $criteria, array $orderBy = null)
 * @method Category[]    findAll()
 * @method Category[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoryRepository extends ServiceEntityRepository
{
    private $paginator;
    
    public function __construct(RegistryInterface $registry,PaginatorInterface $paginator)
    {
        $this->paginator = $paginator;
        parent::__construct($registry, Category::class);
    }

    // /**
    //  * @return Category[] Returns an array of Category objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Category
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function findAllPaginated(int $page = 1, int $perPage = 10){

        $query = $this->createQueryBuilder('c')
                    ->orderBy("c.created_at","DESC")
                    ->getQuery();

        return $this->paginator->paginate($query,$page,$perPage);

    }

    public function ajaxCategories(int $page = 1,int $perPage = 10,string $q = ''){
        $qb = $this->createQueryBuilder('c')
                    ->orderBy('c.created_at','DESC');

        if(!empty($q)){
            $qb->andWhere('c.name LIKE :q');
            $qb->setParameter("q","%".$q."%");
        }
        
        $query = $qb->getQuery();

        return $this->paginator->paginate($query,$page,$perPage);
    }
}
