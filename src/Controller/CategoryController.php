<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use App\Entity\Category;
use App\Repository\CategoryRepository;
use App\Form\CategoryFormType;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Filesystem\Filesystem;
use AndreaSprega\Bundle\BreadcrumbBundle\Annotation\Breadcrumb;

/**
 * @IsGranted("ROLE_ADMIN_CATEGORIES")
 * @Breadcrumb({"label" = "Categories", "route" = "categories" })
 */
class CategoryController extends AbstractController
{
    /**
     * @Route("/categories/{page}", name="categories", defaults={"page":"1"}, requirements={"page":"\d+"})
     */
    public function index(Request $request,
                          EntityManagerInterface $em,
                          CategoryRepository $categoryRepo,
                          $page
                          )
    {
        $categories = $categoryRepo->findAllPaginated($page,$this->getParameter('default_per_page'));
        $uploadsDir = $this->getParameter('default_category_upload_dir');
        return $this->render('category/index.html.twig', [
            'page_title' => 'Categories',
            'categories'=>$categories,
            'uploadsDir'=>$uploadsDir
        ]);
    }

    /**
     * @Route("/categories/ajax", name="categories_ajax")
     */
    public function ajax(Request $request,
                          EntityManagerInterface $em,
                          CategoryRepository $categoryRepo,
                          $tr
                          )
    {
        $resp_data = [
            'results'=>[

            ],
            'more'=>false
        ];

        $page = $request->query->get("page",1);
        $pageLimit = $request->query->get("page_limit",$this->getParameter('default_per_page'));
        $q = $request->query->get("q",'');
        $categories = $categoryRepo->ajaxCategories($page,$pageLimit,$q);

        $totalPages = $categories->getPageCount();
        $currentPage = $categories->getPage();

        $more = ((int) $currentPage) < $totalPages;

        foreach($categories as $category){
            $resp_data['results'][]=[
                'id'=>$category->getId(),
                'text'=>$category->getName()
            ];
            $resp_data['more'] = $more;
        }
        return $this->json($resp_data);
    }

    /**
     * @Route("/categories/add", name="categories_add")
     */
    public function add(Request $request,
                        EntityManagerInterface $em,
                        $tr)
    {
        try{
           
            $category = new Category();
            
            $form = $this->createForm(CategoryFormType::class,$category);
            
            $form->handleRequest($request);

            if($form->isSubmitted() && $form->isValid()){
                $file = $category->getMainImage();
               
                $categoryUploadPath = $this->getParameter("default_category_upload_dir");

                if($file instanceof UploadedFile){
                   $newName = md5(uniqid()).".".$file->guessExtension();
                   $file->move(
                    $categoryUploadPath,
                    $newName
                   );

                   $category->setMainImage($newName);
                }
               
                
                $em->persist($category);
                $em->flush();

                $this->addFlash("success",$tr->trans("Category saved"));
                return $this->redirectToRoute("categories");

            }

            return $this->render('category/add.html.twig',[
                'page_title'=>'Add category',
                'form'=>$form->createView()
                
            ]);

        }catch(\Exception $e){
            
            $this->addFlash("error",$e->getMessage());

            return $this->redirectToRoute("categories");
        }
        
    }

    /**
     * @Route("/categories/edit/{id}", name="categories_edit", requirements={"id":"\d+"})
     */
    public function edit(Request $request,
                        EntityManagerInterface $em,
                        Category $category,
                        $tr
                        )
    {
        try{
           
            
            $form = $this->createForm(CategoryFormType::class,$category);
            $imageName = $category->getMainImage();
            $form->handleRequest($request);

            if($form->isSubmitted() && $form->isValid()){
                $file = $category->getMainImage();
                $newName = $imageName;
                $categoryUploadPath = $this->getParameter("default_category_upload_dir");

                if($file instanceof UploadedFile){
                   $newName = md5(uniqid()).".".$file->guessExtension();
                   $file->move(
                    $categoryUploadPath,
                    $newName
                   );

                  
                }

                $category->setMainImage($newName);
               
               
                $em->flush();

                $this->addFlash("success",$tr->trans("Category edited"));
                return $this->redirectToRoute("categories_edit",[
                    'id'=>$category->getId()
                ]);

            }

            return $this->render('category/edit.html.twig',[
                'page_title'=>'Edit category',
                'form'=>$form->createView(),
                'category'=>$category
                
            ]);

        }catch(\Exception $e){
            
            $this->addFlash("error",$e->getMessage());

            return $this->redirectToRoute("categories");
        }
    }

    /**
     * @Route("/categories/delete/{id}", name="categories_delete", requirements={"id":"\d+"})
     */
    public function delete(Request $request,
                        EntityManagerInterface $em,
                        Category $category,
                        Filesystem $filesystem,
                        $tr
                        )
    {
        try{
            
            $uploadsDir = $this->getParameter('default_category_upload_dir');
            $filePath = null;
            if($category->getMainImage()){
                $filePath = $uploadsDir.DIRECTORY_SEPARATOR.$category->getMainImage();
               
            }
            $em->remove($category);
            $em->flush();

            if($filesystem->exists($filePath)){
                $filesystem->remove($filePath);
            }

            $this->addFlash("success",$tr->trans("Category deleted"));
            return $this->redirectToRoute('categories');
        }catch(\Exception $e){
            
            $this->addFlash("error",$e->getMessage());

            return $this->redirectToRoute("categories");
        }
        
    }
}
