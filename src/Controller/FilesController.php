<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\Constraints as Assert;
use App\Service\FileHelper;
use Symfony\Component\HttpFoundation\File\File;
use App\Entity\Files;
use App\Repository\FilesRepository;
use Intervention\Image\ImageManager;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @IsGranted("ROLE_ADMIN_FILES")
 */
class FilesController extends AbstractController
{
    /**
     * @Route("/files/upload", name="files_upload")
     */
    public function upload(
                           Request $request,
                           ValidatorInterface $validator,
                           EntityManagerInterface $em,
                           $tr
                           )
    {

        try{

           
            $entity_id = filter_var($request->request->get('entity_id',0),FILTER_VALIDATE_INT);
            if(!$entity_id){
                $entity_id = 0;
            }
           
            $entity_type = $request->request->get('entity_type',null);
           
            if(!class_exists($entity_type)){
                throw new \Exception();
            }
            
            
            $entity_type_dir = basename(str_replace("\\",DIRECTORY_SEPARATOR,$entity_type));
           
            $token = $request->request->get('token',null);
            $file = $request->files->get('file',null);
           
            $inputs = [
               'entity_id'=>$entity_id,
               'entity_type'=>$entity_type,
               'token'=>$token,
               'file'=>$file
            ];
    
            $rules = [];
           
            
            if((int) $entity_id < 1){
                $rules['token'] = [
                    new Assert\NotBlank(),
                    new Assert\Length(["min"=>16])
                ];
                $rules['entity_type'] = new Assert\NotBlank();
                $rules['entity_id'] = new Assert\EqualTo([
                    "value"=>0
                ]);
            }else{
                $rules = [
                    'entity_id'=>[
                        new Assert\NotBlank(),
                        new Assert\NotNull()
                    ],
                    'entity_type'=>[
                        new Assert\NotBlank(),
                        new Assert\NotNull()
                    ],
                    'token'=>new Assert\IsNull()
                ];
            }
    
            $rules['file'] = [
                new Assert\NotBlank(),
                new Assert\File([
                    'maxSize'=>"5M",
                    'mimeTypes'=>FileHelper::ALLOWED_MIME_TYPES
                ])
            ];
           
            $constraint = new Assert\Collection($rules);
          
            $violations = $validator->validate($inputs,$constraint);
    
            if($violations->count() > 0){
                return $this->json($violations,400);
            }
    
            $defaultUploadPath = $this->getParameter("default_upload_dir");
            $defaultUploadDir = FileHelper::slugify($entity_type_dir);
            
            $fileHelper = new FileHelper($defaultUploadPath,$defaultUploadDir);
            
            $fileData = $fileHelper->uploadFile($file);

            if(empty($fileData)){

                throw new \Exception();
            }

            $newFile = new Files();
            $newFile->setName($fileData['name']);
            $newFile->setExtension($fileData['extension']);
            $newFile->setMimeType($fileData['mime_type']);
            $newFile->setEntityId($entity_id);
            $newFile->setEntityType($entity_type);
            $newFile->setToken($token);
            $newFile->setPath($fileData['path']);

            $em->persist($newFile);
            $em->flush();

        }catch(\Exception $e){
           
            return $this->json($tr->trans("There was an error uploading the file",400));
        }


        return $this->json([
            "success"=>true,
            "message"=>$tr->trans("File saved")
        ]);
    }

    /**
     * @Route("/files/fetch", name="files_fetch", methods={"POST"})
     */
    public function fetch(Request $request,
                          EntityManagerInterface $em,
                          ValidatorInterface $validator,
                          FilesRepository $filesRepo,
                          $tr
                          ){

        try{
            if(!$request->isXmlHttpRequest()){
                throw new \Exception($tr->trans("Request must be made throught ajax"));
            }
            
            $entity_id = filter_var($request->request->get('entity_id',0),FILTER_VALIDATE_INT);
            if(!$entity_id){
                $entity_id = 0;
            }
            $entity_type = $request->request->get('entity_type',null);
            
            if(!class_exists($entity_type)){
                throw new \Exception($tr->trans("Unexistent entity"));
            }

            $token = $request->request->get('token',null);
            
            $inputs = [
               'entity_id'=>$entity_id,
               'entity_type'=>$entity_type,
               'token'=>!empty($token)?$token:null,
            ];
    
            $rules = [];
    
            $has_token = false;
            
            if((int) $entity_id < 1){
                $has_token = true;

                $rules['token'] = [
                    new Assert\NotBlank(),
                    new Assert\Length(["min"=>16])
                ];
                $rules['entity_type'] = new Assert\NotBlank();
                $rules['entity_id'] = new Assert\EqualTo([
                    "value"=>0
                ]);
            }else{
                $rules = [
                    'entity_id'=>[
                        new Assert\NotBlank(),
                        new Assert\NotNull()
                    ],
                    'entity_type'=>[
                        new Assert\NotBlank(),
                        new Assert\NotNull()
                    ],
                    'token'=>new Assert\IsNull()
                ];
            }
            
            $constraint = new Assert\Collection($rules);
          
            $violations = $validator->validate($inputs,$constraint);
           
            if($violations->count() > 0){
                return $this->json($violations,402);
            }

            if(!$has_token){
                $entity = $em->getRepository($entity_type)->find($entity_id);
                if(!$entity){
                    throw new \Exception($tr->trans("Inexistent entity"));
                }
               
                $files = $filesRepo->getAllFiles($entity->getId(),get_class($entity));
                
            }else{
                $files = $filesRepo->getAllFilesByToken($token,$entity_type);
            }

            $html = $this->renderView("dropzone/list.html.twig",[
                'files'=>$files
            ]);


            return $this->json([
                "success"=>true,
                "html"=>$html
            ]);
            
        }catch(\Exception $e){
            return $this->json([
                "success"=>"false",
                "message"=>$e->getMessage()
            ]);
        }
    }

    /**
     * @Route("/files/delete/{id}", name="files_delete", methods="GET", requirements={"id":"\d+"})
     */
    public function delete(Request $request,
                            Files $file,
                            EntityManagerInterface $em,
                            $tr
                            ){
       
        try{

            if(!$request->isXmlHttpRequest()){
                throw new \Exception($tr->trans("Request must be made throught ajax"));
            }

          
            $entity_type_dir = basename(str_replace("\\",DIRECTORY_SEPARATOR,$file->getEntityType()));

            $fileHelper = new FileHelper($this->getParameter("default_upload_dir"),FileHelper::slugify($entity_type_dir));

            $diskFile = $fileHelper->getFileFromEntity($file);
            
            $em->remove($file);
            $em->flush();

            if($diskFile){

                $fileHelper->deleteFile($diskFile);
            }

            return $this->json([
                "success"=>true,
                "message"=>$tr->trans("File deleted")
            ]);

        }catch(\Exception $e){
            return $this->json([
                "success"=>false,
                "message"=>$e->getMessage()
            ]);
        }

    }

    /**
     * @Route("/files/download/{id}", name="files_download", methods={"GET"}, requirements={"id":"\d+"})
     */
    public function downloadFile(Request $request, 
                    Files $file,
                    $tr){

        try{
       
            $full_path = $this->getParameter("default_upload_dir").DIRECTORY_SEPARATOR.$file->getPath();
            if(file_exists($full_path)){

                return $this->file(new File($full_path),$file->getName().".".$file->getExtension());
            }
        }catch(\Exception $e){
            $referer = $request->server->get("HTTP_REFERER",null);
            if(!$referer){
                return new RedirectResponse($referer);
            }else{
                return $this->redirectToRoute("users");
            }
        }

    }

    /**
     * @Route("/image/{name}/{width?}/{height?}", name="display_image", methods="GET")
     */
    public function displayImage(Request $request,Files $file,$width = null,$height = null){

        try{
            $manager = new ImageManager();

            $width = filter_var($width,FILTER_VALIDATE_INT);
            $height = filter_var($height,FILTER_VALIDATE_INT);

            $entity_type_dir = basename(str_replace("\\",DIRECTORY_SEPARATOR,$file->getEntityType()));
            $fileHelper = new FileHelper($this->getParameter("default_upload_dir"),FileHelper::slugify($entity_type_dir));
            $diskFile = $fileHelper->getFileFromEntity($file);

            
            if($diskFile){

                // $response = new Response(file_get_contents($diskFile));
                // $disposition = $response->headers->makeDisposition(
                //     ResponseHeaderBag::DISPOSITION_INLINE,
                //     $file->getName().".".$file->getExtension()
                // );
                
                // $response->headers->set('Content-Disposition', $disposition);
               
                if(strpos($diskFile->getMimeType(),"image/") === false){
                  return $this->file(new File($this->getParameter("default_upload_dir").DIRECTORY_SEPARATOR."placeholder.jpg"));   
                }
                $response = $fileHelper->streamedResponse($file,ResponseHeaderBag::DISPOSITION_INLINE);    
                return $response;
               
               // return $this->file($diskFile,200,ResponseHeaderBag::DISPOSITION_INLINE);
            }else{

                return $this->file(new File($this->getParameter("default_upload_dir").DIRECTORY_SEPARATOR."placeholder.jpg"));   
              
                // $response = new Response(file_get_contents($diskFile));
                // $disposition = $response->headers->makeDisposition(
                //     ResponseHeaderBag::DISPOSITION_INLINE,
                //     $file->getName().".".$file->getExtension()
                // );
                
                // return $response->headers->set('Content-Disposition', $disposition);

                // return $response;
            }
            
        }catch(\Exception $e){
            return $this->file(new File($this->getParameter("default_upload_dir").DIRECTORY_SEPARATOR."placeholder.jpg"));
        }
       
       
    }
}
