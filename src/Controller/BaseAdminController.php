<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

/**
 * @IsGranted("ROLE_ADMIN")
 */
class BaseAdminController extends AbstractController
{
    public function __construct(AuthorizationCheckerInterface $auth){
       

       //dd($auth->isGranted("ROLE_ADMIN"));
    }
    
}
