<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Manufacturer;

class ManufacturersFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $manufacturer1 = new Manufacturer();
        $manufacturer1->setName("Manufacturer 1");

        $manufacturer2 = new Manufacturer();
        $manufacturer2->setName("Manufacturer 2");

        $manager->persist($manufacturer1);
        $manager->persist($manufacturer2);

        $manager->flush();
    }
}
