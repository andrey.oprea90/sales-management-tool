<?php 

namespace App\Service;

use App\Entity\Attribute;
use App\Entity\AttributeSet;
use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;

class AttributesHelper{

    private $product = null;

    private $em;

    private $tr;

    public function __construct(EntityManagerInterface $em,$tr){
        
        $this->em = $em;
        $this->tr = $tr;
    }

    public function setProduct(Product $product):self{
        $this->product = $product;
        return $this;
    }

    public function getProduct():Product{
        return $this->product;
    }

    public function checkValidity(){
        if(!$this->product){
            throw new \Exception($this->tr->trans("Please set product first"));
        }
    }

    public function getAttributesData():array{
        
        $this->checkValidity();
        $data = [];

        try{
            if($this->product->getAttributeSet()){
                $attributesArr = $this->product->getAttributeSet()->getAttributes()->toArray();
                $extraAttributes = $this->product->getExtraAttributes() ?? [];
               
                foreach($attributesArr as $attribute){
                   // $value = $attribute->getValue();
                    $prodValue = [
                        "plain"=>"",
                        "selected"=>[

                        ]
                    ];
                    // array_walk($value,function($item,$id) use(&$prodValue,$attribute){
                    //     if($id == $attribute->getId()){
                    //         $prodValue = $item;    
                    //     }
                    // });
                    array_walk($extraAttributes,function($item,$code) use(&$prodValue,$attribute){
                        if($code == $attribute->getCode()){
                            $prodValue = $item;    
                        }
                    });
                    
                    $data[$attribute->getCode()]['id'] = $attribute->getId();
                    $data[$attribute->getCode()]['code'] = $attribute->getCode();
                    $data[$attribute->getCode()]['label'] = $this->tr->trans($attribute->getName());
                    $data[$attribute->getCode()]['type'] = $attribute->getType();

                    if($attribute->isDropdown()){
                        $data[$attribute->getCode()]['options'] = $attribute->getOptions();
                    }

                    $data[$attribute->getCode()]['value'] = $prodValue;
                        
                }
                
                
            }
        }catch(\Exception $e){
           
            $data = [];
        }
      
        return $data;
    }

    public function saveAttributeData(Attribute $attribute,array $data = []){
        $data = array_merge([
            "plain"=>"",
            "selected"=>[

            ]
            ],$data);
        $extraAttributes = $this->product->getExtraAttributes() ?? [];
        $extraAttributes[$attribute->getCode()] = $data;
        $this->product->setExtraAttributes($extraAttributes);
    }
}