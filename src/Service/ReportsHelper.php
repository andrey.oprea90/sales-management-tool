<?php 
namespace App\Service;

use App\Entity\Product;
use App\Entity\WarehouseProduct;
use App\Entity\Warehouse;
use App\Repository\ProductRepository;
use App\Repository\WarehouseProductRepository;
use App\Repository\WarehouseRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Order;
use App\Entity\OrderItem;
use App\Repository\OrderRepository;
use App\Repository\OrderItemRepository;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use App\Repository\SettingRepository;
use Carbon\Carbon;
use App\Repository\FinanceRepository;
use App\Repository\SalaryRepository;
use App\Entity\User;

class ReportsHelper{

    private $tr;

    private $em;

    private $orderRepo;

    private $orderItemRepo;

    private $params;

    private $setting;

    private $productRepo;

    private $financeRepo;

    private $salaryRepo;

    private $user = null;

    public function __construct(EntityManagerInterface $em,
                                OrderRepository $orderRepo,
                                OrderItemRepository $orderItemRepo,
                                ParameterBagInterface $params,
                                SettingRepository $settingRepository,
                                ProductRepository $productRepo,
                                FinanceRepository $financeRepo,
                                SalaryRepository $salaryRepo,
                                $tr
    ){

            $this->em = $em;
            $this->orderRepo = $orderRepo;
            $this->orderItemRepo = $orderItemRepo;
            $this->params = $params;
            $this->setting = $settingRepository->getFirstOrNull();
            $this->productRepo = $productRepo;  
            $this->financeRepo = $financeRepo;
            $this->salaryRepo = $salaryRepo;
            $this->tr = $tr;                          

    }

    public function setUser(?User $user){
        $this->user = $user;
    }

    public function getUser():?User{
        return $this->user;
    }

    public function salesPerWarehouse(Carbon $start,Carbon $end,?Warehouse $warehouse = null):array{
        $result = [];
        $items = $this->orderItemRepo->getSalesPerWarehouse($start,$end,$warehouse);
        
        foreach($items as $item){
            if($item->getWarehouse()){
                $result[$item->getWarehouse()->getId()] = !empty($result[$item->getWarehouse()->getId()])?$result[$item->getWarehouse()->getId()]:[
                    "warehouse_name"=>$item->getWarehouse()->getName(),
                    "total_excl_tax"=>0,
                    "total_incl_tax"=>0
                ];
                $result[$item->getWarehouse()->getId()]['total_excl_tax']+=$item->getTotal();
                $result[$item->getWarehouse()->getId()]['total_incl_tax']+=$item->getTotalInclTax();
            }else{
                $result[0] = !empty($result[0])?$result[0]:[
                    "warehouse_name"=>"Global Warehouse",
                    "total_excl_tax"=>0,
                    "total_incl_tax"=>0
                ];
                $result[0]['total_excl_tax']+=$item->getTotal();
                $result[0]['total_incl_tax']+=$item->getTotalInclTax();
            }
            
        }
        array_walk($result,function(&$item,$id){
            $item['total_excl_tax'] = (float) (number_format($item['total_excl_tax'],2,".",""));
            $item['total_incl_tax'] = (float) (number_format($item['total_incl_tax'],2,".",""));
        });
        
        return $result;
    }

    public function costsPerPeriod(Carbon $start,Carbon $end,string $mode = 'month'):array{
        $mode = in_array($mode,['month','week'])?$mode:'month';
        $result = [];
        
        $items = $this->orderRepo->getCostsPerPeriod($start,$end,$this->user);
        $s_cpy = $start->copy();
        $e_cpy = $end->copy();
       
        switch($mode){
            case 'week':
                while($s_cpy->lte($e_cpy)){
                   
                    $result[$s_cpy->format("Y-m-d")] = [
                        "period"=>$s_cpy->format("Y-m-d"),
                        "total_buy"=>0,
                        "total_sell"=>0
                    ];

                    $s_cpy->addDay();
                    if($s_cpy->gt($e_cpy)){
                       
                        $result[$e_cpy->format("Y-m-d")] = [
                            "period"=>$e_cpy->format("Y-m-d"),
                            "total_buy"=>0,
                            "total_sell"=>0
                        ];
                        break;
                    }
                }
                break;
            default:
                while($s_cpy->lte($e_cpy)){
                    
                    $result[$s_cpy->format("Y-m")] = [
                        "period"=>$s_cpy->format("Y-m"),
                        "total_buy"=>0,
                        "total_sell"=>0
                    ];

                    $s_cpy->addDay();
                    if($s_cpy->gt($e_cpy)){
                    
                        $result[$e_cpy->format("Y-m")] = [
                            "period"=>$e_cpy->format("Y-m"),
                            "total_buy"=>0,
                            "total_sell"=>0
                        ];
                        break;
                    }
                }

               

        }

        
        foreach($items as $order){
            

            $dateCreated = Carbon::parse($order->getDateCreated()->format("Y-m-d H:i"));
            $key = $mode == 'week'?$dateCreated->format("Y-m-d"):$dateCreated->format("Y-m");
            if(empty($result[$key])){
                $result[$key] = [
                    "period"=>$key,
                    "total_buy"=>0,
                    "total_sell"=>0
                ];
            }
            $result[$key]['total_buy']+=$order->getTotalBuy();
            $result[$key]['total_sell']+=$order->getTotalInclTax();
        }
        
        
        return $result;
        
    }

    public function additionalIncomeExpensePerPeriod(Carbon $start,Carbon $end,string $mode = 'month'):array{
        $mode = in_array($mode,['month','week'])?$mode:'month';
        $result = [];
        $totals_result = [
            "total_income"=>0,
            "total_expense"=>0
        ];
        $detailed_data = [
            "income"=>[],
            "expense"=>[]
        ];
        $items = $this->financeRepo->findAllByDateInterval($start,$end);
      
        $s_cpy = $start->copy();
        $e_cpy = $end->copy();
       
        switch($mode){
            case 'week':
                while($s_cpy->lte($e_cpy)){
                   
                    $result[$s_cpy->format("Y-m-d")] = [
                        "period"=>$s_cpy->format("Y-m-d"),
                        "total_income"=>0,
                        "total_expense"=>0
                    ];

                    $s_cpy->addDay();
                    if($s_cpy->gt($e_cpy)){
                       
                        $result[$e_cpy->format("Y-m-d")] = [
                            "period"=>$e_cpy->format("Y-m-d"),
                            "total_income"=>0,
                            "total_expense"=>0
                        ];
                        break;
                    }
                }
                break;
            default:
                while($s_cpy->lte($e_cpy)){
                    
                    $result[$s_cpy->format("Y-m")] = [
                        "period"=>$s_cpy->format("Y-m"),
                        "total_income"=>0,
                        "total_expense"=>0
                    ];

                    $s_cpy->addDay();
                    if($s_cpy->gt($e_cpy)){
                    
                        $result[$e_cpy->format("Y-m")] = [
                            "period"=>$e_cpy->format("Y-m"),
                            "total_income"=>0,
                            "total_expense"=>0
                        ];
                        break;
                    }
                }

               

        }

        
        foreach($items as $item){
            

            $dateCreated = Carbon::parse($item->getCreatedAt()->format("Y-m-d"));
            $key = $mode == 'week'?$dateCreated->format("Y-m-d"):$dateCreated->format("Y-m");
            if(empty($result[$key])){
                $result[$key] = [
                    "period"=>$key,
                    "total_income"=>0,
                    "total_expense"=>0
                ];
            }
            
           
            switch($item->getType()){
                case 'income':
                    $result[$key]['total_income']+=$item->getCost();
                    $totals_result['total_income']+=$item->getCost();
                    $detailed_data['income'][$item->getId()] = [
                        "type"=>'income',
                        "name"=>$item->getName(),
                        "cost"=>$this->formatPrices($item->getCost())
                    ];
                    break;
                case 'expense':
                    $result[$key]['total_expense']+=$item->getCost();
                    $totals_result['total_expense']+=$item->getCost();
                    $detailed_data['expense'][$item->getId()] = [
                        "type"=>'expense',
                        "name"=>$item->getName(),
                        "cost"=>$this->formatPrices($item->getCost())
                    ];
                    break;
            }
            
            
        }
        $totals_result['total_income'] = $this->formatPrices($totals_result['total_income']);
        $totals_result['total_expense'] = $this->formatPrices($totals_result['total_expense']);
        
        $result_detailed_data = [];
        $detailed_data['income'] = array_values($detailed_data['income']);
        $detailed_data['expense'] = array_values($detailed_data['expense']);

        $main = $detailed_data['income'];
        $second = $detailed_data['expense'];

        if(count($detailed_data['expense']) > count($detailed_data['income'])){
            $main = $detailed_data['expense'];
            $second = $detailed_data['income'];
        }

        foreach($main as $key=>$item){
            $result_detailed_data[] = [
                $item,
                $second[$key] ?? [
                    "type"=>$item['type'] == "income"?"expense":"income",
                    "name"=>"",
                    "cost"=>""
                ]
            ];
        }
        
        return [
            "result"=>array_values($result),
            "totals"=>$totals_result,
            "detailed"=>$result_detailed_data
        ];
        
    }

    public function totalSalesPerPeriod(Carbon $start, Carbon $end):array{
        $result = [];
        $s_cpy = $start->copy();
        $e_cpy = $end->copy();
        
        while($s_cpy->lte($end)){
            $result[$s_cpy->month] = [
                'label'=>$s_cpy->format("M"),
                'total'=>0
            ];

            $s_cpy->addMonth();
            if($s_cpy->gt($e_cpy)){
                $result[$e_cpy->month] = [
                    'label'=>$e_cpy->format("M"),
                    'total'=>0
                ];
                break;
            }
        }
        
        $orders = $this->orderRepo->getCompleteOrdersPerPeriod($start,$end,$this->user);
        foreach($orders as $order){
            $dateCreated = Carbon::parse($order->getDateCreated()->format("Y-m-d H:i"));
            $key = $dateCreated->month;
            $result[$key] = !empty($result[$key])?$result[$key]:[
                'label'=>$dateCreated->format("M"),
                'total'=>0
            ];
            $result[$key]['total']+=$order->getTotalInclTax(); 
        }
       
        return $result;
    }

    public function incomeVsExpenseDetailed(Carbon $start,Carbon $end,bool $includeStock = false):array{
        $result = [
            "income"=>[
                "sales"=>[
                    "label"=>$this->tr->trans("Sales (Income)"),
                    "name"=>$this->tr->trans("Sales"),
                    "data"=>0,
                    "data_formatted"=>""
                ],
                "additional"=>[
                    "label"=>$this->tr->trans("Additional (Income)"),
                    "name"=>$this->tr->trans("Additional"),
                    "data"=>0,
                    "data_formatted"=>""
                ]
            ],
            "expense"=>[
                "products"=>[
                    "label"=>$this->tr->trans("Products (Expense)"),
                    "name"=>$this->tr->trans("Products"),
                    "data"=>0,
                    "data_formatted"=>""
                ],
                "products_in_stock"=>[
                    "label"=>$this->tr->trans("Products in stock (Expense)"),
                    "name"=>$this->tr->trans("Products in stock"),
                    "data"=>0,
                    "data_formatted"=>""
                ],
                "taxes"=>[
                    "label"=>$this->tr->trans("Sales Taxes (Expense)"),
                    "name"=>$this->tr->trans("Sales Taxes"),
                    "data"=>0,
                    "data_formatted"=>""
                ],
                "additional"=>[
                    "label"=>$this->tr->trans("Additional (Expense)"),
                    "name"=>$this->tr->trans("Additional"),
                    "data"=>0,
                    "data_formatted"=>""
                ],
                "salaries"=>[
                    "label"=>$this->tr->trans("Salaries (Expense)"),
                    "name"=>$this->tr->trans("Salaries"),
                    "data"=>0,
                    "data_formatted"=>""
                ]
            ],
            "total_income"=>[
                "label"=>$this->tr->trans("Income"),
                "data"=>0,
                "data_formatted"=>""
            ],
            "total_expense"=>[
                "label"=>$this->tr->trans("Expense"),
                "data"=>0,
                "data_formatted"=>""
            ],
            "profit"=>[
                "label"=>$this->tr->trans("Profit"),
                "data"=>0,
                "data_formatted"=>""
            ],
        ];
       
        $orders = $this->orderRepo->getCompleteOrdersPerPeriod($start,$end);
        foreach($orders as $order){
            $tax = $order->getTax();
            $total = $order->getTotalInclTax();
            $totalBuyCost = $order->getTotalBuy();

            $result['income']['sales']['data']+=$total;
            $result['expense']['products']['data']+=$totalBuyCost;
            $result['expense']['taxes']['data']+=$tax;

            $result['total_income']['data']+=$total;
            $result['total_expense']['data']+= ($totalBuyCost + $tax );
        }

        if($includeStock){
            $products = $this->productRepo->getProductsByDateInterval($start,$end);
            foreach($products as $product){
                $result['expense']['products_in_stock']['data']+=$product->getQty() * $product->getBuyPrice();
                $result['total_expense']['data']+=$product->getQty() * $product->getBuyPrice();
            }
        }
        
        $additional = $this->financeRepo->findAllByDateInterval($start,$end);
        foreach($additional as $ad){
            if($ad->getType() == "income"){
                $result['income']['additional']['data']+=$ad->getCost();
                $result['total_income']['data']+=$ad->getCost();
            }else{
                $result['expense']['additional']['data']+=$ad->getCost();
                $result['total_expense']['data']+=$ad->getCost();
            }
        }

        $salaries = $this->salaryRepo->getSalariesPerPeriod($start,$end);
        foreach($salaries as $salary){
            $result['expense']['salaries']['data']+=$salary->getTotalPaid();
            $result['total_expense']['data']+=$salary->getTotalPaid();
        }

        $result['profit']['data'] = $result['total_income']['data'] - $result['total_expense']['data'];
        $result['profit']['data_formatted'] = $this->formatPrices($result['profit']['data']);

        $result['income']['sales']['data_formatted'] = $this->formatPrices($result['income']['sales']['data']);
        $result['income']['additional']['data_formatted'] = $this->formatPrices($result['income']['additional']['data']);
        $result['expense']['products']['data_formatted'] = $this->formatPrices($result['expense']['products']['data']);
        $result['expense']['taxes']['data_formatted'] = $this->formatPrices($result['expense']['taxes']['data']);
        $result['expense']['salaries']['data_formatted'] = $this->formatPrices($result['expense']['salaries']['data']);
        $result['expense']['additional']['data_formatted'] = $this->formatPrices($result['expense']['additional']['data']);

        $result['total_income']['data_formatted'] = $this->formatPrices($result['total_income']['data']);
        $result['total_expense']['data_formatted'] = $this->formatPrices($result['total_expense']['data']);

        $result['income']['sales']['label'].=" - ".$result['income']['sales']['data'];
        $result['income']['additional']['label'].=" - ".$result['income']['additional']['data'];
        $result['expense']['products']['label'].=" - ".$result['expense']['products']['data'];
        $result['expense']['products_in_stock']['label'].=" - ".$result['expense']['products_in_stock']['data'];
        $result['expense']['taxes']['label'].=" - ".$result['expense']['taxes']['data'];
        $result['expense']['additional']['label'].=" - ".$result['expense']['additional']['data'];
        $result['expense']['salaries']['label'].=" - ".$result['expense']['salaries']['data'];

        $result['total_income']['label'].=" - ".$result['total_income']['data'];
        $result['total_expense']['label'].=" - ".$result['total_expense']['data'];
        
        return $result;

    }

    public function formatPrices(float $price){
        $currencySymbol = $this->params->has('default_currency_symbol')?$this->params->get('default_currency_symbol'):'';

        return number_format($price,2)." ".$currencySymbol;
    }
}