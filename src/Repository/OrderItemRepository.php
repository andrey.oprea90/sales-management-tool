<?php

namespace App\Repository;

use App\Entity\OrderItem;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use App\Entity\Order;
use App\Entity\Product;
use App\Entity\Warehouse;
use Carbon\Carbon;

/**
 * @method OrderItem|null find($id, $lockMode = null, $lockVersion = null)
 * @method OrderItem|null findOneBy(array $criteria, array $orderBy = null)
 * @method OrderItem[]    findAll()
 * @method OrderItem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderItemRepository extends ServiceEntityRepository
{
    private $paginator;
    
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, OrderItem::class);
    }

    // /**
    //  * @return OrderItem[] Returns an array of OrderItem objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?OrderItem
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function getItems(Order $order){

        return $this->createQueryBuilder('oi')
                    ->leftJoin("oi.product","p")
                    ->leftJoin("oi.warehouse","w")
                    ->andWhere("oi.ord = :ord")
                    ->setParameter("ord",$order)
                    ->addSelect("oi","p","w")
                    ->getQuery()
                    ->getResult();
    }

    public function getInvoiceItems(Order $order){

        return $this->createQueryBuilder('oi')
                    ->innerJoin("oi.product","p")
                    ->andWhere("oi.ord = :ord")
                    ->andWhere("oi.canceled <> :num OR oi.canceled IS NULL")
                    ->setParameter("ord",$order)
                    ->setParameter("num","1")
                    ->addSelect("oi","p")
                    ->getQuery()
                    ->getResult();
    }

    public function getItemOrNull(Order $order,Product $product,?Warehouse $warehouse = null):?OrderItem{

        $qb =  $this->createQueryBuilder('oi')
                    ->leftJoin("oi.product","p")
                    ->leftJoin("oi.warehouse","w")
                    ->andWhere("oi.ord = :ord")
                    ->andWhere("oi.product = :product")
                    ->setParameter("ord",$order)
                    ->setParameter("product",$product);

                   if($warehouse){
                       $qb->andWhere("oi.warehouse = :warehouse")
                          ->setParameter("warehouse",$warehouse);
                   }else{
                       $qb->andWhere("oi.warehouse IS NULL");
                   }

                 return $qb->addSelect("oi","p","w")
                        ->setMaxResults(1)
                        ->getQuery()
                        ->getOneOrNullResult();
    }

    public function getEmptyItems(Order $order){
        
        return $this->createQueryBuilder("oi")
                    ->andWhere("oi.qty_ordered = :qty")
                    ->andWhere("oi.ord = :ord")
                    ->setParameter("qty",0)
                    ->setParameter("ord",$order)
                    ->getQuery()
                    ->getResult();
    }

    public function getSalesPerWarehouse(Carbon $start,Carbon $end,?Warehouse $warehouse = null){
        $params = [
            "start"=>$start->format("Y-m-d H:i"),
            "end"=>$end->format("Y-m-d H:i"),
            "status"=>"complete",
            "num"=>"1"
            
        ];
       
        $qb = $this->createQueryBuilder('oi')
                    ->innerJoin("oi.ord","o")
                    ->leftJoin("oi.warehouse","w");
        if($warehouse){
            $qb->andWhere("w.id = :warehouse");
            $params['warehouse'] = $warehouse;
        }
        $qb->addSelect("o","w");
        $qb->andWhere("o.date_created BETWEEN :start AND :end ");
        $qb->andWhere("o.status = :status");
        $qb->andWhere("oi.canceled <> :num OR oi.canceled IS NULL");
        $qb->setParameters($params);

        return $qb->getQuery()->getResult();
    }

   

    public function getCostsPerPeriod(Carbon $start,Carbon $end){
        $params = [
            "start"=>$start->format("Y-m-d H:i"),
            "end"=>$end->format("Y-m-d H:i"),
            "status"=>"complete",
            "num"=>"1"
            
        ];
       
        $qb = $this->createQueryBuilder('oi')
                    ->innerJoin("oi.product","p")
                    ->innerJoin("oi.ord","o");

        $qb->addSelect("o","p");
        $qb->andWhere("o.date_created BETWEEN :start AND :end ");
        $qb->andWhere("o.status = :status");
        $qb->andWhere("oi.canceled <> :num OR oi.canceled IS NULL");
        $qb->setParameters($params);

        return $qb->getQuery()->getResult();
    }
}
