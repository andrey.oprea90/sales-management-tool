<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FinanceRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Finance
{
    const TYPES = [
        "income",
        "expense"
    ];

    const OPTIONS = [
       true,
       false
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()   
     * @Assert\Length(
     *      min = 2,
     *      max = 200,
     *      minMessage = "Name must be at least {{ limit }} characters long",
     *      maxMessage = "Name cannot be longer than {{ limit }} characters"
     * )
     */
    private $name;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, nullable=true, options={"default":0})
     * @Assert\NotBlank()
     * @Assert\Type(
     *     type="numeric",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    private $cost;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Choice(choices=Finance::TYPES, message="Choose a valid type.")
     */
    private $type;

    /**
     * @ORM\Column(type="boolean", nullable=true,options={"default":0})
     * @Assert\Choice(choices=Finance::OPTIONS, message="Choose a valid option.")
     */
    private $monthly;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Assert\Date()
     */
    private $created_at;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCost()
    {
        return $this->cost;
    }

    public function setCost($cost): self
    {
        $this->cost = $cost;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getMonthly(): ?bool
    {
        return $this->monthly;
    }

    public function setMonthly(?bool $monthly): self
    {
        $this->monthly = $monthly;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(?\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function setDefaultValues(){
        
        if(!$this->created_at){
            $this->created_at = new \DateTime();
        }
        
        if(!$this->cost){
            $this->cost = 0;
        }

        if(!$this->monthly){
            $this->monthly = 0;
        }
        
    }

    /**
     * @ORM\PreFlush
     */
    public function setDefaults(){
        if(!$this->cost){
            $this->cost = 0;
        }

        if(!$this->monthly){
            $this->monthly = 0;
        }
    }
}
