<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use App\Entity\Industry;
use App\Repository\IndustryRepository;
use App\Form\IndustryFormType;
use AndreaSprega\Bundle\BreadcrumbBundle\Annotation\Breadcrumb;

/**
 * IsGranted("ROLE_ADMIN_INDUSTRIES")
 * @Breadcrumb({"label" = "Industry list", "route" = "industry" })
 */
class IndustryController extends AbstractController
{
    /**
     * @Route("/industry/{page}", name="industry", defaults={"page":"1"}, requirements={"page":"\d+"})
     */
    public function index(Request $request, 
                         IndustryRepository $industryRepo,
                        $page
                        )
    {
        $industries = $industryRepo->findAllPaginated($page,$this->getParameter('default_per_page'));

        return $this->render('industry/index.html.twig', [
            'page_title' => 'Industry list',
            'industries'=>$industries
        ]);
    }

    /**
     * @Route("/industry/add", name="industry_add")
     */
    public function add(Request $request,
                        EntityManagerInterface $em,
                        $tr){

        try{
            $industry = new Industry();
            $form = $this->createForm(IndustryFormType::class,$industry);

            $form->handleRequest($request);

            if($form->isSubmitted() && $form->isValid()){

                $em->persist($industry);
                $em->flush();

                $this->addFlash("success",$tr->trans("Industry saved"));
                return $this->redirectToRoute("industry");
            }
        }catch(\Exception $e){
           
            $this->addFlash("error",$e->getMessage());
            return $this->redirectToRoute("industry");
        }
        

        return $this->render('industry/add.html.twig',[
            'page_title'=>'Add industry',
            'form'=>$form->createView()
        ]);
    }

  

    /**
     * @Route("/industry/delete/{id}", name="industry_delete", requirements={"id":"\d+"})
     */
    public function delete(Request $request,
                           EntityManagerInterface $em,
                           Industry $industry,
                           $tr
                           ){

        try{
            
            $em->remove($industry);
            $em->flush();

            $this->addFlash("success",$tr->trans("Industry deleted"));
            return $this->redirectToRoute("industry");

        }catch(\Exception $e){

            $this->addFlash("error",$e->getMessage());
            return $this->redirectToRoute("industry");
        }          

    }
}
