function fetchItems(elemClass){

    var append_elem = document.querySelector(elemClass);
   
    if(!append_elem){
        return;
    }
    var url = append_elem.getAttribute("data-url");
    if(!url){
        return;
    }
    return fetch(url,{
        method:"POST",
        headers: {
            //'Content-Type': 'application/json',
             'Content-Type': 'application/x-www-form-urlencoded',
             'X-Requested-With': 'XMLHttpRequest'
        },
       
    }).then(function(data){
        return data.json();
    }).then(function(response){
        if(response.success && response.html){
            append_elem.innerHTML = response.html;
        }else{
            append_elem.innerHTML = "<p>No results found</p>"
        }
  
        return response;
    });
  }

$(document).ready(function(){
   fetchItems(".items_table");
   $(".add_item_btn").click(function(){
    var form = $(".item_movement_form");
    var action = form.attr("data-action");
    if(action){
        $.ajax({
            url:action,
            method:'POST',
            data:form.serialize(),
            success:function(data){
                
            }
        }).then(function(data){
            
           if(data.success){
               var success_html = $('<div class="alert alert-success">'+data.message+'</div>');
               $(".form-messages").html(success_html);
               $(".form-messages div.alert-success").fadeOut(5000);

               return true;
           }else{
               var error_html = $('<div class="alert alert-danger">'+data.message+'</div>');
               $(".form-messages").html(error_html);
               $(".form-messages div.alert-danger").fadeOut(5000);

               return false;
           }
        }).then(function(data){
            if(data){
               $(".item_movement_form")[0].reset();
               $(".item_movement_form .product_select").val("").trigger("change");
               fetchItems(".items_table");
            }
        });
    }
});

$(".items_table").on("click",".remove_item",function(e){
    e.preventDefault();
   var action = e.currentTarget.getAttribute("data-url");
    if(action){
        $.ajax({
            url:action,
            method:'GET',

        }).then(function(data){
            
           if(data.success){
               var success_html = $('<div class="alert alert-success">'+data.message+'</div>');
               $(".items-messages").html(success_html);
               $(".items-messages div.alert-success").fadeOut(5000);

               return true;
           }else{
               var error_html = $('<div class="alert alert-danger">'+data.message+'</div>');
               $(".items-messages").html(error_html);
               $(".items-messages div.alert-danger").fadeOut(5000);

               return false;
           }
        }).then(function(data){
            fetchItems(".items_table");
            window.location.reload();
        });
    }
});
});