<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type as Type;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\CallbackTransformer;
use App\Entity\Attribute;
use App\Entity\AttributeSet;
use App\Entity\Product;
use App\Service\AttributesHelper;

class ProductAttributesFormType extends AbstractType
{
    private $tr;
    
    public function __construct($tr){
        $this->tr = $tr;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $attributesHelper = !empty($options['data']) && $options['data'] instanceof AttributesHelper?$options['data']:null;
        
        if($attributesHelper){
           
           $attrData = $attributesHelper->getAttributesData();
          
           if(!empty($attrData)){
               foreach($attrData as $code=>$data){
                 
                   switch($data['type']){
                       case 'text':
                            $builder->add($code,Type\TextType::class,[
                                'label'=>$this->tr->trans($data['label']),
                                'data'=>$data['value']['plain'],
                                'mapped'=>false
                            ]);
                            break;
                       case 'textarea':
                            $builder->add($code,Type\TextareaType::class,[
                                'label'=>$this->tr->trans($data['label']),
                                'data'=>$data['value']['plain'],
                                'mapped'=>false
                            ]);
                            break;
                       case 'dropdown':
                       case 'dropdown_multiple':
                          $choices = [];
                          if(!empty($data['options'])){
                              
                              array_walk($data['options'],function($option,$code) use (&$choices,&$data){
                                    $choices[$option['name']] = $code;
                                    if(empty($data['value']['selected']) && $option['default'] == 1){
                                        $data['value']['selected'][] = $code;
                                    }
                              });
                          }
                          $buildData = [
                            'label'=>$this->tr->trans($data['label']),
                            'placeholder'=>$this->tr->trans($data['label']),
                            'data'=>!empty($data['value']['selected'])?current($data['value']['selected']):"",
                            'choices'=>$choices,
                            'multiple'=>in_array($data['type'],['dropdown_multiple']),
                            'mapped'=>false
                          ];
                          if(in_array($data['type'],['dropdown_multiple'])){
                              $buildData['data'] = is_array($data['value']['selected'])?$data['value']['selected']:[];
                          }
                            $builder->add($code,Type\ChoiceType::class,$buildData);   
                            break;

                   }
               }
           }
        }else{

        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class'=>null
        ]);
    }
}
