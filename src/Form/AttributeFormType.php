<?php

namespace App\Form;

use App\Entity\Attribute;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type as Type;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\CallbackTransformer;
use App\Entity\AttributeSet;
use App\Repository\AttributeSetRepository;
use App\Repository\AttributeRepository;

class AttributeFormType extends AbstractType
{
    private $tr;
    
    public function __construct($tr){
        $this->tr = $tr;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $isEdit = !empty($options['data']) && $options['data']->getId()?$options['data']:null;

        $builder
            ->add('name',Type\TextType::class,[
                'label'=>$this->tr->trans('Attribute name'),
                'disabled'=>$isEdit
            ])
            ->add('label',Type\TextType::class,[
                'label'=>$this->tr->trans('Attribute label')
            ])
            ->add('type',Type\ChoiceType::class,[
                'label'=>$this->tr->trans('Attribute type'),
                'placeholder'=>$this->tr->trans('Please select type'),
                'disabled'=>$isEdit,
                'choices'=>array_combine(
                            array_values(Attribute::TYPES),
                            array_keys(Attribute::TYPES)
                    )
            ])
            
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Attribute::class,
        ]);
    }
}
