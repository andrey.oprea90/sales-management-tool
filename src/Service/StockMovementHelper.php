<?php 

namespace App\Service;

use App\Entity\Product;
use App\Entity\WarehouseProduct;
use App\Entity\Warehouse;
use App\Repository\ProductRepository;
use App\Repository\WarehouseProductRepository;
use App\Repository\WarehouseRepository;
use Doctrine\ORM\EntityManagerInterface;

class StockMovementHelper{

    private $product;

    private $productRepository;

    private $em;

    private $warehouseRepository;

    private $defaultWarehouse;

    private $tr;

    public function __construct(ProductRepository $productRepository,
                                EntityManagerInterface $em,
                                WarehouseRepository $warehouseRepository,
                                $tr
                                ){
       
        $this->productRepository = $productRepository;
        $this->warehouseRepository = $warehouseRepository;
        $this->em = $em;
        $this->defaultWarehouse = $this->warehouseRepository->findOneOrNull();
        $this->tr = $tr;
       if(!$this->defaultWarehouse){
           $defaultW = new Warehouse();
           $defaultW->setName("Default warehouse");
           $defaultW->setAddress("Default address");
           $defaultW->setDetails("Created automatically");
          
           $em->persist($defaultW);
           $em->flush();
          
           $this->defaultWarehouse = $defaultW;
       }
        
    }

    public function setProduct(Product $product):self{
        $this->product = $this->productRepository->getProductWithWarehousesData($product);
     
        if($this->product->getWharehouseProduct()->count() < 1){
           $this->createStockObj($this->defaultWarehouse);
        }

        return $this;
          
    }

    public function getProduct():?Product{
        return $this->product;
    }

    public function getStockObj(Warehouse $warehouse):WarehouseProduct{
        if(!$this->product){
            throw new \Exception($this->tr->trans("Please set product first"));
        }
       
        $stockObj = $this->productRepository->getWarehouseProductOrNew($this->product,$warehouse);

        return $stockObj;
    }

    public function getStocksData():array{
        if(!$this->product){
            throw new \Exception($this->tr->trans("Please set product first"));
        }
        $data = [];
        foreach($this->product->getWharehouseProduct() as $wp){
            $data[] = [
                "warehouse"=>$wp->getWarehouse()->getName(),
                "warehouse_code"=>$wp->getWarehouse()->getCode(),
                "qty"=>$wp->getStock(),
                "last_updated"=>$wp->getUpdatedAt(),
                "id"=>$wp->getId()     
            ];
        }

        return $data;
     
    }

    public function getQtyPerWarehouseData(?Warehouse $current = null):array{
        if(!$this->product){
            throw new \Exception($this->tr->trans("Please set product first"));
        }
        $qtyPerWarehouseData = [
            'warehouses'=>[],
            'total_in_current'=>0,
            'total_in_warehouse'=>0,
            'total_on_prod'=>$this->product->getQty(),
        ];
        $wharehouseProducts = $this->product->getWharehouseProduct();
        foreach($this->product->getWharehouseProduct() as $wp ){
            $warehouseId = $wp->getWarehouse()->getId();
            $qtyPerWarehouseData['warehouses'][$warehouseId] = $wp->getStock();
            $qtyPerWarehouseData['total_in_warehouse']+=$wp->getStock();
            $qtyPerWarehouseData['total_on_prod']-=$wp->getStock();
            
        }
        if($current){
            $qtyPerWarehouseData['total_in_current'] = $qtyPerWarehouseData['warehouses'][$current->getId()] ?? 0;
        }
        $qtyPerWarehouseData['total_on_prod'] = $qtyPerWarehouseData['total_on_prod'] < 0?0:$qtyPerWarehouseData['total_on_prod'];
        return $qtyPerWarehouseData;

    }

    public function moveStock(?Warehouse $from,Warehouse $to,int $qty){

        $stockData = $this->getQtyPerWarehouseData($from);
        $availableQty = $from?$stockData['total_in_current']:$stockData['total_on_prod'];
        if($from && ($from->getId() == $to->getId())){
            throw new \Exception($this->tr->trans("Identical warehouses"));
        }
        if($qty > $availableQty){
            throw new \Exception($this->tr->trans("Available qty to move")." {$availableQty}.".$this->tr->trans("You tried to move")." {$qty}");
        }
        if($qty < 1){
            return;
        }
        $toStockObj = $this->getStockObj($to);
        if(!$toStockObj->getId()){
            $toStockObj = $this->createStockObj($to);
        }

        $qty = $qty > 0 ? $qty : 0;
     
        $toStockObj->setStock($toStockObj->getStock()+$qty);
        if($from){
            $fromStockObj = $this->getStockObj($from);
            if(!$fromStockObj->getId()){
                throw new \Exception($this->tr->trans("Product not assigned to this warehouse"));
            }
            $fromStockObj->setStock($fromStockObj->getStock() - $qty);
            
        }
        
       $this->em->flush();

    }

    public function removeStockFromWarehouse(Warehouse $from,int $qty){

        $stockData = $this->getQtyPerWarehouseData($from);
        $availableQty = $stockData['total_in_current'];
        
        if($qty > $availableQty){
            throw new \Exception($this->tr->trans("Available qty to move")." {$availableQty}.".$this->tr->trans("You tried to move"." {$qty}"));
        }
        if($qty < 1){
            return;
        }
        $fromStockObj = $this->getStockObj($from);
      
        if(!$fromStockObj->getId()){
            $fromStockObj = $this->createStockObj($from);
        }

        $qty = $qty > 0 ? $qty : 0;
     
        $fromStockObj->setStock($fromStockObj->getStock()-$qty);
        
        
       $this->em->flush();

    }

    public function deleteStockFromWarehouse(Warehouse $from){

        
        $fromStockObj = $this->getStockObj($from);
      
        if(!$fromStockObj->getId()){
            $fromStockObj = $this->createStockObj($from);
        }

       $fromStockObj->setStock(0);
        
       $this->em->flush();

    }

    public function createStockObj(Warehouse $warehouse):WarehouseProduct{
        $wh = new WarehouseProduct();
        $wh->setStock(0);
        $wh->setWarehouse($warehouse);

        $this->product->addWharehouseProduct($wh);
        $this->em->persist($wh);
        $this->em->flush();

        return $wh;
        
    }
    
}