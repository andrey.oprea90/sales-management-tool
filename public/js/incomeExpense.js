$(document).ready(function(){
    $('.finance-filters .s_date').datepicker({
        dateFormat:"yy-mm-dd",
        
    });

    $('.finance-filters .e_date').datepicker({
        dateFormat:"yy-mm-dd",
        
    });

    $("#finance-filters-form .filter-item").each(function(){
        
        $(this).change(function(){
          $.ajax({
            url:'/finance/filtersAjax',
            method:'GET',
            data:$(this).closest('form').serialize()
          }).then(function(data){
              if(data.success){
                  if(data.url){
                    var url="?"+data['url'];
                    window.location.href = url;
                  }else{
                      window.location.href = "/finance";
                  }  
              }
          });  
        });
    });

    $("#finance-filters-form .filter-dates").each(function(){
        
        $(this).change(function(){
          if($(this).hasClass('s_date')){
              if(!$(this).parent().parent().find(".e_date").val() ){
                  return;
              }
          }else if($(this).hasClass('e_date')){
            if(!$(this).parent().parent().find(".s_date").val() ){
                return;
            }
          }else{
              return;
          }  
          $.ajax({
            url:'/finance/filtersAjax',
            method:'GET',
            data:$(this).closest('form').serialize()
          }).then(function(data){
              if(data.success){
                  if(data.url){
                    var url="?"+data['url'];
                    window.location.href = url;
                  }else{
                      window.location.href = "/finance";
                  }  
              }
          });  
        });
    });
});