<?php

namespace App\Security\Voter;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;
use App\Entity\Order;
use Symfony\Component\Security\Core\Security;

class OrdersVoter extends Voter
{
    private $security;

    public function __construct(Security $security){
        $this->security = $security;
    }

    protected function supports($attribute, $subject)
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return in_array($attribute, ['ORDER_VIEW','GENERATE_INVOICE'])
            && $subject instanceof \App\Entity\Order;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface) {
            return false;
        }

        if($this->security->isGranted("ROLE_ADMIN_ORDERS")){
            return true;
        }

       

        // ... (check conditions and return true to grant permission) ...
        switch ($attribute) {
            case 'ORDER_VIEW':
            case 'GENERATE_INVOICE':
                if($user->getCustomer()){
                    return $subject->getCustomer() && ($subject->getCustomer() == $user->getCustomer());
                }elseif($user->getClient()){
                    return $subject->getClient() && ($subject->getClient() == $user->getClient());
                }
                break;
            
        }

        return false;
    }
}
