<?php

namespace App\Repository;

use App\Entity\WarehouseProduct;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Knp\Component\Pager\PaginatorInterface;
use App\Entity\Warehouse;

/**
 * @method WarehouseProduct|null find($id, $lockMode = null, $lockVersion = null)
 * @method WarehouseProduct|null findOneBy(array $criteria, array $orderBy = null)
 * @method WarehouseProduct[]    findAll()
 * @method WarehouseProduct[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WarehouseProductRepository extends ServiceEntityRepository
{
    private $paginator;
    
    public function __construct(RegistryInterface $registry,PaginatorInterface $paginator)
    {
        $this->paginator = $paginator;
        parent::__construct($registry, WarehouseProduct::class);
    }

    // /**
    //  * @return WarehouseProduct[] Returns an array of WarehouseProduct objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('w.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?WarehouseProduct
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function findAllPaginated(Warehouse $warehouse,int $page = 1, int $perPage = 10){

        $query = $this->createQueryBuilder('wp')
                      ->innerJoin("wp.warehouse","w")
                      ->innerJoin("wp.product","p")
                      ->andWhere("w.id = :warehouse")
                      ->andWhere("p.deleted_at IS NULL")
                      ->setParameters([
                          "warehouse"=>$warehouse,
                      ])
                      ->addSelect("w","p")
                      ->getQuery();
       

        return $this->paginator->paginate($query,$page,$perPage);

    }
}
