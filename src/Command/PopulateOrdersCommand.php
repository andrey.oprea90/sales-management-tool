<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use App\Entity\Order;
use App\Entity\OrderItem;
use App\Entity\Product;
use App\Repository\OrderRepository;
use App\Repository\OrderItemRepository;
use App\Repository\ProductRepository;
use App\Repository\WarehouseRepository;
use App\Form\OrderFormType;
use App\Form\OrderItemMovementFormType;
use App\Service\OrderHelper;
use App\Service\StockMovementHelper;
use App\Form\DataObjects\OrderItemMovement;
use App\Entity\Invoice;
use App\Repository\InvoiceRepository;
use App\Form\InvoiceFormType;
use Symfony\Component\Console\Helper\ProgressBar;
use App\Repository\TaxRepository;
use App\Repository\CustomerRepository;

class PopulateOrdersCommand extends Command
{
    protected static $defaultName = 'populate:orders';

    public function __construct(EntityManagerInterface $em,
                                StockMovementHelper $stockHelper,
                                WarehouseRepository $warehouseRepo,
                                TaxRepository $taxRepo,
                                OrderRepository $orderRepo,
                                OrderHelper $orderHelper,
                                ProductRepository $productRepo,
                                CustomerRepository $customerRepo){

        $this->em = $em;
        $this->stockHelper = $stockHelper;
        $this->warehouseRepo = $warehouseRepo;
        $this->taxRepo = $taxRepo;
        $this->orderRepo = $orderRepo;
        $this->orderHelper = $orderHelper;
        $this->productRepo = $productRepo;
        $this->customerRepo = $customerRepo;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Populates db with orders')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->note('Starting order creation process');
        $this->createOrders($output,2000);
        $io->success('Order creation process finished');
    }

    private function createOrders(OutputInterface $output,int $total):bool{

        $progressBar = new ProgressBar($output,$total);
        $progressBar->start();
        $days = range(1,360);
        $warehouses = $this->warehouseRepo->findAll();
        $taxes = $this->taxRepo->findAll();
        $customers = $this->customerRepo->findAll();
        $products = $this->productRepo->findBy(["qty"=>50],[
            "created_at"=>"DESC"
        ]);

        try{
            $i = 0;
           foreach($products as $product){
            if($i > $total){
                break;
            }
          
            $this->em->transactional(function($em) use($product,$i,$warehouses,$customers){
                $customer = $i % 2 == 0? $customers[0]:$customers[1];
                $order = new Order();
                $order->setCustomer($customer);
                $order->setNumber($this->orderHelper->generateUniqueOrderNumber());
                $order->setStatus("new");
                $em->persist($order);
                $em->flush();
               
                foreach($warehouses as $warehouse){
                    $orderItem = $this->orderHelper->getOrderItemOrNew($order,$product,$warehouse);
                    $order->addItem($orderItem);
                    $this->orderHelper->updateOrderItem($orderItem,10);
                   
                }

                $invoice = new Invoice();
                $prDate = $product->getCreatedAt();
                $dueDate = (new \DateTime($prDate->format("Y-m-d H:i")))->add(new \DateInterval("P1M"));
              
                $invoiceNumber = $this->orderHelper->generateUniqueInvoiceNumber();
                $invoice->setSeries("TST");
                $invoice->setNumber($invoiceNumber);
                $invoice->setOrd($order);
                $invoice->setPartial(false);
                $invoice->setTotalPaid($order->getTotalInclTax());
                $order->setStatus("complete");
                $order->setDateCreated($prDate);

                $em->persist($invoice);
                $em->flush();


            });
            $progressBar->advance();
            $i++;
           }
        }catch(\Exception $e){
           dd($e->getMessage());
            return false;
        }
       
        $progressBar->finish();
        return true;
    }
}
