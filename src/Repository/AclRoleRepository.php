<?php

namespace App\Repository;

use App\Entity\AclRole;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @method AclRole|null find($id, $lockMode = null, $lockVersion = null)
 * @method AclRole|null findOneBy(array $criteria, array $orderBy = null)
 * @method AclRole[]    findAll()
 * @method AclRole[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AclRoleRepository extends ServiceEntityRepository
{
    private $paginator;
    
    public function __construct(RegistryInterface $registry,PaginatorInterface $paginator)
    {
        $this->paginator = $paginator;
        parent::__construct($registry, AclRole::class);
    }

    // /**
    //  * @return AclRole[] Returns an array of AclRole objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AclRole
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function findAllPaginated($page = 1,$perPage = 10){

        $query =  $this->createQueryBuilder('a')
                        ->orderBy('a.name','ASC')
                        ->getQuery();

        return $this->paginator->paginate($query,$page,$perPage);
    }


}
