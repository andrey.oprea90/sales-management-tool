<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Position;


class PositionsFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $position = new Position();
        $position->setName("Position 1");
        $position2 = new Position();
        $position2->setName("Position 2");
        
        $manager->persist($position);
        $manager->persist($position2);

        $manager->flush();
    }
}
