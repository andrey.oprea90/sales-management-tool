<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use AndreaSprega\Bundle\BreadcrumbBundle\Annotation\Breadcrumb;
use App\Entity\AttributeSet;
use App\Repository\AttributeSetRepository;
use App\Entity\Attribute;
use App\Repository\AttributeRepository;
use App\Form\AttributeSetFormType;
use App\Form\AttributeFormType;
use App\Form\DataObjects\AttributeOption;
use App\Form\AttributeOptionFormType;
use App\Service\FileHelper;


/**
 * @IsGranted("ROLE_ADMIN_ATTRIBUTES")
 * @Breadcrumb({"label" = "Attributes", "route" = "attribute" })
 */
class AttributeController extends AbstractController
{
    /**
     * @Route("/attribute", name="attribute", defaults={"page":"1"}, requirements={"page":"\d+"})
     */
    public function index(Request $request,
                         EntityManagerInterface $em,
                         AttributeRepository $attrRepo,
                         $page 
                        )
    {
        $attributes = $attrRepo->findAllPaginated($page,$this->getParameter('default_per_page'));
        return $this->render('attribute/index.html.twig', [
            'page_title' => 'Attributes',
            'attributes' => $attributes
        ]);
    }

        /**
     * @Route("/attribute/add", name="attribute_add")
     */
    public function add(Request $request,
                        EntityManagerInterface $em,
                        AttributeRepository $attributeRepo,
                        $tr){

        try{
           
            $attribute = new Attribute();
            $form = $this->createForm(AttributeFormType::class,$attribute);

            $form->handleRequest($request);

            if($form->isSubmitted() && $form->isValid()){

                $em->persist($attribute);
                $em->flush();

                $this->addFlash("success",$tr->trans("Attribute saved"));
                return $this->redirectToRoute("attribute_edit",[
                    "id"=>$attribute->getId()
                ]);
            }
        }catch(\Exception $e){
           
            $this->addFlash("error",$e->getMessage());
            return $this->redirectToRoute("attribute_set");
        }
        

        return $this->render('attribute/add.html.twig',[
            'page_title'=>'Add attribute',
            'form'=>$form->createView(),
            'attribute'=>$attribute
        ]);
    }

    /**
     * @Route("/attribute/edit/{id}", name="attribute_edit", requirements={"id":"\d+"})
     */
    public function edit(Request $request,
                        EntityManagerInterface $em,
                        Attribute $attribute,
                        $tr){

        try{
           
            $form = $this->createForm(AttributeFormType::class,$attribute);
           
            $form->handleRequest($request);

            if($form->isSubmitted() && $form->isValid()){

                $em->persist($attribute);
                $em->flush();

                $this->addFlash("success",$tr->trans("Attribute edited"));
                return $this->redirectToRoute("attribute_edit",[
                    "id"=>$attribute->getId()
                ]);
            }
        }catch(\Exception $e){
           
            $this->addFlash("error",$e->getMessage());
            return $this->redirectToRoute("attribute_set");
        }
        

        return $this->render('attribute/edit.html.twig',[
            'page_title'=>'Edit attribute set',
            'form'=>$form->createView(),
            'attribute'=>$attribute,
            'dropdowns'=>['dropdown','dropdown_multiple']
        ]);
    }

    /**
     * @Route("/attribute/delete/{id}", name="attribute_delete", requirements={"id":"\d+"})
     */
    public function delete(Request $request,
                           EntityManagerInterface $em,
                           Attribute $attribute,
                           $tr
                           ){

        try{
            
            $em->remove($attribute);
            $em->flush();

            $this->addFlash("success",$tr->trans("Attribute deleted"));
            return $this->redirectToRoute("attribute");

        }catch(\Exception $e){
            
            $this->addFlash("error",$e->getMessage());
            return $this->redirectToRoute("attribute");
        }          

    }

    /**
     * @Route("/attribute/edit/{id}/options/add", name="attribute_add_option", requirements={"id":"\d+"})
     */
    public function addOption(Request $request,
                             EntityManagerInterface $em,
                             Attribute $attribute,
                             $tr){

        try{
            $option = AttributeOption::createFromData();
           
            $form = $this->createForm(AttributeOptionFormType::class,$option);
                        
            $form->handleRequest($request);
                        
            if($form->isSubmitted() && $form->isValid()){
                        
                    
                    $existingAttributes = $attribute->getOptions();
                  
                    $option->setCode(FileHelper::slugify($option->getName()));
                  
                    if(in_array($option->getCode(),array_keys($existingAttributes))){
                        throw new \Exception($tr->trans("Attribute already exists"));
                    }
                    if($option->getDefault()){
                        array_walk($existingAttributes,function(&$item,$k){
                            $item['default'] = 0;
                        });
                    }
                    
                    $existingAttributes[$option->getCode()] = [
                        "name"=>$option->getName(),
                        "code"=>$option->getCode(),
                        "default"=>$option->getDefault()
                    ];
                   
                    $attribute->setOptions($existingAttributes);
                    
                    $em->flush();
                        
                    $this->addFlash("success",$tr->trans("Attribute edited"));
                        return $this->redirectToRoute("attribute_edit",[
                                            "id"=>$attribute->getId()
                        ]);
                    }
        }catch(\Exception $e){
          
            $this->addFlash("error",$e->getMessage());
            return $this->redirectToRoute("attribute");
        }
                                
                        
        return $this->render('attribute/dropdowns/add.html.twig',[
                                    'page_title'=>'Add option',
                                    'form'=>$form->createView(),
                                    'attribute'=>$attribute
                                    
                                ]);                         

    }

     /**
     * @Route("/attribute/edit/{id}/options/delete/{code}", name="attribute_delete_option", requirements={"id":"\d+"})
     */
    public function deleteOption(Request $request,
                             EntityManagerInterface $em,
                             Attribute $attribute,
                             $code,
                             $tr){

        try{
            
           $options = $attribute->getOptions();
           if(!empty($options[$code])){
               unset($options[$code]);
           } 
           $attribute->setOptions($options);
           $em->flush();

           $this->addFlash("error",$tr->trans("Option deleted"));
           return $this->redirectToRoute("attribute_edit",[
                "id"=>$attribute->getId()
            ]);
            
        }catch(\Exception $e){
           
            $this->addFlash("error",$e->getMessage());
            return $this->redirectToRoute("attribute");
        }
                                
                                               

    }
}
