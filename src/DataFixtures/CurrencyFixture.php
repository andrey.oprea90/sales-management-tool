<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Currency;

class CurrencyFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $currencyData = [
            [
                "name"=>"Euro",
                "code"=>"EUR",
                "symbol"=>"&#128;"
            ],
            [
                "name"=>"US Dollar",
                "code"=>"USD",
                "symbol"=>"&#36;"
            ]
        ];
        foreach($currencyData as $c){
            $currency = new Currency();
            $currency->setName($c['name']);
            $currency->setCode($c['code']);
            $currency->setSymbol($c['symbol']);

            $manager->persist($currency);
        }

        $manager->flush();
    }
}
