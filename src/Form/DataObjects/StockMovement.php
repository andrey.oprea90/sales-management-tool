<?php 

namespace App\Form\DataObjects;
use App\Validator\UniqueUser;
use Symfony\Component\Validator\Constraints as Assert;

class StockMovement{

    public $fromWarehouse;

    /**
     * @Assert\NotNull(message="No to warehouse selected")
     */
    public $toWarehouse;

    /**
     * @Assert\NotBlank(message="Qty field is mandatory")
     * @Assert\Type(
     *     type="numeric",
     *     message="The qty field {{ value }} is not a valid {{ type }}."
     * )
     */
    public $qty;
    

}