<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OrderRepository")
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity(
 *     fields={"number"},
 *     message="Order number already exists"
 * )
 * @ORM\Table(name="orders")
 */
class Order
{
    const STATUS_NEW = 'new';
    const STATUS_COMPLETE = 'complete';
    const STATUS_CANCELED = 'canceled';

    const ORDER_STATUSES = [
        'new',
        'complete',
        'canceled'
    ];
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Type(
     *     type="numeric",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    private $number;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Choice(choices=Order::ORDER_STATUSES, message="Choose a valid status.")
     */
    private $status;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date_created;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Client", inversedBy="orders")
     */
    private $client;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Customer", inversedBy="orders")
     */
    private $customer;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\OrderItem", mappedBy="ord", cascade={"remove"})
     */
    private $items;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, options={"default":0})
     * @Assert\NotBlank()
     * @Assert\Type(
     *     type="numeric",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    private $total_excl_tax;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, options={"default":0})
     * @Assert\NotBlank()
     * @Assert\Type(
     *     type="numeric",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    private $tax;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, options={"default":0})
     * @Assert\NotBlank()
     * @Assert\Type(
     *     type="numeric",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    private $total_incl_tax;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, nullable=true, options={"default":0})
     * @Assert\Type(
     *     type="numeric",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    private $total_buy;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Invoice", mappedBy="ord", orphanRemoval=true, cascade={"remove"})
     */
    private $invoices;

    public function __construct()
    {
        $this->items = new ArrayCollection();
        $this->invoices = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumber(): ?string
    {
        return $this->number;
    }

    public function setNumber(string $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getDateCreated(): ?\DateTimeInterface
    {
        return $this->date_created;
    }

    public function setDateCreated(\DateTimeInterface $date_created): self
    {
        $this->date_created = $date_created;

        return $this;
    }


    public function getClient(): ?Client
    {
        return $this->client;
    }

    public function setClient(?Client $client): self
    {
        $this->client = $client;

        return $this;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setCustomer(?Customer $customer): self
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * @return Collection|OrderItem[]
     */
    public function getItems(): Collection
    {
        return $this->items;
    }

    public function addItem(OrderItem $item): self
    {
        if (!$this->items->contains($item)) {
            $this->items[] = $item;
            $item->setOrd($this);
        }

        return $this;
    }

    public function removeItem(OrderItem $item): self
    {
        if ($this->items->contains($item)) {
            $this->items->removeElement($item);
            // set the owning side to null (unless already changed)
            if ($item->getOrd() === $this) {
                $item->setOrd(null);
            }
        }

        return $this;
    }

    public function getTotalExclTax()
    {
        return $this->total_excl_tax;
    }

    public function setTotalExclTax($total_excl_tax): self
    {
        $this->total_excl_tax = $total_excl_tax;

        return $this;
    }

    public function getTax()
    {
        return $this->tax;
    }

    public function setTax($tax): self
    {
        $this->tax = $tax;

        return $this;
    }

    public function getTotalInclTax()
    {
        return $this->total_incl_tax;
    }

    public function setTotalInclTax($total_incl_tax): self
    {
        $this->total_incl_tax = $total_incl_tax;

        return $this;
    }

     /**
     * @ORM\PrePersist
     */
    public function setDefaultValues(){

        $this->date_created = new \DateTime();

        if(!$this->total_excl_tax){
            $this->total_excl_tax = 0;
        }

        if(!$this->total_buy){
            $this->total_buy = 0;
        }

        if(!$this->tax){
            $this->tax = 0;
        }

        if(!$this->total_incl_tax){
            $this->total_incl_tax = $this->total_excl_tax + $this->tax;
        }

        if(!$this->status){
            $this->status = static::STATUS_NEW;
        }

        
    }

    public function getClientName():string{
        if($this->client){
            return $this->client->getName()." (".$this->client->getEmail().")";
        }

        if($this->customer){

            return $this->customer->getFirstName()." ".$this->customer->getLastName()." (".$this->customer->getEmail().")";
        }

        return "";
    }

    public function getClientData():array{
        $data = [
            "name"=>"",
            "email"=>"",
            "phone"=>"",
            "identification"=>"",
            "vat"=>"",
            "id_card"=>"",
            "details"=>"",
            "shipping_address"=>"",
            "billing_address"=>"",
            "is_company"=>""

        ];
        
        if($this->client){
            $data['is_company'] = true;
            $data['name'] =  $this->client->getName();
            $data['email'] = $this->client->getEmail();
            $data['phone'] = $this->client->getPhone();
            $data['identification'] = $this->client->getIdentification();
            $data['vat'] = $this->client->getVatnumber();
            $data['details'] = $this->client->getDetails();
            $data['shipping_address'] = $this->client->getShippingAddress();
            $data['billing_address'] = $this->client->getBillingAddress();

            return $data;
        }

        if($this->customer){
            $data['is_company'] = false;
            $data['name'] =  $this->customer->getFirstName()." ".$this->customer->getLastName();
            $data['email'] = $this->customer->getEmail();
            $data['phone'] = $this->customer->getPhone();
            $data['identification'] = $this->customer->getIdentification();
            $data['id_card'] = $this->customer->getIdCard();
            $data['details'] = $this->customer->getDetails();
            $data['shipping_address'] = $this->customer->getShippingAddress();
            $data['billing_address'] = $this->customer->getBillingAddress();
        }

        return $data;
    }

    public function isCompany(){
        return $this->client instanceof Client;
    }

    public function getFormattedTotalExclTax(string $currency = ""){
        return number_format($this->total_excl_tax,2)." ".$currency;
    }

    public function getFormattedTotalInclTax(string $currency = ""){
        return number_format($this->total_incl_tax,2)." ".$currency;
    }

    public function getTotalBuy()
    {
        return $this->total_buy;
    }

    public function setTotalBuy($total_buy): self
    {
        $this->total_buy = $total_buy;

        return $this;
    }

    /**
     * @return Collection|Invoice[]
     */
    public function getInvoices(): Collection
    {
        return $this->invoices;
    }

    public function addInvoice(Invoice $invoice): self
    {
        if (!$this->invoices->contains($invoice)) {
            $this->invoices[] = $invoice;
            $invoice->setOrd($this);
        }

        return $this;
    }

    public function removeInvoice(Invoice $invoice): self
    {
        if ($this->invoices->contains($invoice)) {
            $this->invoices->removeElement($invoice);
            // set the owning side to null (unless already changed)
            if ($invoice->getOrd() === $this) {
                $invoice->setOrd(null);
            }
        }

        return $this;
    }

    public function __toString(){

        return static::class;
    }
}
