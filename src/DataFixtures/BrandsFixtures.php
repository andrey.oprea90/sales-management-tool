<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Brand;

class BrandsFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $brand1 = new Brand();
        $brand1->setName("Brand 1");

        $brand2 = new Brand();
        $brand2->setName("Brand 2");

        $manager->persist($brand1);
        $manager->persist($brand2);

        $manager->flush();
    }
}
