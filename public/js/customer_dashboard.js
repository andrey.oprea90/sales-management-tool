
  function salesPerPeriod(elemClass){

    var append_elem = document.querySelector("#"+elemClass);
    
    if(!append_elem){
        return;
    }

    $(append_elem).html("");
    var sp = spinner($(append_elem).attr("id"));

    var url = append_elem.getAttribute("data-url");
    if(!url){
        return;
    }
    var token = append_elem.getAttribute("data-tok");
    if(!token){
        return;
    }
  
    var start =  append_elem.getAttribute("data-start");
    var end =  append_elem.getAttribute("data-end");
   

    if(!start || !end){
        return;
    }

    var headers = {
        'Content-Type': 'application/json',
       //  'Content-Type': 'application/x-www-form-urlencoded',
       'Authorization': 'Bearer '+token
         
    };
    data={
        start:start,
        end:end
    };
    return fetch(url,{
        method:"POST",
        headers: headers,
        body:JSON.stringify(data)
       
       
    }).then(function(data){
        
        return data.json();
    }).then(function(response){
          
      if(response.length){
        $.plot("#"+elemClass, [ response ], {
            series: {
                bars: {
                    show: true,
                    barWidth: 0.6,
                    align: "center"
                }
            },
            xaxis: {
                mode: "categories",
                tickLength: 0
            }
        });
      }
    }).finally(function(){
        try{
            sp.stop();
        }catch(e){

        }
        
    });
  }

  function salesPerMonth(elemClass){

    var append_elem = document.querySelector("#"+elemClass);

    if(!append_elem){
        return;
    }

    $(append_elem).html("");
    var sp = spinner($(append_elem).attr("id"));


    var url = append_elem.getAttribute("data-url");
    if(!url){
        return;
    }
    var token = append_elem.getAttribute("data-tok");
    if(!token){
        return;
    }

    var start =  append_elem.getAttribute("data-start");
    var end =  append_elem.getAttribute("data-end");
    var mode =  append_elem.getAttribute("data-mode");

    if(!start || !end){
        return;
    }

    var headers = {
        'Content-Type': 'application/json',
       //  'Content-Type': 'application/x-www-form-urlencoded',
       'Authorization': 'Bearer '+token
         
    };
    data={
        start:start,
        end:end,
        mode:mode
    };
    return fetch(url,{
        method:"POST",
        headers: headers,
        body:JSON.stringify(data)
       
       
    }).then(function(data){
        
        return data.json();
    }).then(function(response){
          
      if(response.result.length){
        Morris.Line({
            element: elemClass,
            data: response.result,
            xkey: 'period',
            xLabels: 'day',
            ykeys: ['total'],
            labels: ['Total ordered']
        });
      }
   
    }).finally(function(){
        try{
            sp.stop();

        }catch(e){

        }
        
    });
  }

  $('.start_date').datepicker({
    dateFormat:"dd.mm.yy",
    onSelect: function(date,datepicker){
      
       var startDate = moment(date,"DD.MM.YYYY");
       startDateFormatted = startDate.format("YYYY-MM-DD");
       if($(this).parent().parent().find(".end_date").val()){
           var endDate = moment($(this).parent().parent().find(".end_date").val(),"DD.MM.YYYY");
           var endDateFormatted = endDate.format("YYYY-MM-DD");
           if(startDate <= endDate){
              startDateFormatted+=" 00:00";
              endDateFormatted+=" 23:59";

              $(this).parent().parent().parent().parent().find(".custom-chart").attr("data-start",startDateFormatted);
              $(this).parent().parent().parent().parent().find(".custom-chart").attr("data-end",endDateFormatted);

              var id =  $(this).parent().parent().parent().parent().find(".custom-chart").attr("id");
             
              $("#"+id).html("");
             
              switch(id){
                  case "hero-bar":
                      salesPerWarehouse("hero-bar");
                      break;
                   case "hero-graph":
                       profitPerPeriod("hero-graph");
                       break;
                   case "catchart":
                       salesPerPeriod("catchart");
                       break;
                   case "piechart2":
                       incomeVsExpense("piechart2","piechart1");
                       break;

              }
             
           }else{
               var errorMessage = 'Start date must be lower than end date';
               $(this).parent().parent().parent().parent().find(".messages").fadeIn().html("<div class='alert alert-info'>"+errorMessage+"</div>").fadeOut(5000);
           }
       }
    }
});



$('.end_date').datepicker({
   dateFormat:"dd.mm.yy",
   onSelect: function(date,datepicker){
       var endDate = moment(date,"DD.MM.YYYY");
       var endDateFormatted = endDate.format("YYYY-MM-DD");
       if($(this).parent().parent().find(".start_date").val()){
           var startDate = moment($(this).parent().parent().find(".start_date").val(),"DD.MM.YYYY");
           var startDateFormatted = startDate.format("YYYY-MM-DD");
           if(startDate <= endDate){
               startDateFormatted+=" 00:00";
               endDateFormatted+=" 23:59";
               
               $(this).parent().parent().parent().parent().find(".custom-chart").attr("data-start",startDateFormatted);
               $(this).parent().parent().parent().parent().find(".custom-chart").attr("data-end",endDateFormatted);
               
               var id =  $(this).parent().parent().parent().parent().find(".custom-chart").attr("id");
              
               switch(id){
                   case "hero-bar":
                       salesPerWarehouse("hero-bar");
                       break;
                       case "hero-graph":
                           profitPerPeriod("hero-graph");
                           break;
                       case "catchart":
                           salesPerPeriod("catchart");
                           break;
                       case "piechart2":
                            incomeVsExpense("piechart2","piechart1");
                            break;
               }
           }else{
               var errorMessage = 'Start date must be lower than end date';
               $(this).parent().parent().parent().parent().find(".messages").fadeIn().html("<div class='alert alert-info'>"+errorMessage+"</div>").fadeOut(5000);
           }
       }
   }
});

$(".start_month").change(function(){
    var startMonth = $(this).val().trim();
    var startYear = $(this).parent().parent().find(".start_year").val().trim();

    if( (startMonth !="" && !isNaN(startMonth)) && (startYear !="" && !isNaN(startYear))){
        var yearMonth = startYear+"-"+startMonth;
        var sDate = moment(yearMonth,"YYYY-MM");
        var eDate = moment(sDate).endOf("month");

        if(sDate.isValid() && eDate.isValid()){
           var startDateFormatted = sDate.format("YYYY-MM-DD")+" "+"00:00";
           var endDateFormatted = eDate.format("YYYY-MM-DD")+" "+"23:59";
           
           $(this).parent().parent().parent().parent().find(".custom-chart").attr("data-start",startDateFormatted);
           $(this).parent().parent().parent().parent().find(".custom-chart").attr("data-end",endDateFormatted);

           salesPerMonth('income-expense-graph');
        }
    }

});

$(".start_year").change(function(){
    var startYear = $(this).val().trim();
    var startMonth = $(this).parent().parent().find(".start_month").val().trim();

    if( (startMonth !="" && !isNaN(startMonth)) && (startYear !="" && !isNaN(startYear))){
        var yearMonth = startYear+"-"+startMonth;
        var sDate = moment(yearMonth,"YYYY-MM");
        var eDate = moment(sDate).endOf("month");

        if(sDate.isValid() && eDate.isValid()){
           var startDateFormatted = sDate.format("YYYY-MM-DD")+" "+"00:00";
           var endDateFormatted = eDate.format("YYYY-MM-DD")+" "+"23:59";
           
           $(this).parent().parent().parent().parent().find(".custom-chart").attr("data-start",startDateFormatted);
           $(this).parent().parent().parent().parent().find(".custom-chart").attr("data-end",endDateFormatted);

           salesPerMonth('income-expense-graph');
        }
    }

});

 salesPerPeriod("catchart");

 salesPerMonth('income-expense-graph');