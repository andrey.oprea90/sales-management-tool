<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190624123754 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE attributes (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, code VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, options JSON DEFAULT NULL, value JSON DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE attribute_attribute_set (attribute_id INT NOT NULL, attribute_set_id INT NOT NULL, INDEX IDX_7592EB06B6E62EFA (attribute_id), INDEX IDX_7592EB06321A2342 (attribute_set_id), PRIMARY KEY(attribute_id, attribute_set_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE attributesets (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE attribute_attribute_set ADD CONSTRAINT FK_7592EB06B6E62EFA FOREIGN KEY (attribute_id) REFERENCES attributes (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE attribute_attribute_set ADD CONSTRAINT FK_7592EB06321A2342 FOREIGN KEY (attribute_set_id) REFERENCES attributesets (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE product ADD attribute_set_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD321A2342 FOREIGN KEY (attribute_set_id) REFERENCES attributesets (id)');
        $this->addSql('CREATE INDEX IDX_D34A04AD321A2342 ON product (attribute_set_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE attribute_attribute_set DROP FOREIGN KEY FK_7592EB06B6E62EFA');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD321A2342');
        $this->addSql('ALTER TABLE attribute_attribute_set DROP FOREIGN KEY FK_7592EB06321A2342');
        $this->addSql('DROP TABLE attributes');
        $this->addSql('DROP TABLE attribute_attribute_set');
        $this->addSql('DROP TABLE attributesets');
        $this->addSql('DROP INDEX IDX_D34A04AD321A2342 ON product');
        $this->addSql('ALTER TABLE product DROP attribute_set_id');
    }
}
