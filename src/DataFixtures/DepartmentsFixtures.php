<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Department;

class DepartmentsFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for($i = 0;$i < 2;$i++){
            $department = new Department();
            $department->setName("Department ".($i+1));

            $manager->persist($department);
        }

        $manager->flush();
    }
}
