<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OrderItemRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="orderitems")
 */
class OrderItem
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Order", inversedBy="items")
     */
    private $ord;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $product_name;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Product")
     */
    private $product;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $product_sku;

    /**
     * @ORM\Column(type="integer", options={"default":0})
     * @Assert\NotBlank()
     * @Assert\Type(
     *     type="numeric",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    private $qty_ordered;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, options={"default":0})
     * @Assert\NotBlank()
     * @Assert\Type(
     *     type="numeric",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    private $price;

    /**
     * @ORM\Column(type="integer", options={"default":0})
     * @Assert\NotBlank()
     * @Assert\Type(
     *     type="numeric",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    private $tax_percent;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, options={"default":0})
     * @Assert\NotBlank()
     * @Assert\Type(
     *     type="numeric",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    private $price_incl_tax;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, options={"default":0})
     * @Assert\NotBlank()
     * @Assert\Type(
     *     type="numeric",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    private $total;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, options={"default":0})
     * @Assert\NotBlank()
     * @Assert\Type(
     *     type="numeric",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    private $total_incl_tax;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Warehouse")
     */
    private $warehouse;

    /**
     * @ORM\Column(type="boolean", nullable=true,options={"default":0})
     */
    private $canceled;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOrd(): ?Order
    {
        return $this->ord;
    }

    public function setOrd(?Order $ord): self
    {
        $this->ord = $ord;

        return $this;
    }

    public function getProductName(): ?string
    {
        return $this->product_name;
    }

    public function setProductName(string $product_name): self
    {
        $this->product_name = $product_name;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getProductSku(): ?string
    {
        return $this->product_sku;
    }

    public function setProductSku(string $product_sku): self
    {
        $this->product_sku = $product_sku;

        return $this;
    }

    public function getQtyOrdered(): ?int
    {
        return $this->qty_ordered;
    }

    public function setQtyOrdered(int $qty_ordered): self
    {
        $this->qty_ordered = $qty_ordered;

        return $this;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice($price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getTaxPercent(): ?int
    {
        return $this->tax_percent;
    }

    public function setTaxPercent(int $tax_percent): self
    {
        $this->tax_percent = $tax_percent;

        return $this;
    }

    public function getPriceInclTax()
    {
        return $this->price_incl_tax;
    }

    public function setPriceInclTax($price_incl_tax): self
    {
        $this->price_incl_tax = $price_incl_tax;

        return $this;
    }

    public function getTotal()
    {
        return $this->total;
    }

    public function setTotal($total): self
    {
        $this->total = $total;

        return $this;
    }

    public function getTotalInclTax()
    {
        return $this->total_incl_tax;
    }

    public function setTotalInclTax($total_incl_tax): self
    {
        $this->total_incl_tax = $total_incl_tax;

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function setDefaultValues(){


        if(!$this->price){
            $this->price = 0;
        }

        if(!$this->tax_percent){
            $this->tax_percent = 0;
        }

        if(!$this->qty_ordered){
            $this->qty_ordered = 0;
        }

        if(!$this->total){
            $this->total = $this->price * $this->qty_ordered;
        }

        if(!$this->price_incl_tax){
            $this->price_incl_tax = $this->price * (1 + ($this->tax_percent/100));
        }

        if(!$this->total_incl_tax){
            $this->total_incl_tax = $this->tax_percent == 0 ? $this->total : ($this->total * (1 + ($this->tax_percent/100)) );
        }

        
    }

    public function getWarehouse(): ?Warehouse
    {
        return $this->warehouse;
    }

    public function setWarehouse(?Warehouse $warehouse): self
    {
        $this->warehouse = $warehouse;

        return $this;
    }

    public function getFormattedPrice(string $currency = ""):string{
        return number_format($this->price,2)." ".$currency;
    }

    public function getFormattedPriceInclTax(string $currency = ""):string{
        return number_format($this->price_incl_tax,2)." ".$currency;
    }

    public function getFormattedTotal(string $currency = ""):string{
        return number_format($this->total,2)." ".$currency;
    }

    public function getFormattedTotalInclTax(string $currency = ""):string{
        return number_format($this->total_incl_tax,2)." ".$currency;
    }

    public function getCanceled(): ?bool
    {
        return $this->canceled;
    }

    public function setCanceled(?bool $canceled): self
    {
        $this->canceled = $canceled;

        return $this;
    }
}
