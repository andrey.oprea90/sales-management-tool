<?php 
namespace App\Service;

use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Asset\Context\RequestStackContext;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Intervention\Image\ImageManager;
use League\Flysystem\Adapter\Local;
use League\Flysystem\Filesystem as Flysystem;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\COmponent\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\HeaderUtils;
use App\Entity\Files;

class FileHelper{

    private $fileSystem;

    private $flySystem;

    private $defaultUploadPath;

    private $defaultUploadDirectory;

    private $fullUploadPath;

    const ALLOWED_MIME_TYPES = [
        'image/*',
        'application/pdf',
        'application/msword',
        'application/vnd.ms-excel',
        'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        'application/vnd.openxmlformats-officedocument.presentationml.presentation',
        'text/plain'
    ];

    public function __construct(
        string $defaultUploadPath,
        string $defaultUploadDirectory
        ){

            $this->fileSystem = new Filesystem();
            $this->defaultUploadPath = $defaultUploadPath;
            $this->defaultUploadDirectory = $defaultUploadDirectory;
            $this->fullUploadPath = $this->defaultUploadPath.DIRECTORY_SEPARATOR.$this->defaultUploadDirectory;
            
            if(!file_exists($this->fullUploadPath)){
                $this->fileSystem->mkdir($defaultUploadPath);
            }

            $adapter = new Local("/");
            $this->flySystem = new Flysystem($adapter);
           
        
    }


    public function uploadFile(UploadedFile $file,$newName = null):array{
        $data = [];

        try{
           
            $filename = $file->getClientOriginalName() ?? $file->getFilename();
            $filename = pathinfo($filename,PATHINFO_FILENAME);
            $extension = $file->guessExtension();
            $mimeType = $file->getMimeType();
            
            $data = [
                'extension'=>$extension,
                'mime_type'=>$mimeType,
                'path'=>null
            ];

            $newName = $newName ? static::slugify(pathinfo($newName,PATHINFO_FILENAME)).".".$extension : static::generateUniqueName($filename).".".$extension;

            $savedFile = $file->move($this->fullUploadPath,$newName);

            if(!$savedFile instanceof File){
                throw new \Exception();
            }

            $data['name'] = pathinfo($newName,PATHINFO_FILENAME);
            $data['path'] = $this->defaultUploadDirectory.DIRECTORY_SEPARATOR.basename($savedFile->getPathname());
          
           
        }catch(\Exception $e){
           
            $data = [];

        }
      
        return $data;
        

    }

    public function deleteFile(File $file):bool{

       try{
            $path = $file->getPathname();
            if($this->fileSystem->exists($path)){
                $this->fileSystem->remove($path);

                return !$this->fileSystem->exists($path);
            }
       }catch(\Exception $e){
           return false;
       }
        
        return false;

    }

    public function copyFile(File $source,string $destination):bool{

        try{
            $this->fileSystem->copy($source->getPathname(),$destination);

            return $this->fileSystem->exists($destination);
        }catch(\Exception $e){
           
            return false;
        }
       
        return false;
    }

    public function resizeImage($path){
        //to do
    }


    public function readStream(string $path){
        $resource = $this->flySystem->readStream($path);

        if ($resource === false) {
            throw new \Exception(sprintf('Error opening stream for "%s"', $path));
        }

        return $resource;
    }


    public function uploadFileOptimized(File $file, string $newName = null): array{

        $data = [];

        try{

            if ($file instanceof UploadedFile) {
                $filename = $file->getClientOriginalName();
            } else {
                $filename = $file->getFilename();
            }
    
                $filename = pathinfo($filename,PATHINFO_FILENAME);
                $extension = $file->guessExtension();
                $mimeType = $file->getMimeType();
                
                $data = [
                    'extension'=>$extension,
                    'mime_type'=>$mimeType,
                    'path'=>null
                ];
           
            $newName = $newName ? static::slugify(pathinfo($newName,PATHINFO_FILENAME)).".".$extension : static::generateUniqueName($filename).".".$extension;
    
            $stream = fopen($file->getPathname(), 'r');
            $result = $this->flySystem->writeStream(
                $this->fullUploadPath.DIRECTORY_SEPARATOR.$newName,
                $stream
            );
    
            if ($result === false) {
                throw new \Exception(sprintf('Could not write uploaded file "%s"', $newFilename));
            }
    
            if (is_resource($stream)) {
                fclose($stream);
            }

            $data['name'] = pathinfo($newName,PATHINFO_FILENAME);
            $data['path'] = $this->defaultUploadDirectoryh.DIRECTORY_SEPARATOR.$newName;

          
        }catch(\Exception $e){
            
            $data = [];
        }



        return $data;
    }

    public function getFileFromEntity(Files $file):?File{

        $disk_path = $this->defaultUploadPath.DIRECTORY_SEPARATOR.$file->getPath();
        if($this->fileSystem->exists($disk_path)){

            return new File($disk_path);
        }

        return null;
    }

    public function streamedResponse(Files $file, string $disposition = HeaderUtils::DISPOSITION_ATTACHMENT){
        $diskFile = $this->getFileFromEntity($file);
        if($diskFile){
            $response = new StreamedResponse(function() use ($diskFile) {
                $outputStream = fopen('php://output', 'wb');
                $fileStream = $this->readStream($diskFile->getPathname(), false);
                stream_copy_to_stream($fileStream, $outputStream);
            });
           $response->headers->set("Content-Type",$diskFile->getMimeType());

           return $response;
        }else{
            $response = new Response();
        }

        return $response;

    }

    public static function slugify(string $string, string $separator = '-' ):string {
        $accents_regex = '~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i';
        $special_cases = array( '&' => 'and', "'" => '');
        $string = mb_strtolower( trim( $string ), 'UTF-8' );
        $string = str_replace( array_keys($special_cases), array_values( $special_cases), $string );
        $string = preg_replace( $accents_regex, '$1', htmlentities( $string, ENT_QUOTES, 'UTF-8' ) );
        $string = preg_replace("/[^a-z0-9]/u", "$separator", $string);
        $string = preg_replace("/[$separator]+/u", "$separator", $string);
        return $string;
    }

    public static function generateUniqueName(string $name):string{

        return static::slugify($name."-".md5(uniqid()));
    }

    

}