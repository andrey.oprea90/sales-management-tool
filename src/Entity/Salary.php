<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SalaryRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Salary
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date",nullable=true)
     */
    private $date_created;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $month_year_check;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"default":0})
     * @Assert\NotBlank()
     * @Assert\Type(
     *     type="numeric",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    private $hours_worked;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Employee", inversedBy="salaries")
     * @ORM\JoinColumn(nullable=false)
     */
    private $employee;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, nullable=true, options={"default":0})
     */
    private $total_paid;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateCreated(): ?\DateTimeInterface
    {
        return $this->date_created;
    }

    public function setDateCreated(?\DateTimeInterface $date_created): self
    {
        $this->date_created = $date_created;

        return $this;
    }

    public function getMonthYearCheck(): ?string
    {
        return $this->month_year_check;
    }

    public function setMonthYearCheck(?string $month_year_check): self
    {
        $this->month_year_check = $month_year_check;

        return $this;
    }

    public function getHoursWorked(): ?int
    {
        return $this->hours_worked;
    }

    public function setHoursWorked(?int $hours_worked): self
    {
        $this->hours_worked = $hours_worked;

        return $this;
    }

    public function getEmployee(): ?Employee
    {
        return $this->employee;
    }

    public function setEmployee(?Employee $employee): self
    {
        $this->employee = $employee;

        return $this;
    }

     /**
     * @ORM\PrePersist
     */
    public function setDefaultValues(){

        if(!$this->date_created){
            $this->date_created = new \DateTime();
        }

        if(!$this->hours_worked){
            $this->hours_worked = 0;
        }
        if(!$this->month_year_check){
            $this->month_year_check = $this->date_created->format("m-Y");
        }

        if(!$this->total_paid){
            $this->total_paid = 0;
        }
            
    }

    public function getTotalPaid()
    {
        return $this->total_paid;
    }

    public function setTotalPaid($total_paid): self
    {
        $this->total_paid = $total_paid;

        return $this;
    }

    public function getFormattedTotalPaid($currency = ''){
        return number_format($this->total_paid,2)." ".$currency;
    }
}
