<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FinancialRepository")
 */
class Financial
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank()
     */
    private $account;

    /**
     * @ORM\Column(type="decimal", nullable=true, precision=10, scale=2, options={"default":0})
     * @Assert\Type(
     *     type="numeric",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    private $hours_per_week;

    /**
     * @ORM\Column(type="decimal", nullable=true, precision=10, scale=2, options={"default":0})
     * @Assert\Type(
     *     type="numeric",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    private $payment_per_hour;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAccount(): ?string
    {
        return $this->account;
    }

    public function setAccount(?string $account): self
    {
        $this->account = $account;

        return $this;
    }

    public function getHoursPerWeek(): ?float
    {
        return $this->hours_per_week;
    }

    public function setHoursPerWeek($hours_per_week): self
    {
        $this->hours_per_week = $hours_per_week;

        return $this;
    }

    public function getPaymentPerHour(): ?float
    {
        return $this->payment_per_hour;
    }

    public function setPaymentPerHour($payment_per_hour): self
    {
        $this->payment_per_hour = $payment_per_hour;

        return $this;
    }
}
