<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Industry;

class IndustryFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $it = new Industry();
        $it->setType("IT");

        $telecom = new Industry();
        $telecom->setType("Telecom");

        $manager->persist($it);
        $manager->persist($telecom);

        $manager->flush();
    }
}
