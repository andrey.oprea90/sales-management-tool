<?php 
namespace App\Service;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class ApiHelper{

    private $token_storage;

    private $encoder;

    private $tr;

    private $session;
    
    public function __construct(TokenStorageInterface $tokenStorage,
                                JWTEncoderInterface $encoder,
                                SessionInterface $session,
                                $tr){
        $this->token_storage = $tokenStorage;
        $this->encoder = $encoder;
        $this->tr = $tr;
        $this->session = $session;
    }

    public function getApiToken(){
        $token = "";
      
            $old = $this->session->get("api_token",null);
            if(!empty($old)){
                $valid = $this->checkAuth($old);
                if($valid){
                    $token = $old;
                    return $token;
                }
            }
              
        try{
            $user = $this->token_storage->getToken()->getUser();
            if(!$user){
                throw new \Exception($this->tr->trans("Invalid user"));
            }
            $token = $this->encoder->encode([
                "username"=>$user->getUsername(),
                "exp"=>time() + 3600
            ]);
            $this->session->set("api_token",$token);
            return $token;
        }catch(\Exception $e){
           
            return "";
        }
    }

    public function checkAuth(string $old):bool{

        try{
 
           $this->encoder->decode($old);
        }catch(\Exception $e){
            
            return false;
        }

        return true;
    }
}