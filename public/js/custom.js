


$(document).ready(function(){

  $(".hs-menubar").hsMenu(); 

  $(".submenu > a").click(function(e) {
    e.preventDefault();
    var $li = $(this).parent("li");
    var $ul = $(this).next("ul");

    if($li.hasClass("open")) {
      $ul.slideUp(350);
      $li.removeClass("open");
    } else {
      $(".nav > li > ul").slideUp(350);
      $(".nav > li").removeClass("open");
      $ul.slideDown(350);
      $li.addClass("open");
    }
  });

  var header_height = $("header").height();
  var footer_height = $("footer").height();

  $(".page-content").css({
    "min-height":($(window).height() - header_height - footer_height)
  });

  // var current_url = window.location.href;

  // $("ul.nav-links li a").each(function(){
  //   var href = $(this).attr("href");
   
  //   if(href != "/" && current_url.indexOf(href) !== -1){
  //     $("ul.nav-links li").removeClass("current");
  //     $(this).parent().addClass("current");
      
  //   }
  // });

});

function spinner(id){
  var opts = {
    lines: 13, // The number of lines to draw
    length: 38, // The length of each line
    width: 17, // The line thickness
    radius: 45, // The radius of the inner circle
    scale: 1, // Scales overall size of the spinner
    corners: 1, // Corner roundness (0..1)
    color: '#c0c0c0', // CSS color or array of colors
    fadeColor: 'transparent', // CSS color or array of colors
    speed: 1, // Rounds per second
    rotate: 0, // The rotation offset
    animation: 'spinner-line-fade-quick', // The CSS animation name for the lines
    direction: 1, // 1: clockwise, -1: counterclockwise
    zIndex: 2e9, // The z-index (defaults to 2000000000)
    className: 'spinner', // The CSS class to assign to the spinner
    top: '50%', // Top position relative to parent
    left: '50%', // Left position relative to parent
    shadow: '0 0 1px transparent', // Box-shadow for the lines
    position: 'absolute' // Element positioning
  };
  
  var target = document.getElementById(id);
  if(!target){
    return;
  }
  var spinner = new Spinner(opts).spin(target);

  return spinner;
}