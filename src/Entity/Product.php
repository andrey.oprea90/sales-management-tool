<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use App\Service\FileHelper;


/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity(
 *     fields={"sku"},
 *     message="Sku already exists"
 * )
 */
class Product
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = 2,
     *      max = 200,
     *      minMessage = "Product title must be at least {{ limit }} characters long",
     *      maxMessage = "Product title cannot be longer than {{ limit }} characters"
     * )
     */
    private $title;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Assert\Length(
     *      min = 4,
     *      max = 2000,
     *      minMessage = "Product description must be at least {{ limit }} characters long",
     *      maxMessage = "Product description cannot be longer than {{ limit }} characters"
     * )
     */
    private $description;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"default":0,"unsigned":true})
     * @Assert\NotBlank()
     * @Assert\Type(
     *     type="numeric",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    private $qty;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, options={"default":0,"unsigned":true})
     * @Assert\NotBlank()
     * @Assert\Type(
     *     type="numeric",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    private $price;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, options={"default":0,"unsigned":true})
     * @Assert\Type(
     *     type="numeric",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    private $special_price;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $main_image;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Category", mappedBy="products",cascade={"detach"})
     */
    private $categories;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\WarehouseProduct", mappedBy="product", orphanRemoval=true)
     */
    private $wharehouseProduct;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2,options={"default":0,"unsigned":true})
     * @Assert\NotBlank()
     * @Assert\Type(
     *     type="numeric",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    private $buy_price;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Brand")
     */
    private $brand;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Manufacturer")
     */
    private $manufacturer;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Tax")
     */
    private $tax;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * Assert\NotBlank()   
     * @Assert\Length(
     *      min = 2,
     *      max = 200,
     *      minMessage = "Product sku must be at least {{ limit }} characters long",
     *      maxMessage = "Product sku cannot be longer than {{ limit }} characters"
     * )
     */
    private $sku;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deleted_at;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Type(
     *     type="numeric",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    private $weight;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Color")
     */
    private $color;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $width;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $height;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default":1})

     * @Assert\Choice({true,false}, message="Choose a valid status")
     */
    private $in_stock;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\AttributeSet")
     */
    private $attribute_set;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $extra_attributes = [];

    

    public function __construct()
    {
        $this->categories = new ArrayCollection();
        $this->wharehouseProduct = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getQty(): ?int
    {
        return $this->qty;
    }

    public function setQty(?int $qty): self
    {
        $this->qty = $qty;

        return $this;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice($price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getSpecialPrice()
    {
        return $this->special_price;
    }

    public function setSpecialPrice($special_price): self
    {
        $this->special_price = $special_price;

        return $this;
    }

    public function getMainImage()
    {
        return $this->main_image;
    }

    public function setMainImage($main_image): self
    {
        $this->main_image = $main_image;

        return $this;
    }

    /**
     * @return Collection|Category[]
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }

    public function addCategory(Category $category): self
    {
        if (!$this->categories->contains($category)) {
            $this->categories[] = $category;
            $category->addProduct($this);
        }

        return $this;
    }

    public function removeCategory(Category $category): self
    {
        if ($this->categories->contains($category)) {
            $this->categories->removeElement($category);
            $category->removeProduct($this);
        }

        return $this;
    }

    /**
     * @return Collection|WarehouseProduct[]
     */
    public function getWharehouseProduct(): Collection
    {
        return $this->wharehouseProduct;
    }

    public function addWharehouseProduct(WarehouseProduct $wharehouseProduct): self
    {
        if (!$this->wharehouseProduct->contains($wharehouseProduct)) {
            $this->wharehouseProduct[] = $wharehouseProduct;
            $wharehouseProduct->setProduct($this);
        }

        return $this;
    }

    public function removeWharehouseProduct(WarehouseProduct $wharehouseProduct): self
    {
        if ($this->wharehouseProduct->contains($wharehouseProduct)) {
            $this->wharehouseProduct->removeElement($wharehouseProduct);
            // set the owning side to null (unless already changed)
            if ($wharehouseProduct->getProduct() === $this) {
                $wharehouseProduct->setProduct(null);
            }
        }

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(?\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(?\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue(){

        // $this->created_at = new \DateTime();
        // $this->updated_at = new \DateTime();

        if(!$this->price){
            $this->price = 0;
        }

        if(!$this->special_price){
            $this->special_price = 0;
        }

        if(!$this->buy_price){
            $this->buy_price = 0;
        }

        if(!$this->sku){
           
            $this->sku = substr(FileHelper::slugify($this->title)."-".md5(uniqid()),0,200);
        }else{
           
            $this->sku = substr(FileHelper::slugify($this->sku),0,200);
        }

        
        
    }

     /**
     * @ORM\PreUpdate
     */
    public function setUpdatedAtValue(){

      //  $this->updated_at = new \DateTime();

        if(!$this->price){
            $this->price = 0;
        }

        if(!$this->special_price){
            $this->special_price = 0;
        }

        if(!$this->buy_price){
            $this->buy_price = 0;
        }

        if(!$this->sku){
           
            $this->sku = substr(FileHelper::slugify($this->title)."-".md5(uniqid()),0,200);
        }else{
            
            $this->sku = substr(FileHelper::slugify($this->sku),0,200);
        }
        
        
    }

    /**
     * @ORM\PreFlush
     */

    public function setDefaultSkuValue(){
        if(!$this->sku){
           
            $this->sku = substr(FileHelper::slugify($this->title)."-".md5(uniqid()),0,200);
        }else{
            
            $this->sku = substr(FileHelper::slugify($this->sku),0,200);
        }

        if(!is_numeric($this->qty) || $this->qty < 0){
            $this->qty = 0;
            $this->in_stock = false;
        }

        
    }

    public function getBuyPrice()
    {
        return $this->buy_price;
    }

    public function setBuyPrice($buy_price): self
    {
        $this->buy_price = $buy_price;

        return $this;
    }

    public function getImage(?string $checkPath,string $uploadsFolder = 'uploads/products'):string{
         
        $assetPath = 'uploads/placeholder.jpg';
        if(!empty($this->main_image)){
            if($checkPath){
                if(is_file($checkPath.DIRECTORY_SEPARATOR.$this->main_image)){
                    return $uploadsFolder."/".$this->main_image;
                }
            }else{
                return $uploadsFolder."/".$this->main_image;
            }
        }
        return $assetPath;
    }

    public function getBrand(): ?Brand
    {
        return $this->brand;
    }

    public function setBrand(?Brand $brand): self
    {
        $this->brand = $brand;

        return $this;
    }

    public function getManufacturer(): ?Manufacturer
    {
        return $this->manufacturer;
    }

    public function setManufacturer(?Manufacturer $manufacturer): self
    {
        $this->manufacturer = $manufacturer;

        return $this;
    }

    public function getTax(): ?Tax
    {
        return $this->tax;
    }

    public function setTax(?Tax $tax): self
    {
        $this->tax = $tax;

        return $this;
    }

    public function getSku(): ?string
    {
        return $this->sku;
    }

    public function setSku(?string $sku): self
    {
        $this->sku = $sku;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deleted_at;
    }

    public function setDeletedAt(?\DateTimeInterface $deleted_at): self
    {
        $this->deleted_at = $deleted_at;

        return $this;
    }

    public function getSellPrice():float{
        return (float) (is_numeric($this->special_price) && $this->special_price > 0?$this->special_price:$this->price);
    }

    public function getFormattedSellPrice($currency = ''){
        return number_format($this->getSellPrice(),2)." ".$currency;
    }

    public function getFormattedBuyPrice($currency = ''){
        return number_format($this->buy_price,2)." ".$currency;
    }

    public function getWeight(): ?string
    {
        return $this->weight;
    }

    public function setWeight(?string $weight): self
    {
        $this->weight = $weight;

        return $this;
    }

    public function getColor(): ?Color
    {
        return $this->color;
    }

    public function setColor(?Color $color): self
    {
        $this->color = $color;

        return $this;
    }

    public function getWidth(): ?string
    {
        return $this->width;
    }

    public function setWidth(?string $width): self
    {
        $this->width = $width;

        return $this;
    }

    public function getHeight(): ?string
    {
        return $this->height;
    }

    public function setHeight(?string $height): self
    {
        $this->height = $height;

        return $this;
    }

    public function getInStock(): ?bool
    {
        return $this->in_stock;
    }

    public function setInStock(?bool $in_stock): self
    {
        $this->in_stock = $in_stock;

        return $this;
    }

    public function getAttributeSet(): ?AttributeSet
    {
        return $this->attribute_set;
    }

    public function setAttributeSet(?AttributeSet $attribute_set): self
    {
        $this->attribute_set = $attribute_set;

        return $this;
    }

    public function getExtraAttributes(): ?array
    {
        return $this->extra_attributes;
    }

    public function setExtraAttributes(?array $extra_attributes): self
    {
        $this->extra_attributes = $extra_attributes;

        return $this;
    }

    public function __toString(){

        return static::class;
    }

    
}
