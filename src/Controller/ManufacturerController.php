<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use App\Entity\Manufacturer;
use App\Repository\ManufacturerRepository;
use App\Form\ManufacturerFormType;
use AndreaSprega\Bundle\BreadcrumbBundle\Annotation\Breadcrumb;

/**
 * IsGranted("ROLE_ADMIN_MANUFACTURERS")
 * @Breadcrumb({"label" = "Manufacturer list", "route" = "manufacturer" })
 */
class ManufacturerController extends AbstractController
{   
    
    /**
    * @Route("/manufacturer/{page}", name="manufacturer", defaults={"page":"1"}, requirements={"page":"\d+"})
    */
   public function index(Request $request, 
                        ManufacturerRepository $manufacturerRepo,
                       $page
                       )
   {
       
       $manufacturers = $manufacturerRepo->findAllPaginated($page,$this->getParameter('default_per_page'));

       return $this->render('manufacturer/index.html.twig', [
           'page_title' => 'Manufacturer list',
           'manufacturers'=>$manufacturers
       ]);
   }

   /**
    * @Route("/manufacturer/add", name="manufacturer_add")
    */
   public function add(Request $request,
                       EntityManagerInterface $em,
                       $tr){

       try{
           $manufacturer = new Manufacturer();
           $form = $this->createForm(ManufacturerFormType::class,$manufacturer);

           $form->handleRequest($request);

           if($form->isSubmitted() && $form->isValid()){

               $em->persist($manufacturer);
               $em->flush();

               $this->addFlash("success",$tr->trans("Manufacturer saved"));
               return $this->redirectToRoute("manufacturer");
           }
       }catch(\Exception $e){
          
           $this->addFlash("error",$e->getMessage());
           return $this->redirectToRoute("manufacturer");
       }
       

       return $this->render('manufacturer/add.html.twig',[
           'page_title'=>'Add manufacturer',
           'form'=>$form->createView()
       ]);
   }

 

   /**
    * @Route("/manufacturer/delete/{id}", name="manufacturer_delete", requirements={"id":"\d+"})
    */
   public function delete(Request $request,
                          EntityManagerInterface $em,
                          Manufacturer $manufacturer,
                          $tr
                          ){

       try{
           
           $em->remove($manufacturer);
           $em->flush();

           $this->addFlash("success",$tr->trans("Manufacturer deleted"));
           return $this->redirectToRoute("manufacturer");

       }catch(\Exception $e){

           $this->addFlash("error",$e->getMessage());
           return $this->redirectToRoute("manufacturer");
       }          

   }
}
