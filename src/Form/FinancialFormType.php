<?php

namespace App\Form;

use App\Entity\Financial;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type as Type;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\CallbackTransformer;

class FinancialFormType extends AbstractType
{
    private $tr;
    
    public function __construct($tr){
        $this->tr = $tr;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('account',Type\TextType::class,[
                'empty_data'=>"",
                'label'=>$this->tr->trans('Bank account')
            ])
            ->add('hours_per_week',Type\TextType::class,[
                'empty_data'=>0,
                'label'=>$this->tr->trans('Hours per week')
            ])
            ->add('payment_per_hour',Type\TextType::class,[
                'empty_data'=>0,
                'label'=>$this->tr->trans('Billing per hour')
            ])
        ;

        // $builder->get('hours_per_week')->addModelTransformer(
        //     new CallbackTransformer(
        //         function($value){
        //            return (float) $value;
        // },function($value){
        //     return (float) $value;
        // }));

        // $builder->addEventListener(
        //     FormEvents::PRE_SET_DATA,
        //     function (FormEvent $event) {
        //         /** @var Article|null $data */
        //         $data = $event->getData();
        //         dd($data);
        //         if (!$data) {
        //             return;
        //         }

               
        //     }
        // );

        // $builder->get('hours_per_week')->addEventListener(
        //     FormEvents::POST_SUBMIT,
        //     function(FormEvent $event) {
        //         $form = $event->getForm();
                
               
        //     }
        // );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Financial::class,
        ]);
    }
}
