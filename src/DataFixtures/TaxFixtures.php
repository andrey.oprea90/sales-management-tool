<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Tax;

class TaxFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $tax = new Tax();
        $tax->setName("Starndard rate");
        $tax->setPercent(19);
        $manager->persist($tax);
        
        $manager->flush();
    }
}
