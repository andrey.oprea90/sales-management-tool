<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use App\Service\FileHelper;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AttributeRepository")
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity(
 *     fields={"name"},
 *     message="Attribute already exists"
 * )
 * @ORM\Table(name="attributes")
 */
class Attribute
{
    const TYPES = [
        'text'=>'Text',
        'textarea'=>'Textarea',
        'dropdown'=>'Dropdown',
        'dropdown_multiple'=>'Multiple dropdown'
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Please choose a name")
     * @Assert\Length(
     *      min = 2,
     *      max = 200,
     *      minMessage = "Attribute name must be at least {{ limit }} characters long",
     *      maxMessage = "Attribute name cannot be longer than {{ limit }} characters"
     * )
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $code;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\AttributeSet", inversedBy="attributes", cascade={"detach"})
     */
    private $attribute_set;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Please select a type")
     * @Assert\Choice({"text","textarea","dropdown","dropdown_multiple"})
     */
    private $type;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $options = [];

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $value = [];

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank(message="Please choose a label")
     * @Assert\Length(
     *      min = 2,
     *      max = 200,
     *      minMessage = "Label must be at least {{ limit }} characters long",
     *      maxMessage = "Label cannot be longer than {{ limit }} characters"
     * )
     */
    private $label;

    public function __construct()
    {
        $this->attribute_set = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): self
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return Collection|AttributeSet[]
     */
    public function getAttributeSet(): Collection
    {
        return $this->attribute_set;
    }

    public function addAttributeSet(AttributeSet $attributeSet): self
    {
        if (!$this->attribute_set->contains($attributeSet)) {
            $this->attribute_set[] = $attributeSet;
        }

        return $this;
    }

    public function removeAttributeSet(AttributeSet $attributeSet): self
    {
        if ($this->attribute_set->contains($attributeSet)) {
            $this->attribute_set->removeElement($attributeSet);
        }

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getOptions(): ?array
    {
        return $this->options;
    }

    public function setOptions(?array $options): self
    {
        $this->options = $options;

        return $this;
    }

    public function getValue(): ?array
    {
        return $this->value;
    }

    public function setValue(?array $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(?string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getFormattedType():string{
      
        return static::TYPES[$this->type] ?? '';
    }

    /**
     * @ORM\PreFlush
     */

    public function setAutomaticValues(){

        if(!$this->code){
           
            $this->code = str_replace("-","_",FileHelper::slugify($this->name));
        }
        
    }

    public function isDropdown(){
        return in_array($this->type,["dropdown","dropdown_multiple"]);
    }
}
