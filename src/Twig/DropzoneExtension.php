<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;
use Symfony\Component\Templating\EngineInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;


class DropzoneExtension extends AbstractExtension
{
    private $templateEngine;

    public function __construct(EngineInterface $engine,UrlGeneratorInterface $generator){
        $this->templateEngine = $engine;
        $this->generator = $generator;
    }

    public function getFilters(): array
    {
        return [
            // If your filter generates SAFE HTML, you should add a third
            // parameter: ['is_safe' => ['html']]
            // Reference: https://twig.symfony.com/doc/2.x/advanced.html#automatic-escaping
           // new TwigFilter('filter_name', [$this, 'doSomething']),
        ];
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('generate_dropzone', [$this, 'generateDropzone'],['is_safe'=>['html']]),
        ];
    }

    public function generateDropzone($entity_type,$entity_id = 0,$token = null,array $options = [])
    {
        $entity_type =(string) $entity_type;
        
        $defaultOptions = [
            "singular"=>false,
            "width"=>null,
            "height"=>null,
            "action"=>$this->generator->generate("files_upload"),
            "dropzone_class"=>"js-reference-dropzone",
            "file_name"=>"file",
            "fetch_url"=>$this->generator->generate("files_fetch"),
            "container_append_class"=>"js-append-files",
            "error_message"=>"No files found",
            "delete_url"=>preg_replace("#\d+#","",$this->generator->generate("files_delete",[
                "id"=>0
            ])),
            "message_container_class"=>"file-messages"
        ];

        $options = array_merge($defaultOptions,$options);
       
        return $this->templateEngine->render('dropzone/dropzone.html.twig',[
            'entity_type'=>$entity_type,
            'entity_id'=>$entity_id,
            'token'=>$token,
            'options'=>$options
        ]);
        
    }

   
}
