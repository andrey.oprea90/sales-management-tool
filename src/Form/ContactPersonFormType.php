<?php

namespace App\Form;

use App\Entity\ContactPerson;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type as Type;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints as Assert;

class ContactPersonFormType extends AbstractType
{
    private $tr;
    
    public function __construct($tr){
        $this->tr = $tr;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',Type\TextType::class,[
                'label'=>$this->tr->trans('Name'),
                'empty_data'=>''
            ])
            ->add('email',Type\TextType::class,[
                'label'=>$this->tr->trans('email'),
                'empty_data'=>''
            ])
            ->add('phone',Type\TextType::class,[
                'label'=>$this->tr->trans('phone'),
                'empty_data'=>''
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ContactPerson::class,
        ]);
    }
}
