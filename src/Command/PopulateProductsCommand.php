<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\Entity\Product;
use App\Entity\Warehouse;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use App\Service\StockMovementHelper;
use Carbon\Carbon;
use App\Repository\WarehouseRepository;
use App\Repository\TaxRepository;


class PopulateProductsCommand extends Command
{
    public function __construct(EntityManagerInterface $em,
                                StockMovementHelper $stockHelper,
                                WarehouseRepository $warehouseRepo,
                                TaxRepository $taxRepo){

        $this->em = $em;
        $this->stockHelper = $stockHelper;
        $this->warehouseRepo = $warehouseRepo;
        $this->taxRepo = $taxRepo;

        parent::__construct();
    }

    protected static $defaultName = 'populate:products';

    protected function configure()
    {
        $this
            ->setDescription('Populates db with test products')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->note('Starting product creation process');
        $this->createProducts($output,5000);
        $io->success('Product creation process finished');
    }

    private function createProducts(OutputInterface $output,int $total):bool{

        $progressBar = new ProgressBar($output,$total);
        $progressBar->start();
        $days = range(1,360);
        $warehouses = $this->warehouseRepo->findAll();
        $taxes = $this->taxRepo->findAll();
        
        try{
            for($i = 0;$i < $total;$i++){
                $output->writeln("Starting creating product...Product {$i}");
                $this->em->transactional(function($em) use ($warehouses,$days,$i,$taxes) {
                    $day = array_rand($days);
                    $date = new \DateTime("2019-01-01");
                    $date->add(new \DateInterval("P{$day}D"));
                    $randomTitle = "Product ".$this->readableRandomString();
                    $product = new Product();
                    $product->setTitle($randomTitle);
                    $product->setQty(80);
                    $product->setBuyPrice(20);
                    $product->setPrice(40);
                    $product->setTax($taxes[0] ?? null);
                    $product->setInStock(1);
                    $product->setCreatedAt($date);
                    $product->setUpdatedAt($date);
                    $em->persist($product);
                    $em->flush();
                    $this->stockHelper->setProduct($product);
                    foreach($warehouses as $warehouse){
                        $this->stockHelper->moveStock(null,$warehouse,20);
                    }
                });
               
                $output->writeln("Product created succesfully");

                $progressBar->advance();
            }
        }catch(\Exception $e){
           
            return false;
        }
       
        $progressBar->finish();
        return true;
    }

    private function readableRandomString($length = 6)
    {  
        $string     = '';
        $vowels     = array("a","e","i","o","u");  
        $consonants = array(
            'b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 
            'n', 'p', 'r', 's', 't', 'v', 'w', 'x', 'y', 'z'
        );  
        // Seed it
        srand((double) microtime() * 1000000);
        $max = $length/2;
        for ($i = 1; $i <= $max; $i++)
        {
            $string .= $consonants[rand(0,19)];
            $string .= $vowels[rand(0,4)];
        }
        return $string;
    }
}
