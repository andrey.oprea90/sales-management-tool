<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use App\Entity\Order;
use App\Entity\OrderItem;
use App\Entity\Product;
use App\Repository\OrderRepository;
use App\Repository\OrderItemRepository;
use App\Repository\ProductRepository;
use App\Repository\WarehouseRepository;
use App\Form\OrderFormType;
use App\Form\OrderItemMovementFormType;
use App\Service\OrderHelper;
use App\Service\StockMovementHelper;
use App\Form\DataObjects\OrderItemMovement;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Dompdf\Dompdf;
use App\Entity\Invoice;
use App\Repository\InvoiceRepository;
use App\Form\InvoiceFormType;
use Symfony\Component\Validator\Constraints as Assert;
use AndreaSprega\Bundle\BreadcrumbBundle\Annotation\Breadcrumb;
use App\Repository\SettingRepository;
use App\Repository\UserRepository;

/**
 * @IsGranted("ROLE_CUSTOMER")
 * @Breadcrumb({"label" = "Orders list", "route" = "orders" })
 */
class OrderController extends AbstractController
{
    /**
     * @Route("/orders/{page}", name="orders", defaults={"page":"1"}, requirements={"page":"\d+"})
     */
    public function index(Request $request,
                          EntityManagerInterface $em,
                          OrderRepository $orderRepo,
                          ValidatorInterface $validator,
                          $page
                          )
    {
        
        try{
            
            $filters = $this->getFiltersFromRequest($request,$validator);
            
        }catch(\Throwable $t){
            $filters = [];
        }
       
        $user = $this->getUser();
    
        $orders = $orderRepo->findAllPaginated($page,$this->getParameter('default_per_page'),$filters,$user);
       $hideCustomerFilter = $this->getUser() && $this->getUser()->isOnlyCustomer();
        return $this->render('orders/index.html.twig', [
            'page_title' => 'Orders list',
            'orders'=>$orders,
            'hideCustomerFilter'=>$hideCustomerFilter
        ]);
    }

    /**
     * @IsGranted("ROLE_ADMIN_ORDERS")
     * @Route("/orders/add", name="orders_add")
     */
    public function add(Request $request,
                          EntityManagerInterface $em,
                          OrderRepository $orderRepo,
                          OrderHelper $orderHelper,
                          $tr
                          )
    {
        try{
            $order = new Order();
            $form = $this->createForm(OrderFormType::class,$order);
            $form->handleRequest($request);
            
            if($form->isSubmitted() && $form->isValid()){
                $client = $form->getData()->getClient();
                $customer = $form->getData()->getCustomer();

                if((!$client) && (!$customer)){
                    throw new \Exception($tr->trans("Please choose client ar customer"));
                }
                if($client && $customer){
                    throw new \Exception($tr->trans("Please choose client or customer, not both"));
                }
                //$products = $form->get('products')->getData();
                
                $em->persist($order);
                // foreach($products as $prod){
                //     $orderItem = $orderHelper->createOrderItem($order,$prod);
                //     $em->persist($orderItem);
                   
                // }
                $em->flush();
                $this->addFlash("success",$tr->trans("Order created"));
                return $this->redirectToRoute("orders_edit",[
                    "id"=>$order->getId()
                ]);
               
            }

        }catch(\Exception $e){
          
            $this->addFlash("error",$e->getMessage());
            return $this->redirectToRoute("orders_add");
        }
        
       
        return $this->render('orders/add.html.twig', [
            'page_title' => 'Add order',
            'form'=>$form->createView()
            
        ]);
    }

     /**
     * @Route("/orders/edit/{id}", name="orders_edit", requirements={"id":"\d+"})
     */
    public function edit(Request $request,
                          EntityManagerInterface $em,
                          OrderRepository $orderRepo,
                          OrderItemRepository $orderItemRepo,
                          OrderHelper $orderHelper,
                          Order $order,
                          $tr
                          )
    {
        $this->denyAccessUnlessGranted('ORDER_VIEW',$order);
        try{
           
            $form = $this->createForm(OrderFormType::class,$order);
            $form->handleRequest($request);
            $emptyItems = $orderItemRepo->getEmptyItems($order);

            $orderItemMovement = new OrderItemMovement();
            $orderItemMovementForm = $this->createForm(OrderItemMovementFormType::class,$orderItemMovement);

            if($request->isMethod("GET")){
                foreach($emptyItems as $ei){
                    $em->remove($ei);
                }
                $em->flush();
            }
            
            if($form->isSubmitted() && $form->isValid()){
               if($order->getStatus() == "complete" && $order->getInvoices()->count() < 1){
                   throw new \Exception($tr->trans("Please make invoice first"));
               }
               if($order->getStatus() == Order::STATUS_CANCELED){
                   $orderHelper->cancelOrder($order);
               }
               $em->flush();
            }

        }catch(\Exception $e){
          
            $this->addFlash("error",$e->getMessage());
            return $this->redirectToRoute("orders");
        }
        
       
        return $this->render('orders/edit.html.twig', [
            'page_title' => 'Manage order',
            'form'=>$form->createView(),
            'order'=>$order,
            'orderItemMovementForm'=>$orderItemMovementForm->createView()
            
        ]);
    }

     /**
     * @IsGranted("ROLE_ADMIN_ORDERS")
     * @Route("/orders/edit/{id}/items/add", name="orders_add_items", requirements={"id":"\d+"})
     */
    public function addItems(Request $request,
                          EntityManagerInterface $em,
                          OrderRepository $orderRepo,
                          OrderHelper $orderHelper,
                          Order $order,
                          StockMovementHelper $stockHelper,
                          ProductRepository $productRepository,
                          WarehouseRepository $warehouseRepository,
                          ValidatorInterface $validator,
                          $tr
                          )
    {
        try{
            
            if(!$request->isXmlHttpRequest()){
                throw new \Exception($tr->trans("Invalid request"));
            }

            $orderItemMovement = new OrderItemMovement();
            $form = $this->createForm(OrderItemMovementFormType::class,$orderItemMovement);
            $form->handleRequest($request);
            $errorMessages = [];
            if(!$form->isValid()){
                $errors = $validator->validate($orderItemMovement);
                if($errors->count() > 0){
                    foreach($errors as $error){
                       $errorMessages[] = $error->getMessage();
                    }
                }
            }
            
            if(!empty($errorMessages)){
                throw new \Exception(implode(".",$errorMessages));
            }
           
           $warehouse = $orderItemMovement->fromWarehouse;
           $product = $orderItemMovement->product;
           $qty = (int) $orderItemMovement->qty ?? 0;

           $orderItem = $orderHelper->getOrderItemOrNew($order,$product,$warehouse);
           $orderHelper->updateOrderItem($orderItem,$qty);
            // $product = $productRepository->find(4);
            // $warehouse = $warehouseRepository->find(5);
            // $stockHelper->setProduct($product);
            // $stockHelper->removeStockFromWarehouse($warehouse,2);
        //     $qty = 2;
        //    $orderItem = $orderHelper->getOrderItemOrNew($order,$product,$warehouse);
           //$orderHelper->removeOrderItem($orderItem);
         // $orderHelper->cancelOrderItem($orderItem);
         //   $orderHelper->updateOrderItem($orderItem,$qty);
            // dd($product,$warehouse,$order);

         
            return $this->json([
                "success"=>true,
                "message"=>"Item added"
            ]);
        }catch(\Exception $e){
          
            return $this->json([
                'success'=>false,
                'message'=>$e->getMessage()
                
            ]);
        }
        
       
        
    }

    /**
     * @IsGranted("ROLE_ADMIN_ORDERS")
     * @Route("/orders/edit/{id}/items/remove/{item_id}", name="orders_remove_items", requirements={"id":"\d+","item_id":"\d+"})
     * @ParamConverter("orderItem", class="App:OrderItem", options={"id"="item_id"})
     */
    public function removeItem(Request $request,
                          EntityManagerInterface $em,
                          OrderHelper $orderHelper,
                          Order $order,
                          OrderItem $orderItem,
                          $tr
                          )
    {
        try{
            
            if(!$request->isXmlHttpRequest()){
                throw new \Exception($tr->trans("Invalid request"));
            }

            $orderHelper->removeOrderItem($orderItem);
         
            return $this->json([
                "success"=>true,
                "message"=>"Item removed"
            ]);
        }catch(\Exception $e){
          
            return $this->json([
                'success'=>false,
                'message'=>$e->getMessage()
                
            ]);
        }
        
       
        
    }

      /**
     * @Route("/orders/getItems/{id}", name="orders_get_items", requirements={"id":"\d+"})
     */
    public function getItems(Request $request,
                                EntityManagerInterface $em,
                                Order $order,
                                OrderRepository $orderRepository,
                                StockMovementHelper $helper,
                                OrderItemRepository $orderItemRepo,
                                $tr
                                ){

        try{
            $this->denyAccessUnlessGranted('ORDER_VIEW',$order);
            if(!$request->isXmlHttpRequest()){
                throw new \Exception($tr->trans("Invalid request"));
            } 

            $items = $orderItemRepo->getItems($order);
         
            $html = $this->renderView('orders/items/ajax/_items_table.html.twig',[
                'items'=>$items
            ]);

            return $this->json([
                "success"=>true,
                "html"=>$html
            ]);
        }catch(\Exception $e){
         
            return $this->json([
                'success'=>false,
                'html'=>""
                
            ]);
        }                            
         
        
        

    }

     /**
     * @IsGranted("ROLE_ADMIN_ORDERS") 
     * @Route("/orders/edit/{id}/invoice/create", name="orders_invoice_create", requirements={"id":"\d+"})
     * @Breadcrumb({"label" = "Edit order", "route" = "orders_edit" })
     */
    public function createInvoice(Request $request,
                                EntityManagerInterface $em,
                                Order $order,
                                OrderRepository $orderRepo,
                                OrderItemRepository $orderItemRepo,
                                OrderHelper $orderHelper,
                                SettingRepository $settingRepo,
                                $tr
                                ){

        try{
            
            if(in_array($order->getStatus(),["canceled"])){
                throw new \Exception($tr->trans("Cannot create invoice on canceled orders"));
            }

            if(count($order->getInvoices()) > 0){
                throw new \Exception($tr->trans("Order already has an invoice"));
            }
            $invoice = new Invoice();
            $defaultInvoiceSeries = $this->getParameter('default_invoice_series');
            $setting = $settingRepo->getFirstOrNew();
            if($setting->getId()){
                if($setting->getDefaultInvoiceSeries()){
                    $defaultInvoiceSeries = $setting->getDefaultInvoiceSeries();
                }
            }
            
            $form = $this->createForm(InvoiceFormType::class,$invoice);
           
            $form->handleRequest($request);
            $dueDate = $request->request->get("invoice_form")['due_date']??null;

            $invoiceNumber = $orderHelper->generateUniqueInvoiceNumber();
            $invoice->setNumber($invoiceNumber);
            if($form->isSubmitted() && $form->isValid()){
                
                if($dueDate){
                    $invoice->setDueDate(new \DateTime($dueDate));
                }
                
                $invoice->setOrd($order);
                $invoice->setPartial(false);
                $invoice->setTotalPaid($order->getTotalInclTax());
                $order->setStatus("complete");
                if(!$invoice->getSeries()){
                    $invoice->setSeries($defaultInvoiceSeries);
                }
                
                $em->persist($invoice);
                $em->flush();

                $this->addFlash("success",$tr->trans("Invoice created"));
                return $this->redirectToRoute("orders_edit",[
                    "id"=>$order->getId()
                ]);
            }

            return $this->render("orders/invoice/add.html.twig",[
                "page_title"=>"Create invoice",
                "form"=>$form->createView(),
                "order"=>$order
            ]);
        }catch(\Exception $e){
          
            $this->addFlash("error",$e->getMessage());
            return $this->redirectToRoute("orders_edit",[
                "id"=>$order->getId()
            ]);
        }

    }

     /**
     * @IsGranted("ROLE_ADMIN_ORDERS")
     * @Route("/orders/edit/{id}/invoice/delete/{invoice_id}", name="orders_invoice_delete", requirements={"id":"\d+","invoice_id":"\d+"})
     * @ParamConverter("invoice", class="App:Invoice", options={"id":"invoice_id"})
     */
    public function deleteInvoice(Request $request,
                                EntityManagerInterface $em,
                                Order $order,
                                Invoice $invoice,
                                $tr
                                ){

        try{
            
            if(in_array($order->getStatus(),["canceled"])){
                throw new \Exception($tr->trans("Cannot delete invoice on canceled orders"));
            }

            
            $em->remove($invoice);
            $order->setStatus("new");
            $em->flush();

            $this->addFlash("success",$tr->trans("Invoice deleted"));
            return $this->redirectToRoute("orders_edit",[
                    "id"=>$order->getId()
            ]);

           
        }catch(\Exception $e){
          
            $this->addFlash("error",$e->getMessage());
            return $this->redirectToRoute("orders_edit",[
                "id"=>$order->getId()
            ]);
        }

    }

     /**
     * @Route("/orders/edit/{id}/invoice/print/{invoice_id}", name="orders_invoice_print", requirements={"id":"\d+","invoice_id":"\d+"})
     * @ParamConverter("invoice", class="App:Invoice", options={"id":"invoice_id"})
     */
    public function printInvoice(Request $request,
                                EntityManagerInterface $em,
                                Order $order,
                                OrderRepository $orderRepo,
                                OrderItemRepository $orderItemRepo,
                                OrderHelper $orderHelper,
                                Invoice $invoice,
                                $tr
                                ){

        $this->denyAccessUnlessGranted('GENERATE_INVOICE',$order);

        try{
           $data = $orderHelper->generateInvoiceData($order,$invoice);
          
           $html = $this->renderView("orders/invoice/invoice.html.twig",[
               "data"=>$data
           ]);
            // return $this->render("orders/invoice/invoice.html.twig",[
            //     "data"=>$data
            // ]);
           $dompdf = new Dompdf();
           $dompdf->loadHtml($html);
           
           $dompdf->setPaper('A4', 'portrait');
           
           $dompdf->render();
           $name = "invoice_".$data['invoice']['number'];
           $dompdf->stream($name,["Attachment"=>false]);
           exit;
        }catch(\Exception $e){
           
            $this->addFlash("error",$e->getMessage());
            return $this->redirectToRoute("orders_edit",[
                "id"=>$order->getId()
            ]);
        }

    }

    /**
     * @Route("/orders/filtersAjax", name="orders_filters_ajax")
     */
    public function filtersAjax(Request $request,
                                ValidatorInterface $validator){
        
        try{
            
            $data['success'] = true;
            $data['url'] = "";
                            
            $filters = $this->getFiltersFromRequest($request,$validator);
                                       
            $filters = array_filter($filters,function($elem){
                            return !is_null($elem) && $elem !== "";
                    });
                            
            $data['url'] = http_build_query($filters);
                                        
            }catch(\Exception $e){
                $data['success'] = false;
                $data['url'] = "";
            }                            
                                    
                            
            return $this->json($data);

    }

    private function getFiltersFromRequest(Request $request,ValidatorInterface $validator){

        $filters = [
            "number"=>$request->query->get("number",null),
            "customer"=>$request->query->get("customer",null),
            "status"=>$request->query->get("status",null),
            "total"=>$request->query->get("total",null),
            "date"=>$request->query->get("date",null),
            "e_date"=>$request->query->get("e_date",null)
        ];
        $em = $this->getDoctrine()->getManager();
        $last = $em->getRepository(Order::class)->getLastOrderNumber();
        $firstNum = \App\Service\OrderHelper::STARTING_NUMBER;
        $lastNum = $firstNum;
        if(!$last){
            $firstNum = 0;
            $lastNum = 0;
        }else{
            $lastNum = (int) $last->getNumber();
        }
        $rules = [];
        $rules['number'] = [
            new Assert\Type([
                "type"=>"numeric"
            ]),
        //    new Assert\GreaterThanOrEqual([
        //        "value"=>$firstNum
        //    ]),
        //    new Assert\LessThanOrEqual([
        //        "value"=>$lastNum
        //    ])
        ];
        $rules['customer'] = [
            new Assert\Type([
                "type"=>"string"
            ]),
            new Assert\Length([
                "min"=>2,
                "max"=>200
            ])
        ];
        $rules['status'] = [
            new Assert\Type([
                "type"=>"alpha"
            ]),
            new Assert\Choice([
                "choices"=>["new","canceled","complete"]
            ])
        ];
        $rules['total'] = [
            new Assert\Type([
                "type"=>"numeric"
            ])
        ];
        $rules['date'] = [
            new Assert\DateTime([
                "format"=>"Y-m-d"
            ])
        ];
        $rules['e_date'] = [
            new Assert\DateTime([
                "format"=>"Y-m-d"
            ])
        ];
        $constraint = new Assert\Collection($rules);
        $violations = $validator->validate($filters,$constraint);
        if($violations->count() > 0){
            foreach($violations as $violation){
               $key = trim(str_replace(["[","]"],["",""],$violation->getPropertyPath()));
               if(isset($filters[$key])){
                   $filters[$key] = null;
               }
            }
        }

        if(empty($filters['date']) || empty($filters['e_date'])){
            $filters['date'] = null;
            $filters['e_date'] = null;
        }

        return $filters;
    }
}
