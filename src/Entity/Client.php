<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ClientRepository")
 * @ORM\Table(name="clients")
 */
class Client
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = 2,
     *      max = 200,
     *      minMessage = "Client name must be at least {{ limit }} characters long",
     *      maxMessage = "Client name cannot be longer than {{ limit }} characters"
     * )
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Industry", cascade={"detach"})
     * @Assert\NotBlank()
     */
    private $industry;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(
     *      min = 2,
     *      max = 200,
     *      minMessage = "Client identification must be at least {{ limit }} characters long",
     *      maxMessage = "Client identification cannot be longer than {{ limit }} characters"
     * )
     */
    private $identification;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(
     *      min = 4,
     *      max = 200,
     *      minMessage = "Client vat must be at least {{ limit }} characters long",
     *      maxMessage = "Client vat cannot be longer than {{ limit }} characters"
     * )
     */
    private $vatnumber;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = 4,
     *      max = 200,
     *      minMessage = "Client phone must be at least {{ limit }} characters long",
     *      maxMessage = "Client phone cannot be longer than {{ limit }} characters"
     * )
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(
     *      min = 6,
     *      max = 24,
     *      minMessage = "Client account must be at least {{ limit }} characters long",
     *      maxMessage = "Client account cannot be longer than {{ limit }} characters"
     * )
     */
    private $account;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = 4,
     *      max = 200,
     *      minMessage = "Client shipping address must be at least {{ limit }} characters long",
     *      maxMessage = "Client shipping address cannot be longer than {{ limit }} characters"
     * )
     */
    private $shipping_address;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Assert\Length(
     *      min = 4,
     *      max = 200,
     *      minMessage = "Client billing address must be at least {{ limit }} characters long",
     *      maxMessage = "Client billing address cannot be longer than {{ limit }} characters"
     * )
     */
    private $billing_address;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ContactPerson", mappedBy="client", cascade={"remove"})
     */
    private $contactPersons;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Assert\Length(
     *      min = 4,
     *      max = 200,
     *      minMessage = "Client shipping address must be at least {{ limit }} characters long",
     *      maxMessage = "Client shipping address cannot be longer than {{ limit }} characters"
     * )
     */
    private $details;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Tax")
     */
    private $tax;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Order", mappedBy="client")
     */
    private $orders;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deleted_at;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User", inversedBy="client", cascade={"persist", "remove"})
     */
    private $user;

    public function __construct()
    {
        $this->industry = new ArrayCollection();
        $this->contactPersons = new ArrayCollection();
        $this->orders = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Industry[]
     */
    public function getIndustry(): Collection
    {
        return $this->industry;
    }

    public function addIndustry(Industry $industry): self
    {
        if (!$this->industry->contains($industry)) {
            $this->industry[] = $industry;
        }

        return $this;
    }

    public function removeIndustry(Industry $industry): self
    {
        if ($this->industry->contains($industry)) {
            $this->industry->removeElement($industry);
        }

        return $this;
    }

    public function getIdentification(): ?string
    {
        return $this->identification;
    }

    public function setIdentification(?string $identification): self
    {
        $this->identification = $identification;

        return $this;
    }

    public function getVatnumber(): ?string
    {
        return $this->vatnumber;
    }

    public function setVatnumber(string $vatnumber): self
    {
        $this->vatnumber = $vatnumber;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getAccount(): ?string
    {
        return $this->account;
    }

    public function setAccount(?string $account): self
    {
        $this->account = $account;

        return $this;
    }

    public function getShippingAddress(): ?string
    {
        return $this->shipping_address;
    }

    public function setShippingAddress(string $shipping_address): self
    {
        $this->shipping_address = $shipping_address;

        return $this;
    }

    public function getBillingAddress(): ?string
    {
        return $this->billing_address;
    }

    public function setBillingAddress(?string $billing_address): self
    {
        $this->billing_address = $billing_address;

        return $this;
    }

    /**
     * @return Collection|ContactPerson[]
     */
    public function getContactPersons(): Collection
    {
        return $this->contactPersons;
    }

    public function addContactPerson(ContactPerson $contactPerson): self
    {
        if (!$this->contactPersons->contains($contactPerson)) {
            $this->contactPersons[] = $contactPerson;
            $contactPerson->setClient($this);
        }

        return $this;
    }

    public function removeContactPerson(ContactPerson $contactPerson): self
    {
        if ($this->contactPersons->contains($contactPerson)) {
            $this->contactPersons->removeElement($contactPerson);
            // set the owning side to null (unless already changed)
            if ($contactPerson->getClient() === $this) {
                $contactPerson->setClient(null);
            }
        }

        return $this;
    }

    public function getDetails(): ?string
    {
        return $this->details;
    }

    public function setDetails(?string $details): self
    {
        $this->details = $details;

        return $this;
    }

    public function __toString(){

        return static::class;
    }

    public function getTax(): ?Tax
    {
        return $this->tax;
    }

    public function setTax(?Tax $tax): self
    {
        $this->tax = $tax;

        return $this;
    }

    /**
     * @return Collection|Order[]
     */
    public function getOrders(): Collection
    {
        return $this->orders;
    }

    public function addOrder(Order $order): self
    {
        if (!$this->orders->contains($order)) {
            $this->orders[] = $order;
            $order->setClient($this);
        }

        return $this;
    }

    public function removeOrder(Order $order): self
    {
        if ($this->orders->contains($order)) {
            $this->orders->removeElement($order);
            // set the owning side to null (unless already changed)
            if ($order->getClient() === $this) {
                $order->setClient(null);
            }
        }

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deleted_at;
    }

    public function setDeletedAt(?\DateTimeInterface $deleted_at): self
    {
        $this->deleted_at = $deleted_at;

        return $this;
    }

    public function getInvoiceData():array{
        return [
            "name"=>$this->name,
            "email"=>$this->email,
            "phone"=>$this->phone,
            "vat"=>$this->vatnumber,
            "address"=>$this->billing_address?wordwrap($this->billing_address,80,"<br />\n"):wordwrap($this->shipping_address,80,"<br />\n")
        ];
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
