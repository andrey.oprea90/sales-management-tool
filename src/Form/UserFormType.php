<?php

namespace App\Form;

use App\Entity\User;
use App\Entity\AclRole;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type as Type;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class UserFormType extends AbstractType
{
    private $params;

    private $tokenStorage;

    private $tr;
    
    public function __construct(TokenStorageInterface $tokenStorage,
                                ParameterBagInterface $params,
                                $tr){
        $this->params = $params;
        $this->tokenStorage = $tokenStorage;
        $this->tr = $tr;
        
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $isEdit = !empty($options['data']) && $options['data'] instanceof User && $options['data']->getId();
        $passConstraints = [];
      
        $disabled = $isEdit && ($options['data']->getId() == $this->tokenStorage->getToken()->getUser()->getId());

        if(!$isEdit){
            $passConstraints = [
                new Assert\NotBlank()
            ];
        }

        $userTypes = [
            'Admin'=>'ROLE_ADMIN'
        ];
        if($this->params->has('custom_roles')){
            $roles = $this->params->get('custom_roles');
            $values = $roles;
            array_walk($roles,function(&$item,$id){
                $item = ucwords(strtolower(str_replace("_"," ",str_replace("ROLE_","",$item))));
                
            });
            array_unshift($roles,"Admin");
            array_unshift($values,"ROLE_ADMIN");

            $userTypes = array_combine($roles,$values);
            
        }

        $builder
            ->add("token",Type\HiddenType::class,[
                'data'=>md5(uniqid())
            ])
            ->add('username',Type\TextType::class,[
                'empty_data'=>'',
                'required'=>true,
                'disabled'=>$disabled,
                'label'=>$this->tr->trans('Username')
            ])
            ->add('roles',Type\ChoiceType::class,[
                'required'=>true,
                'choices'=>$userTypes,
                'multiple'=>true,
                'constraints'=>[
                    new Assert\NotBlank()
                ],
                'disabled'=>$disabled,
                'label'=>$this->tr->trans('Roles')
                
            ])
            ->add('password',Type\RepeatedType::class,[
                'required'=>true,
                'type'=>Type\PasswordType::class,
                'empty_data'=>'',
                'first_options'  => ['label' => $this->tr->trans('Password')],
                'second_options' => ['label' => $this->tr->trans('Repeat Password')],
                'constraints'=>$passConstraints
            ])
            ->add('email')
            ->add('fullname',Type\TextType::class,[
                'label'=>$this->tr->trans("Full name"),
                'empty_data'=>''
            ])
            ->add('aclRole',EntityType::class,[
                'placeholder'=>$this->tr->trans('Select api role'),
                'class'=>AclRole::class,
                'choice_label'=>'name',
                'label'=>$this->tr->trans('Acl api role'),
                'disabled'=>true
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
