<?php

namespace App\Form;

use App\Entity\Product;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type as Type;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\File\File;
use App\Entity\Category;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;
use App\Entity\Brand;
use App\Entity\Manufacturer;
use App\Entity\Tax;
use App\Entity\Color;
use App\Entity\AttributeSet;

class ProductFormType extends AbstractType
{
    private $params;

    private $tr;
    
    public function __construct(ParameterBagInterface $params,$tr){

        $this->params = $params;
        $this->tr = $tr;

    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $isEdit = !empty($options['data']) && $options['data']->getId()?$options['data']:null;

        $builder
            ->add('title',Type\TextType::class,[
                'label'=>$this->tr->trans('Title')
            ])
            ->add('description',Type\TextareaType::class,[
                'label'=>$this->tr->trans('Short description')
            ])
            ->add('qty',Type\TextType::class,[
                'label'=>$this->tr->trans('Quantity')
            ])
            ->add('sku',Type\TextType::class,[
                'label'=>$this->tr->trans('Sku')
            ])
            ->add('buy_price',Type\TextType::class,[
                'label'=>$this->tr->trans('Buy price')
            ])
            ->add('weight',Type\TextType::class,[
                'label'=>$this->tr->trans('Weight')
            ])
            ->add('price',Type\TextType::class,[
                'label'=>$this->tr->trans('Sell price')
            ])
            ->add('special_price',Type\TextType::class,[
                'label'=>$this->tr->trans('Sell discount price')
            ])
            ->add('main_image',Type\FileType::class,[
                'label'=>$this->tr->trans('Image'),
                'constraints'=>[
                    new Assert\File([
                        'maxSize'=>"5M",
                        'mimeTypes'=>[
                            "image/*"
                        ]
                    ])
                ]
            ])
            ->add('brand',EntityType::class,[
                'class'=>Brand::class,
                'choice_label'=>'name',
                'label'=>$this->tr->trans('Brand'),
                'placeholder'=>$this->tr->trans('Choose Brand')
            ])
            ->add('manufacturer',EntityType::class,[
                'class'=>Manufacturer::class,
                'choice_label'=>'name',
                'label'=>$this->tr->trans('Manufacturer'),
                'placeholder'=>$this->tr->trans('Choose Manufacturer')
            ])
            ->add('tax',EntityType::class,[
                'class'=>Tax::class,
                'choice_label'=>'name',
                'label'=>$this->tr->trans('Tax class'),
                'placeholder'=>$this->tr->trans('Choose tax class')
            ])
            ->add('attribute_set',EntityType::class,[
                'class'=>AttributeSet::class,
                'disabled'=>$isEdit && $isEdit->getAttributeSet(),
                'choice_label'=>'name',
                'label'=>$this->tr->trans('Attribute set'),
                'placeholder'=>$this->tr->trans('Choose attribute set')
            ])
            // ->add('categories',EntityType::class,[
            //     'class'=>Category::class,
            //     'choice_label'=>'name',
            //     'label'=>'Categories',
            //     'placeholder'=>'Choose categories',
            //     'multiple'=>true

            // ])
            ->add('categories',Select2EntityType::class,[
                'multiple' => true,
                'remote_route' => 'categories_ajax',
                'class'=>Category::class,
                'primary_key' => 'id',
                'text_property' => 'name',
                'minimum_input_length' => 2,
                'page_limit' => 10,
                'allow_clear' => true,
                'delay' => 250,
                'cache' => false,
                'cache_timeout' => 60000, // if 'cache' is true
                'language' => 'en',
                'placeholder' => $this->tr->trans('Select a category'),
                'scroll'=>true

            ])->add('color',EntityType::class,[
                'class'=>Color::class,
                'choice_label'=>'name',
                'label'=>$this->tr->trans('Color'),
                'placeholder'=>$this->tr->trans('Choose Color')
            ])->add('width',Type\TextType::class,[
                'label'=>$this->tr->trans('Width')
            ])->add('height',Type\TextType::class,[
                'label'=>$this->tr->trans('Height')
            ])->add('in_stock',Type\ChoiceType::class,[
                'label'=>$this->tr->trans('Stock status'),
                'choices'=>[
                    $this->tr->trans('In stock')=>true,
                    $this->tr->trans('Out of stock')=>false
                ],
                'empty_data'=>''
            ])
        ;

        $builder->get('main_image')->addModelTransformer(new CallBackTransformer(
            function($data){
                $uploadsDir = $this->params->get("default_product_upload_dir");
                if(!empty($data) && is_string($data)){
                    if(is_file($uploadsDir.DIRECTORY_SEPARATOR.$data)){
                        $file = new File($uploadsDir.DIRECTORY_SEPARATOR.$data);
                        
                        return $file;
                    }
                }
            },
            function($data){
                return $data;
            }
        ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
           // 'csrf_protection'=>false
        ]);
    }
}
