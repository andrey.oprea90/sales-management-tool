<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PermissionRepository")
 * @ORM\Table(name="permissions")
 * @ORM\HasLifecycleCallbacks()
 */
class Permission
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $action;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\AclRole", inversedBy="permissions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $aclRole;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAction(): ?string
    {
        return $this->action;
    }

    public function setAction(string $action): self
    {
        $this->action = $action;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(?\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }


     /**
     * @ORM\PrePersist
     */

    public function setCreatedAtValue(){

        $this->created_at = new \DateTime();
        
     }

    public function getAclRole(): ?AclRole
    {
        return $this->aclRole;
    }

    public function setAclRole(?AclRole $aclRole): self
    {
        $this->aclRole = $aclRole;

        return $this;
    }
}
