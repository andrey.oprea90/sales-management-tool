<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use App\Entity\Category;
use App\Service\ReportsHelper;
use Carbon\Carbon;
use App\Entity\Warehouse;
use App\Repository\WarehouseRepository;
use App\Service\ApiHelper;
use App\Repository\OrderRepository;
use App\Repository\ProductRepository;
use App\Repository\OrderItemRepository;
use AndreaSprega\Bundle\BreadcrumbBundle\Annotation\Breadcrumb;
use App\Repository\FilesRepository;


/**
 * @IsGranted("ROLE_ADMIN_DASHBOARD")
 * @Breadcrumb({"label" = "Dashboard", "route" = "dashboard" })
 */
class DashboardController extends BaseAdminController
{
    /**
     * @Route("/", name="dashboard")
     */
    public function index(TranslatorInterface $translator,
                         EntityManagerInterface $em,
                         ReportsHelper $reportsHelper,
                         WarehouseRepository $warehouseRepo,
                         ApiHelper $apiHelper,
                         OrderRepository $orderRepo,
                         ProductRepository $productRepo,
                         OrderItemRepository $orderItemRepo,
                         FilesRepository $fileRepo,
                         Request $request,
                         AuthorizationCheckerInterface $checker,
                         $tr
                         )
    {

        //$translator->trans('Hello');
       
        $startOfMonth = Carbon::now()->startOfMonth();
        $endOfMonth = Carbon::now()->endOfMOnth();

        $startOfYear = Carbon::now()->startOfYear();
        $endOfYear = Carbon::now()->endOfYear();
     
        $apiToken = $apiHelper->getApiToken();
       
        $latestOnDashboard = $this->getParameter("latest_on_dashboard");

        $lastOrders = $orderRepo->getLastOrders($latestOnDashboard,$this->getUser());
        $latestProducts = $productRepo->getLatestProducts($latestOnDashboard);

        $user = $this->getUser();

        if($user->isOnlyCustomer()){
            
            $validMonths = range(1,12);
            $validYears = range(2019,date("Y") + 10);

            return $this->render('dashboard/customer_index.html.twig', [
                'page_title'=>'Dashboard',
                'apiToken'=>$apiToken,
                'startOfMonth'=>$startOfMonth,
                'endOfMonth'=>$endOfMonth,
                'startOfYear'=>$startOfYear,
                'endOfYear'=>$endOfYear,
                'orders'=>$lastOrders,
                'products'=>$latestProducts,
                'validMonths'=>$validMonths,
                'validYears'=>$validYears
             ]);  
        }

        return $this->render('dashboard/index.html.twig', [
           'page_title'=>'Dashboard',
           'apiToken'=>$apiToken,
           'startOfMonth'=>$startOfMonth,
           'endOfMonth'=>$endOfMonth,
           'startOfYear'=>$startOfYear,
           'endOfYear'=>$endOfYear,
           'orders'=>$lastOrders,
           'products'=>$latestProducts
        ]);
    }
}
