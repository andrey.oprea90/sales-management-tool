<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use App\Entity\Brand;
use App\Repository\BrandRepository;
use App\Form\BrandFormType;
use AndreaSprega\Bundle\BreadcrumbBundle\Annotation\Breadcrumb;

/**
 * IsGranted("ROLE_ADMIN_BRANDS")
 * @Breadcrumb({"label" = "Brands", "route" = "brands" })
 */
class BrandController extends AbstractController
{
     /**
     * @Route("/brands/{page}", name="brands", defaults={"page":"1"}, requirements={"page":"\d+"})
     */
    public function index(Request $request, 
                         BrandRepository $brandRepo,
                        $page
                        )
    {
        $brands = $brandRepo->findAllPaginated($page,$this->getParameter('default_per_page'));

        return $this->render('brand/index.html.twig', [
            'page_title' => 'Brands list',
            'brands'=>$brands
        ]);
    }

    /**
     * @Route("/brand/add", name="brand_add")
     */
    public function add(Request $request,
                        EntityManagerInterface $em,
                        $tr){

        try{
            $brand = new Brand();
            $form = $this->createForm(BrandFormType::class,$brand);

            $form->handleRequest($request);

            if($form->isSubmitted() && $form->isValid()){

                $em->persist($brand);
                $em->flush();

                $this->addFlash("success",$tr->trans("Brand saved"));
                return $this->redirectToRoute("brands");
            }
        }catch(\Exception $e){
           
            $this->addFlash("error",$e->getMessage());
            return $this->redirectToRoute("brands");
        }
        

        return $this->render('brand/add.html.twig',[
            'page_title'=>'Add brand',
            'form'=>$form->createView()
        ]);
    }

  

    /**
     * @Route("/brand/delete/{id}", name="brand_delete", requirements={"id":"\d+"})
     */
    public function delete(Request $request,
                           EntityManagerInterface $em,
                           Brand $brand,
                           $tr
                           ){

        try{
            
            $em->remove($brand);
            $em->flush();

            $this->addFlash("success",$tr->trans("Brand deleted"));
            return $this->redirectToRoute("brands");

        }catch(\Exception $e){

            $this->addFlash("error",$e->getMessage());
            return $this->redirectToRoute("brands");
        }          

    }
}
