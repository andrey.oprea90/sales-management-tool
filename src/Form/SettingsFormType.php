<?php

namespace App\Form;

use App\Entity\Setting;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type as Type;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class SettingsFormType extends AbstractType
{
    private $params;

    private $tr;
    
    public function __construct(ParameterBagInterface $params,$tr){
        $this->params = $params;
        $this->tr = $tr;
        
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        
            ->add('company_name',Type\TextType::class,[
                'label'=>$this->tr->trans('Company name')
            ])
            ->add('vatnumber',Type\TextType::class,[
                'label'=>$this->tr->trans('VAT number')
            ])
            ->add('standard_tax_rate',Type\TextType::class,[
                'label'=>$this->tr->trans('Defalut tax percentage (%)')
            ])
            ->add('company_phone',Type\TextType::class,[
                'label'=>$this->tr->trans('Company phone')
            ])
            ->add('company_address',Type\TextareaType::class,[
                'label'=>$this->tr->trans('Company address')
            ])
            ->add('company_email',Type\TextType::class,[
                'label'=>$this->tr->trans('Company email')
            ])
            ->add('default_invoice_series',Type\TextType::class,[
                'label'=>$this->tr->trans('Default invoice series')
            ])
        ;

        if($this->params->has("currencies")){
            $currencies = $this->params->get("currencies");
            if(!empty($currencies['default'])){
                $builder->add('default_currency',Type\TextType::class,[
                    'label'=>$this->tr->trans('Currency'),
                    'mapped'=>false,
                    'disabled'=>true,
                    'data'=>$currencies['default']['name']." ( ".$currencies['default']['symbol']." )"
                ]);
            }
            
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Setting::class,
        ]);
    }
}
