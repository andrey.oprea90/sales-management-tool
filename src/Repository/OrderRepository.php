<?php

namespace App\Repository;

use App\Entity\Order;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Knp\Component\Pager\PaginatorInterface;
use Carbon\Carbon;
use App\Entity\Warehouse;
use App\Entity\User;
use App\Entity\Client;
use App\Entity\Customer;

/**
 * @method Order|null find($id, $lockMode = null, $lockVersion = null)
 * @method Order|null findOneBy(array $criteria, array $orderBy = null)
 * @method Order[]    findAll()
 * @method Order[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderRepository extends ServiceEntityRepository
{
    private $paginator;
    
    public function __construct(RegistryInterface $registry,PaginatorInterface $paginator)
    {
        $this->paginator = $paginator;
        parent::__construct($registry, Order::class);
    }

    // /**
    //  * @return Order[] Returns an array of Order objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Order
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function findAllPaginated(int $page = 1, int $perPage = 10,  array $filters = [],User $user = null){
        $filters = array_filter(array_merge([
            "number"=>null,
            "customer"=>null,
            "status"=>null,
            "total"=>null,
            "date"=>null,
            "e_date"=>null
        ],$filters),function($k){
            return in_array($k,["number","customer","status","total","date","e_date"]);
        },ARRAY_FILTER_USE_KEY);

        $query = $this->createQueryBuilder('o');
        $query->leftJoin('o.customer','cu');
        $query->leftJoin('o.client','cl');
        $query->addSelect('cu','cl');

        if($user && $user->isOnlyCustomer()){
            if($user->getClient()){
                $query->andWhere('o.client = :client');
                $query->setParameter('client',$user->getClient());
            }elseif($user->getCustomer()){
                $query->andWhere('o.customer = :customer');
                $query->setParameter('customer',$user->getCustomer());
            }
        };

        if(!is_null($filters['number'])){
            $query->andWhere('o.number LIKE :number');
            $query->setParameter('number','%'.$filters['number'].'%');
        }
        if(!is_null($filters['customer'])){
           $query->andWhere('( cu.first_name LIKE :customer OR cu.middle_name LIKE :customer OR cu.last_name LIKE :customer OR cu.email LIKE :customer ) OR (cl.name LIKE :customer OR cl.email LIKE :customer)');
           $query->setParameter('customer','%'.$filters['customer'].'%');
            
        }
        if(!is_null($filters['status'])){
            $query->andWhere('o.status = :status');
            $query->setParameter('status',$filters['status']);
        }
        if(!is_null($filters['total'])){
            if(is_numeric($filters['total'])){
                $number = (int) $filters['total'];
                $first = $number > 20?$number-10:$number;
                $last = $number + 10;

                $number = (string) $number;
                $length = strlen($number);
                if($length > 2){
                    $diff = $length - 2;
                   
                    $baseDigits = substr($number,0,$diff);
                    $first = (int) ($baseDigits.str_repeat("0",($length - $diff)));
                    $last = $first + 100;
                }
              
            }
            
            $query->andWhere('o.total_incl_tax BETWEEN  :f_total AND :l_total OR o.total_excl_tax BETWEEN :f_total AND :l_total');
            $query->setParameter('f_total',$first);
            $query->setParameter('l_total',$last);
        }
        if(!is_null($filters['date']) && !is_null($filters['e_date'])){
            $query->andWhere('o.date_created BETWEEN :s_date AND :e_date');
            $query->setParameter('s_date',Carbon::parse($filters['date'])->startOfDay()->format("Y-m-d H:i"));
            $query->setParameter('e_date',Carbon::parse($filters['e_date'])->endOfDay()->format("Y-m-d H:i"));
        } 

        $query->orderBy("o.date_created","DESC")
                    ->getQuery();

        return $this->paginator->paginate($query,$page,$perPage);

    }

    public function getLastOrderNumber(){
        
        return $this->createQueryBuilder('o')
                    ->orderBy('o.date_created','DESC')
                    ->setMaxResults(1)
                    ->getQuery()
                    ->getOneOrNullResult();
    }

    public function getOrderByNumber(string $number){

        return $this->createQueryBuilder('o')
                    ->addSelect('o.number')
                    ->andWhere('o.number = :number')
                    ->setParameter('number',$number)
                    ->setMaxResults(1)
                    ->getQuery()
                    ->getOneOrNullResult();
    }

    public function getCostsPerPeriod(Carbon $start,Carbon $end,?User $user = null){
        $params = [
            "start"=>$start->format("Y-m-d H:i"),
            "end"=>$end->format("Y-m-d H:i"),
            "status"=>"complete" 
        ];
       
        $qb = $this->createQueryBuilder('o');

        if($user && $user->isOnlyCustomer()){
            if($user->getClient()){
                $qb->andWhere('o.client = :client');
               $params['client'] = $user->getClient();
            }elseif($user->getCustomer()){
                $qb->andWhere('o.customer = :customer');
                $params['customer'] = $user->getCustomer();
            }
        };

        $qb->andWhere("o.date_created BETWEEN :start AND :end ");
        $qb->andWhere("o.status = :status");

        $qb->setParameters($params);

        return $qb->getQuery()->getResult();
    }

    
    public function getCompleteOrdersPerPeriod(Carbon $start,Carbon $end,?User $user = null){
        $params = [
            "start"=>$start->format("Y-m-d H:i"),
            "end"=>$end->format("Y-m-d H:i"),
            "status"=>"complete"
            
        ];
       
        $qb = $this->createQueryBuilder('o');

        if($user && $user->isOnlyCustomer()){
            if($user->getClient()){
                $qb->andWhere('o.client = :client');
                $params['client'] = $user->getClient();
            }elseif($user->getCustomer()){
                $qb->andWhere('o.customer = :customer');
                $params['customer'] = $user->getCustomer();
            }
        };

        $qb->andWhere("o.date_created BETWEEN :start AND :end ");
        $qb->andWhere("o.status = :status");
        $qb->setParameters($params);

        return $qb->getQuery()->getResult();
    }

    public function getLastOrders(int $num,?User $user = null){
        
        $qb = $this->createQueryBuilder("o");
                    if($user && $user->isOnlyCustomer()){
                        if($user->getClient()){
                            $qb->andWhere('o.client = :client');
                            $qb->setParameter('client',$user->getClient());
                        }elseif($user->getCustomer()){
                            $qb->andWhere('o.customer = :customer');
                            $qb->setParameter('customer',$user->getCustomer());
                        }
                    };
                 return $qb->andWhere("o.status <> :status")
                            ->setParameter('status','canceled')
                            ->orderBy('o.date_created','DESC')
                            ->setMaxResults($num)
                            ->getQuery()
                            ->getResult();

    }

    

}
