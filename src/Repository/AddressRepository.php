<?php

namespace App\Repository;

use App\Entity\Address;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use App\Entity\Employee;

/**
 * @method Address|null find($id, $lockMode = null, $lockVersion = null)
 * @method Address|null findOneBy(array $criteria, array $orderBy = null)
 * @method Address[]    findAll()
 * @method Address[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AddressRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Address::class);
    }

    // /**
    //  * @return Address[] Returns an array of Address objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Address
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function getAddressByEmployeeAndType(Employee $emp,int $type){

        $type = in_array($type,[Address::SHIPPING_TYPE,Address::BILLING_TYPE])?$type:Address::SHIPPING_TYPE;

        $qb = $this->createQueryBuilder('a')
                    ->andWhere("a.employee = :emp")
                    ->setParameter("emp",$emp)
                    ->andWhere("a.type = :type")
                    ->setParameter("type",$type);
        $addr = $qb->getQuery()->getOneOrNullResult();
        
        if(!$addr){

            $new_addr = new Address();
            $new_addr->setType($type);
            $new_addr->setEmployee($employee);
           
        }
        return $addr;
        
    }
}
