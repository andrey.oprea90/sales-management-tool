<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use App\Entity\Client;
use App\Entity\ContactPerson;
use App\Repository\ClientRepository;
use App\Repository\ContactPersonRepository;
use App\Form\ClientFormType;
use App\Form\ContactPersonFormType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use AndreaSprega\Bundle\BreadcrumbBundle\Annotation\Breadcrumb;

/**
 * @IsGranted("ROLE_ADMIN_DASHBOARD")
 * @Breadcrumb({"label" = "Clients", "route" = "clients" })
 */
class ClientsController extends AbstractController
{
    /**
     * @IsGranted("ROLE_ADMIN_CLIENTS")
     * @Route("/clients/{page}", name="clients", defaults={"page":"1"}, requirements={"page":"\d+"})
     */
    public function index(Request $request,
                        EntityManagerInterface $em,
                        ClientRepository $clientRepo,
                        $page
                        )
    {
        $clients = $clientRepo->findAllPaginated($page,$this->getParameter('default_per_page'));
        return $this->render('clients/index.html.twig', [
            'page_title' => 'Clients',
            'clients'=>$clients
        ]);
    }

     /**
     * @IsGranted("ROLE_ADMIN_CLIENTS")
     * @Route("/clients/restore/{page}", name="clients_restore", defaults={"page":"1"}, requirements={"page":"\d+"})
     * @Breadcrumb({"label" = "Restore"})
     */
    public function indexRestore(Request $request,
                        EntityManagerInterface $em,
                        ClientRepository $clientRepo,
                        $page
                        )
    {
        $clients = $clientRepo->findAllPaginated($page,$this->getParameter('default_per_page'),true);
        return $this->render('clients/restore.html.twig', [
            'page_title' => 'Restore clients',
            'clients'=>$clients
        ]);
    }

    /**
     * @IsGranted("ROLE_ADMIN_CLIENTS")
     * @Route("/clients/add", name="clients_add")
     */
    public function add(Request $request,
                        EntityManagerInterface $em,
                        $tr)
    {
        try{
            $client = new Client();

            $form = $this->createForm(ClientFormType::class,$client);
            $form->handleRequest($request);

            if($form->isSubmitted() && $form->isValid()){
                
                $em->persist($client);
                $em->flush();

                $this->addFlash("success",$tr->trans("Client saved"));
                return $this->redirectToRoute("clients");
            }

            return $this->render('clients/add.html.twig',[
                "page_title"=>"Add client",
                "form"=>$form->createView(),
                "client"=>$client
            ]);

        }catch(\Exception $e){
            
            $this->addFlash("error",$e->getMessage());

            return $this->redirectToRoute("clients");
        }
        
    }

    /**
     * @Route("/clients/edit/{id}", name="clients_edit", requirements={"id":"\d+"})
     */
    public function edit(Request $request,
                        EntityManagerInterface $em,
                        Client $client,
                        $tr
                        )
    {
        $this->denyAccessUnlessGranted('CLIENT_EDIT',$client);
        try{
            
            $form = $this->createForm(ClientFormType::class,$client);
            $form->handleRequest($request);

            if($form->isSubmitted() && $form->isValid()){
                
                $em->flush();

                $this->addFlash("success",$tr->trans("Client edited"));
                
            }

            return $this->render('clients/edit.html.twig',[
                "page_title"=>"Add client",
                "form"=>$form->createView(),
                "client"=>$client
            ]);

        }catch(\Exception $e){
            
            $this->addFlash("error",$e->getMessage());

            return $this->redirectToRoute("clients");
        }
    }

    /**
     * @IsGranted("ROLE_ADMIN_CLIENTS")
     * @Route("/clients/delete/{id}", name="clients_delete", requirements={"id":"\d+"})
     */
    public function delete(Request $request,
                        EntityManagerInterface $em,
                        Client $client,
                        $tr
                        )
    {
        try{
            //$em->remove($client);
            $client->setDeletedAt(new \DateTime());
            $em->flush();

            $this->addFlash("success",$tr->trans("Client deleted"));

            return $this->redirectToRoute("clients");

        }catch(\Exception $e){
            $this->addFlash("error",$e->getMessage());

            return $this->redirectToRoute("clients");
        }
        
    }

    /**
     * @IsGranted("ROLE_ADMIN_CLIENTS")
     * @Route("/clients/restore/item/{id}", name="clients_item_restore", requirements={"id":"\d+"})
     */
    public function restore(Request $request,
                        EntityManagerInterface $em,
                        Client $client,
                        $tr
                        )
    {
        try{
           
            $client->setDeletedAt(null);
            $em->flush();

            $this->addFlash("success",$tr->trans("Client restored"));

            return $this->redirectToRoute("clients");

        }catch(\Exception $e){
            $this->addFlash("error",$e->getMessage());

            return $this->redirectToRoute("clients");
        }
        
    }

    /**
     * @Route("/clients/edit/{id}/contactpersons/add", name="contactpersons_add", requirements={"id":"\d+"})
     * @Breadcrumb(
     * {"label" = "Edit", "route" = "clients_edit"}
     * )
     *
     */
    public function contactpersonsAdd(Request $request,
                        EntityManagerInterface $em,
                        Client $client,
                        $tr
                        )
    {
        $this->denyAccessUnlessGranted('CLIENT_EDIT',$client);
       try{
          $cp = new ContactPerson();
          $form = $this->createForm(ContactPersonFormType::class,$cp);

          $form->handleRequest($request);
          if($form->isSubmitted() && $form->isValid()){

              $cp->setClient($client);
              $em->persist($cp);
              $em->flush();

              $this->addFlash("success",$tr->trans("Contact person created"));
              return $this->redirectToRoute("clients_edit",[
                "id"=>$client->getId()
            ]);
          }
       }catch(\Exception $e){
            $this->addFlash("error",$e->getMessage());

            return $this->redirectToRoute("clients_edit",[
                "id"=>$client->getId()
            ]);
       }

       return $this->render("clients/contactpersons/add.html.twig",[
           "page_title"=>"Add contact person",
           "cp"=>$cp,
           "client"=>$client,
           "form"=>$form->createView()
       ]);
    }

    /**
     * @Route("/clients/edit/{id}/contactpersons/edit/{cp_id}", name="contactpersons_edit", requirements={"id":"\d+","cp_id":"\d+"})
     * @ParamConverter("cp", class="App:ContactPerson", options={"id"="cp_id"})
     * @Breadcrumb(
     * {"label" = "Edit", "route" = "clients_edit"}
     * )
     */
    public function contactpersonsEdit(Request $request,
                        EntityManagerInterface $em,
                        Client $client,
                        ContactPerson $cp,
                        $tr
                        )
    {
        $this->denyAccessUnlessGranted('CLIENT_EDIT',$client);
        try{
           
            $form = $this->createForm(ContactPersonFormType::class,$cp);
  
            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()){
  
                $em->flush();
  
                $this->addFlash("success",$tr->trans("Contact person created"));
                return $this->redirectToRoute("clients_edit",[
                  "id"=>$client->getId()
              ]);
            }
         }catch(\Exception $e){
              $this->addFlash("error",$e->getMessage());
  
              return $this->redirectToRoute("clients_edit",[
                  "id"=>$client->getId()
              ]);
         }
  
         return $this->render("clients/contactpersons/edit.html.twig",[
             "page_title"=>"Add contact person",
             "cp"=>$cp,
             "client"=>$client,
             "form"=>$form->createView()
         ]);
    }

    /**
     * @Route("/clients/edit/{id}/contactpersons/delete/{cp_id}", name="contactpersons_delete", requirements={"id":"\d+","cp_id":"\d+"})
     * @ParamConverter("cp", class="App:ContactPerson", options={"id"="cp_id"})
     */
    public function contactpersonsDelete(Request $request,
                        EntityManagerInterface $em,
                        Client $client,
                        ContactPerson $cp,
                        $tr
                        )
    {
       $this->denyAccessUnlessGranted('CLIENT_EDIT',$client); 
       try{
           $em->remove($cp);
           $em->flush();

           $this->addFlash("success",$tr->trans("Contact person removed"));
           return $this->redirectToRoute("clients_edit",[
                "id"=>$client->getId()
            ]);
       }catch(\Exception $e){
            $this->addFlash("error",$e->getMessage());
    
            return $this->redirectToRoute("clients_edit",[
                "id"=>$client->getId()
            ]);
       }
    }
}
