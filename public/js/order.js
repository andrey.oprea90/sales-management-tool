$(document).ready(function(){
    $('.order-filters .start_date').datepicker({
        dateFormat:"yy-mm-dd",
        
    });

    $('.order-filters .end_date').datepicker({
      dateFormat:"yy-mm-dd",
      
  });

    $("#order-filters-form .filter-item").each(function(){
        
        $(this).change(function(){
          $.ajax({
            url:'/orders/filtersAjax',
            method:'GET',
            data:$(this).closest('form').serialize()
          }).then(function(data){
              if(data.success){
                  if(data.url){
                    var url="?"+data['url'];
                    window.location.href = url;
                  }else{
                      window.location.href = "/orders";
                  }  
              }
          });  
        });
    });

    $("#order-filters-form .filter-dates").each(function(){
        
      $(this).change(function(){
        if($(this).hasClass('start_date')){
            if(!$(this).parent().parent().find(".end_date").val() ){
                return;
            }
        }else if($(this).hasClass('end_date')){
          if(!$(this).parent().parent().find(".start_date").val() ){
              return;
          }
        }else{
            return;
        }  
        $.ajax({
          url:'/orders/filtersAjax',
          method:'GET',
          data:$(this).closest('form').serialize()
        }).then(function(data){
            if(data.success){
                if(data.url){
                  var url="?"+data['url'];
                  window.location.href = url;
                }else{
                    window.location.href = "/orders";
                }  
            }
        });  
      });
  });
});