<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190618095539 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE brand (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE category (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, short_description LONGTEXT DEFAULT NULL, created_at DATETIME DEFAULT NULL, main_image VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE category_product (category_id INT NOT NULL, product_id INT NOT NULL, INDEX IDX_149244D312469DE2 (category_id), INDEX IDX_149244D34584665A (product_id), PRIMARY KEY(category_id, product_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE clients (id INT AUTO_INCREMENT NOT NULL, tax_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, identification VARCHAR(255) DEFAULT NULL, vatnumber VARCHAR(255) DEFAULT NULL, phone VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, account VARCHAR(255) DEFAULT NULL, shipping_address LONGTEXT NOT NULL, billing_address LONGTEXT DEFAULT NULL, details LONGTEXT DEFAULT NULL, deleted_at DATETIME DEFAULT NULL, INDEX IDX_C82E74B2A824D8 (tax_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE client_industry (client_id INT NOT NULL, industry_id INT NOT NULL, INDEX IDX_E31C5C6519EB6921 (client_id), INDEX IDX_E31C5C652B19A734 (industry_id), PRIMARY KEY(client_id, industry_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE contactpersons (id INT AUTO_INCREMENT NOT NULL, client_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, phone VARCHAR(255) NOT NULL, INDEX IDX_CBFD17D119EB6921 (client_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE customer (id INT AUTO_INCREMENT NOT NULL, tax_id INT DEFAULT NULL, first_name VARCHAR(255) NOT NULL, middle_name VARCHAR(255) DEFAULT NULL, last_name VARCHAR(255) NOT NULL, identification VARCHAR(255) NOT NULL, id_card VARCHAR(255) NOT NULL, shipping_address LONGTEXT NOT NULL, billing_address LONGTEXT DEFAULT NULL, email VARCHAR(255) NOT NULL, phone VARCHAR(255) NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, details LONGTEXT DEFAULT NULL, deleted_at DATETIME DEFAULT NULL, INDEX IDX_81398E09B2A824D8 (tax_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE employee (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, position_id INT NOT NULL, financial_id INT NOT NULL, first_name VARCHAR(255) NOT NULL, middle_name VARCHAR(255) DEFAULT NULL, last_name VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, phone_number VARCHAR(255) NOT NULL, identification VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, token VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_5D9F75A1A76ED395 (user_id), INDEX IDX_5D9F75A1DD842E46 (position_id), UNIQUE INDEX UNIQ_5D9F75A1F73B619 (financial_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE employee_department (employee_id INT NOT NULL, department_id INT NOT NULL, INDEX IDX_55CA515E8C03F15C (employee_id), INDEX IDX_55CA515EAE80F5DF (department_id), PRIMARY KEY(employee_id, department_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE files (id INT AUTO_INCREMENT NOT NULL, name LONGTEXT NOT NULL, extension VARCHAR(255) DEFAULT NULL, mime_type VARCHAR(255) NOT NULL, entity_id INT NOT NULL, entity_type VARCHAR(255) NOT NULL, path LONGTEXT NOT NULL, token VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE industry (id INT AUTO_INCREMENT NOT NULL, type VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE manufacturer (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE orders (id INT AUTO_INCREMENT NOT NULL, client_id INT DEFAULT NULL, customer_id INT DEFAULT NULL, number VARCHAR(255) NOT NULL, status VARCHAR(255) NOT NULL, date_created DATETIME NOT NULL, total_excl_tax NUMERIC(10, 2) DEFAULT \'0\' NOT NULL, tax NUMERIC(10, 2) DEFAULT \'0\' NOT NULL, total_incl_tax NUMERIC(10, 2) DEFAULT \'0\' NOT NULL, INDEX IDX_E52FFDEE19EB6921 (client_id), INDEX IDX_E52FFDEE9395C3F3 (customer_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE orderitems (id INT AUTO_INCREMENT NOT NULL, ord_id INT DEFAULT NULL, product_id INT DEFAULT NULL, warehouse_id INT DEFAULT NULL, product_name VARCHAR(255) NOT NULL, product_sku VARCHAR(255) NOT NULL, qty_ordered INT DEFAULT 0 NOT NULL, price NUMERIC(10, 2) DEFAULT \'0\' NOT NULL, tax_percent INT DEFAULT 0 NOT NULL, price_incl_tax NUMERIC(10, 2) DEFAULT \'0\' NOT NULL, total NUMERIC(10, 2) DEFAULT \'0\' NOT NULL, total_incl_tax NUMERIC(10, 2) DEFAULT \'0\' NOT NULL, canceled TINYINT(1) DEFAULT \'0\', INDEX IDX_F1CAA341E636D3F5 (ord_id), INDEX IDX_F1CAA3414584665A (product_id), INDEX IDX_F1CAA3415080ECDE (warehouse_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product (id INT AUTO_INCREMENT NOT NULL, brand_id INT DEFAULT NULL, manufacturer_id INT DEFAULT NULL, tax_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL, description LONGTEXT DEFAULT NULL, qty INT UNSIGNED DEFAULT 0, price NUMERIC(10, 2) UNSIGNED DEFAULT \'0\' NOT NULL, special_price NUMERIC(10, 2) UNSIGNED DEFAULT \'0\' NOT NULL, main_image VARCHAR(255) DEFAULT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, buy_price NUMERIC(10, 2) UNSIGNED DEFAULT \'0\' NOT NULL, sku VARCHAR(255) DEFAULT NULL, deleted_at DATETIME DEFAULT NULL, INDEX IDX_D34A04AD44F5D008 (brand_id), INDEX IDX_D34A04ADA23B42D (manufacturer_id), INDEX IDX_D34A04ADB2A824D8 (tax_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE warehouse_product (id INT AUTO_INCREMENT NOT NULL, warehouse_id INT NOT NULL, product_id INT NOT NULL, stock INT UNSIGNED DEFAULT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_F4AD11D85080ECDE (warehouse_id), INDEX IDX_F4AD11D84584665A (product_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tax (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, percent NUMERIC(10, 2) UNSIGNED DEFAULT \'0\' NOT NULL, details LONGTEXT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE users (id INT AUTO_INCREMENT NOT NULL, acl_role_id INT DEFAULT NULL, username VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, fullname VARCHAR(255) NOT NULL, deleted_at DATETIME DEFAULT NULL, token VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_1483A5E9F85E0677 (username), INDEX IDX_1483A5E9BD33296F (acl_role_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE warehouse (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, code VARCHAR(255) NOT NULL, address LONGTEXT DEFAULT NULL, created_at DATETIME DEFAULT NULL, details LONGTEXT DEFAULT NULL, deleted_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE acl_role (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, access INT NOT NULL, code VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_7065EB795E237E06 (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE address (id INT AUTO_INCREMENT NOT NULL, employee_id INT NOT NULL, street VARCHAR(255) DEFAULT NULL, number VARCHAR(255) DEFAULT NULL, postcode VARCHAR(255) DEFAULT NULL, city VARCHAR(255) DEFAULT NULL, country VARCHAR(255) DEFAULT NULL, type INT DEFAULT NULL, INDEX IDX_D4E6F818C03F15C (employee_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE currency (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, code VARCHAR(255) NOT NULL, symbol VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE department (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE financial (id INT AUTO_INCREMENT NOT NULL, account VARCHAR(255) DEFAULT NULL, hours_per_week NUMERIC(10, 2) DEFAULT \'0\', payment_per_hour NUMERIC(10, 2) DEFAULT \'0\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE permissions (id INT AUTO_INCREMENT NOT NULL, acl_role_id INT NOT NULL, action LONGTEXT NOT NULL, created_at DATETIME DEFAULT NULL, INDEX IDX_2DEDCC6FBD33296F (acl_role_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE position (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE settings (id INT AUTO_INCREMENT NOT NULL, currency_id INT DEFAULT NULL, company_name VARCHAR(255) NOT NULL, standard_tax_rate NUMERIC(10, 2) DEFAULT \'0\', company_phone VARCHAR(255) DEFAULT NULL, company_email VARCHAR(255) DEFAULT NULL, company_address LONGTEXT DEFAULT NULL, UNIQUE INDEX UNIQ_E545A0C538248176 (currency_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE category_product ADD CONSTRAINT FK_149244D312469DE2 FOREIGN KEY (category_id) REFERENCES category (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE category_product ADD CONSTRAINT FK_149244D34584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE clients ADD CONSTRAINT FK_C82E74B2A824D8 FOREIGN KEY (tax_id) REFERENCES tax (id)');
        $this->addSql('ALTER TABLE client_industry ADD CONSTRAINT FK_E31C5C6519EB6921 FOREIGN KEY (client_id) REFERENCES clients (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE client_industry ADD CONSTRAINT FK_E31C5C652B19A734 FOREIGN KEY (industry_id) REFERENCES industry (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE contactpersons ADD CONSTRAINT FK_CBFD17D119EB6921 FOREIGN KEY (client_id) REFERENCES clients (id)');
        $this->addSql('ALTER TABLE customer ADD CONSTRAINT FK_81398E09B2A824D8 FOREIGN KEY (tax_id) REFERENCES tax (id)');
        $this->addSql('ALTER TABLE employee ADD CONSTRAINT FK_5D9F75A1A76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE employee ADD CONSTRAINT FK_5D9F75A1DD842E46 FOREIGN KEY (position_id) REFERENCES position (id)');
        $this->addSql('ALTER TABLE employee ADD CONSTRAINT FK_5D9F75A1F73B619 FOREIGN KEY (financial_id) REFERENCES financial (id)');
        $this->addSql('ALTER TABLE employee_department ADD CONSTRAINT FK_55CA515E8C03F15C FOREIGN KEY (employee_id) REFERENCES employee (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE employee_department ADD CONSTRAINT FK_55CA515EAE80F5DF FOREIGN KEY (department_id) REFERENCES department (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE orders ADD CONSTRAINT FK_E52FFDEE19EB6921 FOREIGN KEY (client_id) REFERENCES clients (id)');
        $this->addSql('ALTER TABLE orders ADD CONSTRAINT FK_E52FFDEE9395C3F3 FOREIGN KEY (customer_id) REFERENCES customer (id)');
        $this->addSql('ALTER TABLE orderitems ADD CONSTRAINT FK_F1CAA341E636D3F5 FOREIGN KEY (ord_id) REFERENCES orders (id)');
        $this->addSql('ALTER TABLE orderitems ADD CONSTRAINT FK_F1CAA3414584665A FOREIGN KEY (product_id) REFERENCES product (id)');
        $this->addSql('ALTER TABLE orderitems ADD CONSTRAINT FK_F1CAA3415080ECDE FOREIGN KEY (warehouse_id) REFERENCES warehouse (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD44F5D008 FOREIGN KEY (brand_id) REFERENCES brand (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADA23B42D FOREIGN KEY (manufacturer_id) REFERENCES manufacturer (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADB2A824D8 FOREIGN KEY (tax_id) REFERENCES tax (id)');
        $this->addSql('ALTER TABLE warehouse_product ADD CONSTRAINT FK_F4AD11D85080ECDE FOREIGN KEY (warehouse_id) REFERENCES warehouse (id)');
        $this->addSql('ALTER TABLE warehouse_product ADD CONSTRAINT FK_F4AD11D84584665A FOREIGN KEY (product_id) REFERENCES product (id)');
        $this->addSql('ALTER TABLE users ADD CONSTRAINT FK_1483A5E9BD33296F FOREIGN KEY (acl_role_id) REFERENCES acl_role (id)');
        $this->addSql('ALTER TABLE address ADD CONSTRAINT FK_D4E6F818C03F15C FOREIGN KEY (employee_id) REFERENCES employee (id)');
        $this->addSql('ALTER TABLE permissions ADD CONSTRAINT FK_2DEDCC6FBD33296F FOREIGN KEY (acl_role_id) REFERENCES acl_role (id)');
        $this->addSql('ALTER TABLE settings ADD CONSTRAINT FK_E545A0C538248176 FOREIGN KEY (currency_id) REFERENCES currency (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD44F5D008');
        $this->addSql('ALTER TABLE category_product DROP FOREIGN KEY FK_149244D312469DE2');
        $this->addSql('ALTER TABLE client_industry DROP FOREIGN KEY FK_E31C5C6519EB6921');
        $this->addSql('ALTER TABLE contactpersons DROP FOREIGN KEY FK_CBFD17D119EB6921');
        $this->addSql('ALTER TABLE orders DROP FOREIGN KEY FK_E52FFDEE19EB6921');
        $this->addSql('ALTER TABLE orders DROP FOREIGN KEY FK_E52FFDEE9395C3F3');
        $this->addSql('ALTER TABLE employee_department DROP FOREIGN KEY FK_55CA515E8C03F15C');
        $this->addSql('ALTER TABLE address DROP FOREIGN KEY FK_D4E6F818C03F15C');
        $this->addSql('ALTER TABLE client_industry DROP FOREIGN KEY FK_E31C5C652B19A734');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04ADA23B42D');
        $this->addSql('ALTER TABLE orderitems DROP FOREIGN KEY FK_F1CAA341E636D3F5');
        $this->addSql('ALTER TABLE category_product DROP FOREIGN KEY FK_149244D34584665A');
        $this->addSql('ALTER TABLE orderitems DROP FOREIGN KEY FK_F1CAA3414584665A');
        $this->addSql('ALTER TABLE warehouse_product DROP FOREIGN KEY FK_F4AD11D84584665A');
        $this->addSql('ALTER TABLE clients DROP FOREIGN KEY FK_C82E74B2A824D8');
        $this->addSql('ALTER TABLE customer DROP FOREIGN KEY FK_81398E09B2A824D8');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04ADB2A824D8');
        $this->addSql('ALTER TABLE employee DROP FOREIGN KEY FK_5D9F75A1A76ED395');
        $this->addSql('ALTER TABLE orderitems DROP FOREIGN KEY FK_F1CAA3415080ECDE');
        $this->addSql('ALTER TABLE warehouse_product DROP FOREIGN KEY FK_F4AD11D85080ECDE');
        $this->addSql('ALTER TABLE users DROP FOREIGN KEY FK_1483A5E9BD33296F');
        $this->addSql('ALTER TABLE permissions DROP FOREIGN KEY FK_2DEDCC6FBD33296F');
        $this->addSql('ALTER TABLE settings DROP FOREIGN KEY FK_E545A0C538248176');
        $this->addSql('ALTER TABLE employee_department DROP FOREIGN KEY FK_55CA515EAE80F5DF');
        $this->addSql('ALTER TABLE employee DROP FOREIGN KEY FK_5D9F75A1F73B619');
        $this->addSql('ALTER TABLE employee DROP FOREIGN KEY FK_5D9F75A1DD842E46');
        $this->addSql('DROP TABLE brand');
        $this->addSql('DROP TABLE category');
        $this->addSql('DROP TABLE category_product');
        $this->addSql('DROP TABLE clients');
        $this->addSql('DROP TABLE client_industry');
        $this->addSql('DROP TABLE contactpersons');
        $this->addSql('DROP TABLE customer');
        $this->addSql('DROP TABLE employee');
        $this->addSql('DROP TABLE employee_department');
        $this->addSql('DROP TABLE files');
        $this->addSql('DROP TABLE industry');
        $this->addSql('DROP TABLE manufacturer');
        $this->addSql('DROP TABLE orders');
        $this->addSql('DROP TABLE orderitems');
        $this->addSql('DROP TABLE product');
        $this->addSql('DROP TABLE warehouse_product');
        $this->addSql('DROP TABLE tax');
        $this->addSql('DROP TABLE users');
        $this->addSql('DROP TABLE warehouse');
        $this->addSql('DROP TABLE acl_role');
        $this->addSql('DROP TABLE address');
        $this->addSql('DROP TABLE currency');
        $this->addSql('DROP TABLE department');
        $this->addSql('DROP TABLE financial');
        $this->addSql('DROP TABLE permissions');
        $this->addSql('DROP TABLE position');
        $this->addSql('DROP TABLE settings');
    }
}
