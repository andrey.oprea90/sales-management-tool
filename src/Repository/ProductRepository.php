<?php

namespace App\Repository;

use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Knp\Component\Pager\PaginatorInterface;
use App\Entity\WarehouseProduct;
use App\Entity\Warehouse;
use Carbon\Carbon;
use App\Entity\AttributeSet;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    private $paginator;
    
    public function __construct(RegistryInterface $registry,PaginatorInterface $paginator)
    {
        $this->paginator = $paginator;
        parent::__construct($registry, Product::class);
    }

    // /**
    //  * @return Product[] Returns an array of Product objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Product
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function findAllPaginated(int $page = 1, int $perPage = 10, $onlyTrashed = false, array $filters = []){
        $filters = array_filter(array_merge([
            "q"=>null,
            "stock"=>null,
            "date"=>null,
            "e_date"=>null
        ],$filters),function($k){
            return in_array($k,["q","stock","date","e_date"]);
        },ARRAY_FILTER_USE_KEY);
        
        $query = $this->createQueryBuilder('p');
        if($onlyTrashed){
            $query->andWhere('p.deleted_at IS NOT NULL');
        }else{
            $query->andWhere('p.deleted_at IS NULL');
        }

        if(!is_null($filters['q'])){
            $query->andWhere('p.title LIKE :q OR p.sku LIKE :q');
            $query->setParameter('q','%'.$filters['q'].'%');
        }
        if(!is_null($filters['stock']) || (isset($filters['stock']) && $filters['stock'] === "0") ){
  
            $query->andWhere('p.in_stock = :stock');
            $query->setParameter('stock',$filters['stock']);
        } 
        if(!is_null($filters['date']) && !is_null($filters['e_date'])){
            $query->andWhere('(p.created_at BETWEEN :s_date AND :e_date) OR (p.updated_at BETWEEN :s_date AND :e_date)');
            $query->setParameter('s_date',Carbon::parse($filters['date'])->startOfDay()->format("Y-m-d H:i"));
            $query->setParameter('e_date',Carbon::parse($filters['e_date'])->endOfDay()->format("Y-m-d H:i"));
        }
        
        $query->orderBy("p.created_at","DESC")
                    ->getQuery();

        return $this->paginator->paginate($query,$page,$perPage);

    }

    public function getLatestProducts(int $num){

        return $this->createQueryBuilder('p')
                      ->andWhere('p.deleted_at IS NULL')
                      ->orderBy("p.created_at","DESC")
                      ->setMaxResults($num)
                      ->getQuery()
                      ->getResult();
                   
    }

    public function getProductsByDateInterval(Carbon $start,Carbon $end){

        return $this->createQueryBuilder('p')
                      ->andWhere('p.deleted_at IS NULL')
                      ->orderBy("p.updated_at","DESC")
                      ->andWhere("p.updated_at BETWEEN :start AND :end ")
                      ->andWhere("p.qty > 0")
                      ->andWhere("p.buy_price > 0")
                      ->setParameters([
                          "start"=>$start->format("Y-m-d H:i"),
                          "end"=>$end->format("Y-m-d H:i")
                      ])
                      ->getQuery()
                      ->getResult();
    }


    public function getProductWithWarehousesData(Product $product,?Warehouse $warehouse = null):?Product{

        $qb =  $this->createQueryBuilder('p')
                    ->andWhere("p.id = :id")
                    ->setParameter("id",$product->getId())
                    ->leftJoin('p.wharehouseProduct','wp')
                    ->leftJoin('wp.warehouse','w');
        if($warehouse){
           
            $qb->andWhere("wp.warehouse = :wh");
            $qb->setParameter("wh",$warehouse);
        }
        $qb->addSelect("p","wp","w");

        return $qb->getQuery()->getOneOrNullResult();
    }

    public function getWarehouseProductOrNull(Product $product,Warehouse $warehouse):?WarehouseProduct{
        
        $prod = $this->getProductWithWarehousesData($product,$warehouse);
        if(!$prod){
            return null;
        }

        $coll = $prod->getWharehouseProduct();
        if($warehouse){
            $coll = $coll->filter(function($elem) use ($warehouse){
                return $elem->getWarehouse()->getId() == $warehouse->getId();
            });
        }
        return $coll->first() ?? null;

    }

    public function getWarehouseProductOrNew(Product $product,Warehouse $warehouse):?WarehouseProduct{
        
        $prod = $this->getProductWithWarehousesData($product,$warehouse);
        
        if(!$prod){
            return (new WarehouseProduct())
                        ->setStock(0)
                        ->setWarehouse($warehouse)
                        ->setProduct($product);
        }

        $coll = $prod->getWharehouseProduct();
        if($warehouse){
            $coll = $coll->filter(function($elem) use ($warehouse){
                return $elem->getWarehouse()->getId() == $warehouse->getId();
            });
        }
        return $coll->first() ?? (new WarehouseProduct())
                                    ->setStock(0)
                                    ->setWarehouse($warehouse)
                                    ->setProduct($product);

    }

    public function ajaxProducts(int $page = 1,int $perPage = 10,string $q = ''){
        $qb = $this->createQueryBuilder('p')
                    ->andWhere('p.deleted_at IS NULL')
                    ->orderBy('p.created_at','DESC');

        if(!empty($q)){
            $qb->andWhere('p.title LIKE :q');
            $qb->setParameter("q","%".$q."%");
        }
        
        $query = $qb->getQuery();

        return $this->paginator->paginate($query,$page,$perPage);
    }

    public function findProductsByAttributeSet(AttributeSet $attrSet){
        return $this->createQueryBuilder('p')
                    ->andWhere('p.attribute_set = :attrSet')
                    ->setParameter('attrSet',$attrSet)
                    ->getQuery()
                    ->getResult();
    }
}
