function salesPerWarehouse(elemClass){

    var append_elem = document.querySelector("#"+elemClass);

    if(!append_elem){
        return;
    }
    
    
    $(append_elem).html("");
    var sp = spinner($(append_elem).attr("id"));

    var url = append_elem.getAttribute("data-url");
    if(!url){
        return;
    }
    var token = append_elem.getAttribute("data-tok");
    if(!token){
        return;
    }

    var start =  append_elem.getAttribute("data-start");
    var end =  append_elem.getAttribute("data-end");

    if(!start || !end){
        return;
    }

    var headers = {
        'Content-Type': 'application/json',
       //  'Content-Type': 'application/x-www-form-urlencoded',
       'Authorization': 'Bearer '+token
         
    };
    data={
        start:start,
        end:end
    };
    return fetch(url,{
        method:"POST",
        headers: headers,
        body:JSON.stringify(data)
       
       
    }).then(function(data){
        
        return data.json();
    }).then(function(response){
          
      if(response.length){
        Morris.Bar({
            element: 'hero-bar',
            data: response,
            xkey: 'warehouse',
            ykeys: ['totals'],
            labels: ['Totals'],
            barRatio: 0.4,
            xLabelMargin: 10,
            hideHover: 'auto',
            barColors: ["#3d88ba"]
        });
      }
    }).finally(function(){
        try{
            sp.stop();
        }catch(e){

        }
        
    });
  }
  function profitPerPeriod(elemClass){

    var append_elem = document.querySelector("#"+elemClass);

    if(!append_elem){
        return;
    }

    $(append_elem).html("");
    var sp = spinner($(append_elem).attr("id"));

    var url = append_elem.getAttribute("data-url");
    if(!url){
        return;
    }
    var token = append_elem.getAttribute("data-tok");
    if(!token){
        return;
    }

    var start =  append_elem.getAttribute("data-start");
    var end =  append_elem.getAttribute("data-end");
    var mode =  append_elem.getAttribute("data-mode");

    if(!start || !end){
        return;
    }

    var headers = {
        'Content-Type': 'application/json',
       //  'Content-Type': 'application/x-www-form-urlencoded',
       'Authorization': 'Bearer '+token
         
    };
    data={
        start:start,
        end:end,
        mode:mode
    };
    return fetch(url,{
        method:"POST",
        headers: headers,
        body:JSON.stringify(data)
       
       
    }).then(function(data){
        
        return data.json();
    }).then(function(response){
          
      if(response.length){
        Morris.Line({
            element: elemClass,
            data: response,
            xkey: 'period',
            xLabels: 'day',
            ykeys: ['total_sell', 'total_buy'],
            labels: ['Sell price', 'Buy price']
        });
      }
    }).finally(function(){
        try{
            sp.stop();
        }catch(e){

        }
        
    });
  }

  function salesPerPeriod(elemClass){

    var append_elem = document.querySelector("#"+elemClass);
    
    if(!append_elem){
        return;
    }

    $(append_elem).html("");
    var sp = spinner($(append_elem).attr("id"));

    var url = append_elem.getAttribute("data-url");
    if(!url){
        return;
    }
    var token = append_elem.getAttribute("data-tok");
    if(!token){
        return;
    }
  
    var start =  append_elem.getAttribute("data-start");
    var end =  append_elem.getAttribute("data-end");
   

    if(!start || !end){
        return;
    }

    var headers = {
        'Content-Type': 'application/json',
       //  'Content-Type': 'application/x-www-form-urlencoded',
       'Authorization': 'Bearer '+token
         
    };
    data={
        start:start,
        end:end
    };
    return fetch(url,{
        method:"POST",
        headers: headers,
        body:JSON.stringify(data)
       
       
    }).then(function(data){
        
        return data.json();
    }).then(function(response){
          
      if(response.length){
        $.plot("#"+elemClass, [ response ], {
            series: {
                bars: {
                    show: true,
                    barWidth: 0.6,
                    align: "center"
                }
            },
            xaxis: {
                mode: "categories",
                tickLength: 0
            }
        });
      }
    }).finally(function(){
        try{
            sp.stop();
        }catch(e){

        }
        
    });
  }

  function incomeVsExpenseDashboard(elemClass){

    
    var append_elem = document.querySelector("#"+elemClass);
    
    if(!append_elem){
        return;
    }
    
    var total_incomes = $(append_elem).find("#dashboard-income");
    var total_expenses = $(append_elem).find("#dashboard-expense");
    var total_profit = $(append_elem).find("#dashboard-profit");

    if(!total_incomes || !total_expenses || !total_profit){
        return;
    }

    total_incomes.find(".info-box-number").html("");
    total_expenses.find(".info-box-number").html("");
    total_profit.find(".info-box-number").html("");
    
    var sp = spinner($(total_incomes).attr("id"));
    var sp2 = spinner($(total_expenses).attr("id"));
    var sp3 = spinner($(total_profit).attr("id"));

    
    

    var url = append_elem.getAttribute("data-url");
    if(!url){
        return;
    }
    var token = append_elem.getAttribute("data-tok");
    if(!token){
        return;
    }
  
    var start =  append_elem.getAttribute("data-start");
    var end =  append_elem.getAttribute("data-end");
   

    if(!start || !end){
        return;
    }

    var include_stock = append_elem.getAttribute("data-include-stock")
   
    var headers = {
        'Content-Type': 'application/json',
       //  'Content-Type': 'application/x-www-form-urlencoded',
       'Authorization': 'Bearer '+token
         
    };
    data={
        start:start,
        end:end,
        include_stock:include_stock
    };
    return fetch(url,{
        method:"POST",
        headers: headers,
        body:JSON.stringify(data)
       
       
    }).then(function(data){
        
        return data.json();
    }).then(function(response){
        total_incomes.find(".info-box-number").html(response.total_income.data_formatted);
        total_expenses.find(".info-box-number").html(response.total_expense.data_formatted);
        total_profit.find(".info-box-number").html(response.profit.data_formatted);
    }).finally(function(){

        try{
            sp.stop();
            sp2.stop();
            sp3.stop();

        }catch(e){

        }
        
    });
  }


incomeVsExpenseDashboard("totals-row");

salesPerWarehouse("hero-bar");

profitPerPeriod("hero-graph");

salesPerPeriod("catchart");








function labelFormatter(label, series) {
    return "<div style='font-size:8pt; text-align:center; padding:2px; color:white;'>" + label + "<br/>" + Math.round(series.percent) + "%</div>";
}