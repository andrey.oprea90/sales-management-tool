<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use App\Entity\Employee;
use App\Entity\Address;
use App\Repository\EmployeeRepository;
use App\Repository\AddressRepository;
use App\Form\AddressFormType;
use App\Form\EmployeeFormType;
use Symfony\Component\Form\FormFactoryInterface;
use App\Entity\Financial;
use App\Form\FinancialFormType;
use App\Entity\Salary;
use App\Repository\SalaryRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use App\Form\SalaryFormType;
use AndreaSprega\Bundle\BreadcrumbBundle\Annotation\Breadcrumb;


/**
 * @IsGranted("ROLE_ADMIN_EMPLOYEES")
 * @Breadcrumb({"label" = "Employees", "route" = "employees" })
 */
class EmployeesController extends AbstractController
{
    /**
     * @Route("/employees/{page}", name="employees", defaults={"page":"1"}, requirements={"page":"\d+"})
     */
    public function index(Request $request,
                          EntityManagerInterface $em,
                          EmployeeRepository $repo,
                          $page = 1
                          )
    {
        $employees = $repo->findAllPaginated($page,$this->getParameter('default_per_page'));
        
        return $this->render('employees/index.html.twig', [
            'page' => 'Employees',
            'employees'=>$employees
        ]);
    }

    /**
     * @Route("/employees/add", name="employees_add")
     */
    public function add(Request $request,
                        EntityManagerInterface $em,
                        EmployeeRepository $repo,
                        FormFactoryInterface $formFactory,
                        $tr
                        ){

        try{

            $employee = new Employee();
            $financial = new Financial();

            $shipping_address = new Address();
            $shipping_address->setType(Address::SHIPPING_TYPE);
    
            $billing_address = new Address();
            $billing_address->setType(Address::BILLING_TYPE);
    
            $emp_form = $this->createForm(EmployeeFormType::class,$employee);
            $financial_form = $this->createForm(FinancialFormType::class,$financial);
            $shipping_addr_form = $this->createForm(AddressFormType::class,$shipping_address);
            //$billing_addr_form = $this->createForm(AddressFormType::class,$billing_address);
            $billing_addr_form = $formFactory->createNamed('billing_address_form',AddressFormType::class,$billing_address);
    
            $emp_form->handleRequest($request);
            $shipping_addr_form->handleRequest($request);
            $billing_addr_form->handleRequest($request);
            $financial_form->handleRequest($request);
    
            
            $full_valid = ($emp_form->isSubmitted() && $emp_form->isValid()) && 
                            ($shipping_addr_form->isSubmitted() && $shipping_addr_form->isValid()) && 
                            ($billing_addr_form->isSubmitted() && $billing_addr_form->isValid()) &&
                            ($financial_form->isSubmitted() && $financial_form->isValid());
    
            
            if($full_valid){
                
                $em->transactional(function($em) use ($shipping_address,$billing_address,$employee,$financial){
                    $employee->addAddress($shipping_address);
                    $employee->addAddress($billing_address);
                    $employee->setFinancial($financial);
                    // $em->persist($shipping_address);
                    // $em->persist($billing_address);
                
                    $em->persist($employee);
    
                    $em->flush();
                });
    
                $this->addFlash("success",$tr->trans("Employee saved"));
    
                return $this->redirectToRoute("employees");
         }
    
       
        
         return $this->render('employees/add.html.twig',[
             "page_title"=>"Add employee",
             "emp_form"=>$emp_form->createView(),
             "shipping_addr_form"=>$shipping_addr_form->createView(),
             "billing_addr_form"=>$billing_addr_form->createView(),
             "financial_form"=>$financial_form->createView(),
             "employee"=>$employee
         ]);    

        }catch(\Exception $e){
            //dd($e->getMessage()." ".$e->getLine()." ".$e->getFile());
            $this->addFlash("error",$e->getMessage());

            return $this->redirectToRoute("employees");
        }

                    

    }

     /**
     * @Route("/employees/edit/{id}", name="employees_edit")
     */
    public function edit(Request $request,
                        EntityManagerInterface $em,
                        EmployeeRepository $repo,
                        FormFactoryInterface $formFactory,
                        AddressRepository $addressRepo,
                        Employee $employee,
                        $tr
                        ){

          try{
            $shipping_address = $addressRepo->getAddressByEmployeeAndType($employee,Address::SHIPPING_TYPE);
                        
            $billing_address = $addressRepo->getAddressByEmployeeAndType($employee,Address::BILLING_TYPE);
       
            $financial = $employee->getFinancial();
            
            $emp_form = $this->createForm(EmployeeFormType::class,$employee);
            $shipping_addr_form = $this->createForm(AddressFormType::class,$shipping_address);
            $financial_form = $this->createForm(FinancialFormType::class,$financial);
            //$billing_addr_form = $this->createForm(AddressFormType::class,$billing_address);
            $billing_addr_form = $formFactory->createNamed('billing_address_form',AddressFormType::class,$billing_address);
       
            $emp_form->handleRequest($request);
            $shipping_addr_form->handleRequest($request);
            $billing_addr_form->handleRequest($request);
            $financial_form->handleRequest($request);
       
            
            $full_valid = ($emp_form->isSubmitted() && $emp_form->isValid()) && 
                           ($shipping_addr_form->isSubmitted() && $shipping_addr_form->isValid()) && 
                           ($billing_addr_form->isSubmitted() && $billing_addr_form->isValid()) &&
                           ($financial_form->isSubmitted() && $financial_form->isValid());
       
             
            if($full_valid){
               
               $em->transactional(function($em) use ($shipping_address,$billing_address,$employee,$financial){
     
                   $em->flush();
               });
       
               $this->addFlash("success",$tr->trans("Employee updated"));
       
               return $this->redirectToRoute("employees_edit",[
                   "id"=>$employee->getId()
               ]);
            }    

            return $this->render('employees/edit.html.twig',[
                "page_title"=>"Edit employee",
                "emp_form"=>$emp_form->createView(),
                "shipping_addr_form"=>$shipping_addr_form->createView(),
                "billing_addr_form"=>$billing_addr_form->createView(),
                "financial_form"=>$financial_form->createView(),
                "employee"=>$employee
            ]); 
          }catch(\Exception $e){
             
            $this->addFlash("error",$e->getMessage());

            return $this->redirectToRoute("employees");
          }                  
                            

    }

    /**
     * @Route("/employees/delete/{id}",name="employees_delete", methods="GET", requirements={"id":"\d+"})
     */
    public function delete(Request $request,
                            Employee $employee,
                            EntityManagerInterface $em,
                            $tr
                            ){

       try{
         $em->remove($employee);
         $em->flush();

         $this->addFlash("success",$tr->trans("Employee deleted"));
         return $this->redirectToRoute("employees");
       }catch(\Exception $e){

           $this->addFlash("error",$e->getMessage());
           return $this->redirectToRoute("employees");
       }

    }

    /**
     * @Route("/employee/edit/{id}/salaries/{page}", name="employees_salaries", defaults={"page":"1"}, requirements={"page":"\d+"})
     */
    public function salaries(Request $request, 
                         SalaryRepository $salaryRepo,
                         Employee $employee,
                        $page
                        )
    {
        $salaries = $salaryRepo->findAllPaginated($page,$this->getParameter('default_per_page'));

        return $this->render('employees/salaries/index.html.twig', [
            'page_title' => 'Paid salaries',
            'salaries'=>$salaries,
            'employee'=>$employee
        ]);
    }

    /**
     * @Route("/employees/edit/{id}/salary/add", name="employees_salaries_add")
     * @Breadcrumb({"label" = "Salaries", "route" = "employees_salaries" })
     */
    public function addSalary(Request $request,
                        EntityManagerInterface $em,
                        Employee $employee,
                        SalaryRepository $salaryRepo,
                        $tr
                        ){

        try{
            $salary = new Salary();
            $form = $this->createForm(SalaryFormType::class,$salary);

            $form->handleRequest($request);
           
            if($form->isSubmitted() && $form->isValid()){
               
                $hoursWorked = $salary->getHoursWorked();
                $ratePerHour = $employee->getFinancial()?($employee->getFinancial()->getPaymentPerHour() ?? 0):0;
                $paidOn = $request->request->get('salary_form')['date_created'] ?? null;
                
                if(!$paidOn){
                    throw new \Exception($tr->trans("Paid on date not set"));
                }
                $paidOnDate = new \DateTime($paidOn);
                $checkFormat = $paidOnDate->format("m-Y");
                $exists = $salaryRepo->findOneBy(['month_year_check'=>$checkFormat]);
                if($exists){
                    throw new \Exception($tr->trans("Salary already paid this month"));
                }
                $salary->setDateCreated($paidOnDate);
                $salary->setMonthYearCheck($checkFormat);
                $salary->setEmployee($employee);
                $salary->setTotalPaid($hoursWorked * $ratePerHour);
               
                $em->persist($salary);
                $em->flush();

                $this->addFlash("success",$tr->trans("Salary paid"));
                return $this->redirectToRoute("employees_salaries",[
                    "id"=>$employee->getId()
                ]);
            }
        }catch(\Exception $e){
         
            $this->addFlash("error",$e->getMessage());
            return $this->redirectToRoute("employees_salaries_add",[
                "id"=>$employee->getId()
            ]);
        }
        

        return $this->render('employees/salaries/add.html.twig',[
            'page_title'=>'Pay salary',
            'form'=>$form->createView(),
            'employee'=>$employee
        ]);
    }

  

    /**
     * @Route("/employees/edit/{id}/salary/delete/{salary_id}", name="employees_salaries_delete", requirements={"id":"\d+","salary_id":"\d+"})
     * @ParamConverter("salary", class="App:Salary", options={"id"="salary_id"})
     */
    public function deleteSalary(Request $request,
                           EntityManagerInterface $em,
                           Salary $salary,
                           Employee $employee,
                           $tr
                           ){

        try{
            
            $em->remove($salary);
            $em->flush();

            $this->addFlash("success",$tr->trans("Salary removed"));
            return $this->redirectToRoute("employees_salaries",[
                "id"=>$employee->getId()
            ]);

        }catch(\Exception $e){

            $this->addFlash("error",$e->getMessage());
            return $this->redirectToRoute("employees_salaries",[
                "id"=>$employee->getId()
            ]);
        }          

    }
}
