<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\User;
use App\Entity\Files;
use App\Form\UserFormType;
use App\Repository\UserRepository;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Service\FileHelper;
use Symfony\Contracts\Translation\TranslatorInterface;
use AndreaSprega\Bundle\BreadcrumbBundle\Annotation\Breadcrumb;

/**
 * @IsGranted("ROLE_ADMIN_DASHBOARD")
 * @Breadcrumb({"label" = "Users", "route" = "users" })
 */
class UsersController extends AbstractController
{
    /**
     * @IsGranted("ROLE_ADMIN_USERS")
     * @Route("/users/{page}", name="users", defaults={"page":"1"}, requirements={"page":"\d+"})
     */
    public function index(
        Request $request,
        ParameterBagInterface $params, 
        UserRepository $user_repo,
        $page
        )
    {
       $users = $user_repo->getPaginatedUsers($page,$this->getParameter('users_per_page'),$this->getUser());
     
        return $this->render('users/index.html.twig', [
            'page_title'=>'Users',
            'users'=>$users
        ]);
    }

    /**
     * @IsGranted("ROLE_ADMIN_USERS")
     * @Route("/users/add", name="users_add")
     */
    public function add(
        Request $request,
        ParameterBagInterface $params, 
        UserRepository $user_repo,
        EntityManagerInterface $em,
        UserPasswordEncoderInterface $passwordEncoder,
        $tr
        )
    {

       $usr = new User();

       $form = $this->createForm(UserFormType::class,$usr);
       $form->handleRequest($request);

       if($form->isSubmitted() && $form->isValid()){
          $usr->setPassword($passwordEncoder->encodePassword($usr,$usr->getPassword()));
           $em->persist($usr);
           $em->flush();

           $this->addFlash('success',$tr->trans('User saved'));

           return $this->redirectToRoute('users');

       }

        return $this->render('users/add.html.twig', [
            'page_title'=>'Add User',
            'form'=>$form->createView(),
            'user'=>$usr
        ]);
    }

    /**
     * @IsGranted("ROLE_ADMIN_USERS")
     * @Route("/users/edit/{id}", name="users_edit")
     */
    public function edit(
        Request $request,
        ParameterBagInterface $params, 
        UserRepository $user_repo,
        EntityManagerInterface $em,
        User $user,
        UserPasswordEncoderInterface $passwordEncoder,
        $tr
        )
    {

       
       $form = $this->createForm(UserFormType::class,$user);
       $old_pass = $user->getPassword();
       $form->handleRequest($request);

       if($form->isSubmitted() && $form->isValid()){
          
           if(empty($user->getPassword())){
               $user->setPassword($old_pass);
           }else{
               $user->setPassword($passwordEncoder->encodePassword($user,$user->getPassword()));
           }
           $em->flush();

           $this->addFlash('success',$tr->trans('User edited'));

           return $this->redirectToRoute('users_edit',[
               'id'=>$user->getId()
           ]);

       }

        return $this->render('users/edit.html.twig', [
            'page_title'=>'Edit User',
            'form'=>$form->createView(),
            'user'=>$user
        ]);
    }

    /**
     * @IsGranted("ROLE_ADMIN_USERS")
     * @Route("/users/delete/{id}", name="users_delete", methods="GET", requirements={"id":"\d+"})
     */
    public function delete(Request $request,
                            User $user,
                            EntityManagerInterface $em,
                            $tr){

        try{

            if($this->getUser()->getId() == $user->getId()){
                throw new \Exception($tr->trans("Cannot delete the current logged in user"));
            }

            $files = $em->getRepository(User::class)->getUserFiles($user);
            $entity_type_dir = basename(str_replace("\\",DIRECTORY_SEPARATOR,User::class));
            $fileHelper = new FileHelper($this->getParameter("default_upload_dir"),FileHelper::slugify($entity_type_dir));
            
            $fileObjArr = [];
            $em->transactional(function($em) use ($user,$files,$fileHelper,&$fileObjArr){
                
                foreach($files as $file){
                  
                    $diskFile = $fileHelper->getFileFromEntity($file);
                   
                    if($diskFile){
                        $fileObjArr[] = $diskFile;
                    }

                    $em->remove($file);
                   
                }

                $em->remove($user);
                $em->flush();
               
            });

            foreach($fileObjArr as $f){
                $fileHelper->deleteFile($f);
            }

           $this->addFlash("success",$tr->trans("User deleted"));
           return $this->redirectToRoute("users");

        }catch(\Exception $e){

            $this->addFlash("error",$e->getMessage());
           return $this->redirectToRoute("users");
        }
    }


      /** 
     * @Route("/users/profile", name="users_profile")
     */
    public function profile(
        Request $request,
        ParameterBagInterface $params, 
        UserRepository $user_repo,
        EntityManagerInterface $em,
        UserPasswordEncoderInterface $passwordEncoder,
        $tr
        )
    {

       $user = $this->getUser();
       $form = $this->createForm(UserFormType::class,$user);
       $old_pass = $user->getPassword();
       $form->handleRequest($request);

       if($form->isSubmitted() && $form->isValid()){
          
           if(empty($user->getPassword())){
               $user->setPassword($old_pass);
           }else{
               $user->setPassword($passwordEncoder->encodePassword($user,$user->getPassword()));
           }
           $em->flush();

           $this->addFlash('success',$tr->trans('Profile edited'));

           return $this->redirectToRoute('users_profile');

       }

        return $this->render('users/edit.html.twig', [
            'page_title'=>'Profile',
            'form'=>$form->createView(),
            'user'=>$user
        ]);
    }
}
