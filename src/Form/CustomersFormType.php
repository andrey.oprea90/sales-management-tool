<?php

namespace App\Form;

use App\Entity\Customer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type as Type;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints as Assert;
use App\Entity\Tax;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\EntityManagerInterface;

class CustomersFormType extends AbstractType
{
    private $tr;

    private $em;

    public function __construct($tr,EntityManagerInterface $em){
        $this->tr = $tr;
        $this->em = $em;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $isEdit = !empty($options['data']) && $options['data']->getId();
        $hasUser = $isEdit && $options['data']->getUser();

        $builder
            ->add('first_name',Type\TextType::class,[
                'label'=>$this->tr->trans('First name')
            ])
            ->add('middle_name',Type\TextType::class,[
                'label'=>$this->tr->trans('Middle name')
            ])
            ->add('last_name',Type\TextType::class,[
                'label'=>$this->tr->trans('Last name')
            ])
            ->add('identification',Type\TextType::class,[
                'label'=>$this->tr->trans('Identification')
            ])
            ->add('id_card',Type\TextType::class,[
                'label'=>$this->tr->trans('ID card data')
            ])
            ->add('details',Type\TextareaType::class,[
                'label'=>$this->tr->trans('Details')
            ])
            ->add('shipping_address',Type\TextareaType::class,[
                'label'=>$this->tr->trans('Shipping address')
            ])
            ->add('billing_address',Type\TextareaType::class,[
                'label'=>$this->tr->trans('Billing address')
            ])
            ->add('email',Type\TextType::class,[
                'label'=>$this->tr->trans('Email')
            ])
            ->add('phone',Type\TextType::class,[
                'label'=>$this->tr->trans('Phone')
            ])
            ->add('tax',EntityType::class,[
                'class'=>Tax::class,
                'choice_label'=>'name',
                'label'=>$this->tr->trans('Tax class'),
                'placeholder'=>$this->tr->trans('Choose tax class')
            ])

        ;
        if(!$hasUser){
            $builder->add('user',EntityType::class,[
                'class'=>User::class,
                'choice_label'=>'username',
                'placeholder'=>$this->tr->trans("Attach user"),
                'label'=>$this->tr->trans('Available users'),
                'query_builder'=>function(EntityRepository $er){
                    $valid_ids = [];
                    $query = $this->em->createQuery('SELECT u.id AS user_id FROM App:User u 
                                                LEFT JOIN App:Employee e WITH u.id = e.user 
                                                LEFT JOIN App:Customer cu WITH u.id = cu.user 
                                                LEFT JOIN App:Client c WITH u.id = c.user
                                                 WHERE e.id IS NULL
                                                 AND cu.user IS NULL
                                                 AND c.user IS NULL
                                                 AND u.roles LIKE :roles');
                    $query->setParameter("roles","%"."ROLE_CUSTOMER"."%");
                    $res = $query->getArrayResult();
                    foreach($res as $r){
                        $valid_ids[] = $r['user_id'];
                    }
                    return $er->createQueryBuilder("u")
                             ->andWhere("u.id IN (:valid_ids)")
                             ->setParameter("valid_ids",$valid_ids);
                             
                              
                }
            ]);
        }else{
            $builder->add('user',EntityType::class,[
                'class'=>User::class,
                'choice_label'=>'username',
                'placeholder'=>$this->tr->trans("Attach user"),
                'label'=>$this->tr->trans('Available users'),
                'disabled'=>true
                
            ]);
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Customer::class,
        ]);
    }
}
