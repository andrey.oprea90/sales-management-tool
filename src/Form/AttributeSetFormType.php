<?php

namespace App\Form;

use App\Entity\AttributeSet;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type as Type;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\CallbackTransformer;
use App\Repository\AttributeSetRepository;
use App\Entity\Attribute;
use App\Repository\AttributeRepository;

class AttributeSetFormType extends AbstractType
{
    private $tr;
    
    public function __construct($tr){
        $this->tr = $tr;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $isEdit = !empty($options['data']) && $options['data']->getId()?$options['data']:null;

        $builder
            ->add('name',Type\TextType::class,[
                'label'=>$this->tr->trans('Name'),
                'empty_data'=>''
            ])
            ->add('attributes',EntityType::class,[
                'class'=>Attribute::class,
                'disabled'=>!$isEdit,
                'multiple'=>true,
                'choice_label'=>'name',
                'label'=>$this->tr->trans('Attributes'),
                'placeholder'=>$this->tr->trans('Add attribute'),
                'help'=>!$isEdit?$this->tr->trans('Please save first'):''
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AttributeSet::class,
        ]);
    }
}
