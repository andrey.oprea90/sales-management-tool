<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use App\Repository\UserRepository;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use App\Service\ReportsHelper;
use Carbon\Carbon;
use App\Entity\Warehouse;
use App\Repository\WarehouseRepository;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\Constraints as Assert;


class ApiController extends AbstractController
{
    
    /**
     * @Route("/api/login_check", name="api_login_check", methods="POST")
     */
    public function apiLoginCheck(Request $request,
                                 UserRepository $repository,
                                 UserPasswordEncoderInterface $passwordEncoder,
                                 JWTEncoderInterface $encoder,
                                 TranslatorInterface $trans){

        $username = $request->request->get('username',null);
        $password = $request->request->get('password',null);

        try{
            $user = $repository->findOneBy([
                'username'=>$username
            ]);
    
            if (!$user) {
                throw new \Exception($trans->trans("User not found"));
            }
            
            $isValid = $passwordEncoder->isPasswordValid($user,$password);
    
            if(!$isValid){
                throw new \Exception($trans->trans("Invalid credentials"));
            }
          
            $token = $encoder
                            ->encode([
                                'username' => $user->getUsername(),
                                'exp' => time() + 3600 
                            ]);

           return $this->json([
               "success"=>true,
               "token"=>$token
           ]);

        }catch(\Exception $e){

            return $this->json([
                "success"=>false,
                "error"=>$e->getMessage()
            ],401);
        }
        

       
    }


    /**
     * @Route("/api/reports/salesPerWarehouse", name="api_sales_per_warehouse", methods={"GET","POST"})
     * @IsGranted("ROLE_ADMIN_API")
     */
    public function index(Request $request,
                         ReportsHelper $reportsHelper,
                         WarehouseRepository $warehouseRepo,
                         ValidatorInterface $validator,
                         TranslatorInterface $trans)
    {
        try{

           $response = [
                [
                    "warehouse"=>"",
                    "totals"=>0
                ]
            ];
          
            $data = $request->getContent();
           
            if(!empty($data)){
                $data = json_decode($data,true);
            }

            if(json_last_error()!=JSON_ERROR_NONE){
                throw new \Exception($trans->trans("Invalid data passed"));
            }

            $rules = [];
         
            $rules['start'] = [
                new Assert\NotBlank(["message"=>$trans->trans("Please provide a valid start date")]),
                new Assert\NotNull(["message"=>$trans->trans("Please provide a valid start date")]),
                new Assert\DateTime([
                    "format"=>"Y-m-d H:i",
                    "message"=>$trans->trans("Please provide a valid start date in format Y-m-d H:i")
                ])
            ];

            $rules['end'] = [
                new Assert\NotBlank(["message"=>$trans->trans("Please provide a valid end date")]),
                new Assert\NotNull(["message"=>$trans->trans("Please provide a valid end date")]),
                new Assert\DateTime([
                    "format"=>"Y-m-d H:i",
                    "message"=>$trans->trans("Please provide a valid end date in format Y-m-d H:i")
                ]),
                new Assert\GreaterThan([
                    "value"=>$data['start'],
                    "message"=>$trans->trans("End date must be greater than start date")
                ])
            ];

            $warehouse = null;
            if(!empty($data['warehouse'])){
                $rules['warehouse'] = [
                    new Assert\NotBlank(["message"=>$trans->trans("Please provide a valid warehouse")]),
                    new Assert\NotNull(["message"=>$trans->trans("Please provide a valid warehouse")]),
                    new Assert\Type([
                        "type"=>"numeric",
                        "message"=>$trans->trans("Warehouse should be a valid number")
                    ])
                ];

                $warehouse = $warehouseRepo->find($data['warehouse']);
                if(!$warehouse){
                    throw new \Exception($trans->trans("Invalid warehouse"));
                }
            }

            $constraint = new Assert\Collection($rules);
          
            $violations = $validator->validate($data,$constraint);
            
            if($violations->count() > 0){
                $errorMessage = "";
                foreach($violations as $violation){
                    $errorMessage.=$violation->getMessage().".";
                }
                return $this->json($errorMessage,400);
            }

            $start = Carbon::parse($data['start']);
            $end = Carbon::parse($data['end']);

            $results = $reportsHelper->salesPerWarehouse($start,$end);

            if(!empty($results)){
                $response = [];
                foreach($results as $res){
                    $response[] = [
                        'warehouse'=>$res['warehouse_name'],
                        'totals'=>$res['total_incl_tax']
                    ];
                }
            }

            return $this->json($response);

        }catch(\Exception $e){
            return $this->json([
                "success"=>false,
                "message"=>$e->getMessage()
            ],400);
        }
        
    }

    /**
     * @Route("/api/reports/costsPerPeriod", name="api_costs_per_period", methods={"GET","POST"})
     * @IsGranted("ROLE_ADMIN_API")
     */
    public function costsPerPeriod(Request $request,
                         ReportsHelper $reportsHelper,
                         ValidatorInterface $validator,
                         TranslatorInterface $trans)
    {
        try{
           $default = Carbon::now();

           $defaultData = [
               "start"=>"",
               "end"=>"",
               "mode"=>""
           ];

           $response = [
                [
                    "period"=>$default->format("Y-m"),
                    "sell"=>0,
                    "buy"=>0
                ]
            ];
          
            $data = $request->getContent();
           
            if(!empty($data)){
                $data = json_decode($data,true);
            }

            if(json_last_error()!=JSON_ERROR_NONE){
                throw new \Exception($trans->trans("Invalid data passed"));
            }
            $data = array_merge($defaultData,$data);
            $rules = [];
           
            $rules['start'] = [
                new Assert\NotBlank(["message"=>$trans->trans("Please provide a valid start date")]),
                new Assert\NotNull(["message"=>$trans->trans("Please provide a valid start date")]),
                new Assert\DateTime([
                    "format"=>"Y-m-d H:i",
                    "message"=>$trans->trans("Please provide a valid start date in format Y-m-d H:i")
                ])
            ];

            $rules['end'] = [
                new Assert\NotBlank(["message"=>$trans->trans("Please provide a valid end date")]),
                new Assert\NotNull(["message"=>$trans->trans("Please provide a valid end date")]),
                new Assert\DateTime([
                    "format"=>"Y-m-d H:i",
                    "message"=>$trans->trans("Please provide a valid end date in format Y-m-d H:i")
                ]),
                new Assert\GreaterThan([
                    "value"=>$data['start'],
                    "message"=>$trans->trans("End date must be greater than start date")
                ])
            ];

            $rules['mode'] = [
                new Assert\NotBlank(["message"=>$trans->trans("Please provide a mode")]),
                new Assert\NotNull(["message"=>$trans->trans("Please provide a mode")]),
                new Assert\Type([
                    "type"=>"alpha",
                    "message"=>$trans->trans("Invalid mode")
                ]),
                new Assert\Choice([
                    "choices"=>["week","month"],
                    "message"=>$trans->trans("Mode parameter should be 'week' or 'month' ")
                ])
            ];

          

            $constraint = new Assert\Collection($rules);
          
            $violations = $validator->validate($data,$constraint);
            
            if($violations->count() > 0){
                $errorMessage = "";
                foreach($violations as $violation){
                    $errorMessage.=$violation->getMessage().".";
                }
                return $this->json($errorMessage,400);
            }
            
            $start = Carbon::parse($data['start']);
            $end = Carbon::parse($data['end']);

            $result = $reportsHelper->costsPerPeriod($start,$end,$data['mode']);
            
            return $this->json(array_values($result));

        }catch(\Exception $e){
            return $this->json([
                "success"=>false,
                "message"=>$e->getMessage()
            ],400);
        }
        
    }

        /**
     * @Route("/api/reports/additionalIncomeVsExpense", name="api_additional_income_expense_per_period", methods={"GET","POST"})
     * @IsGranted("ROLE_ADMIN_API")
     */
    public function additionalIncomeVsExpense(Request $request,
                         ReportsHelper $reportsHelper,
                         ValidatorInterface $validator,
                         TranslatorInterface $trans)
    {
        try{
           $default = Carbon::now();

           $defaultData = [
               "start"=>"",
               "end"=>"",
               "mode"=>""
           ];

           $response = [
                [
                    "period"=>$default->format("Y-m"),
                    "sell"=>0,
                    "buy"=>0
                ]
            ];
          
            $data = $request->getContent();
           
            if(!empty($data)){
                $data = json_decode($data,true);
            }

            if(json_last_error()!=JSON_ERROR_NONE){
                throw new \Exception($trans->trans("Invalid data passed"));
            }
            $data = array_merge($defaultData,$data);
            $rules = [];
           
            $rules['start'] = [
                new Assert\NotBlank(["message"=>$trans->trans("Please provide a valid start date")]),
                new Assert\NotNull(["message"=>$trans->trans("Please provide a valid start date")]),
                new Assert\DateTime([
                    "format"=>"Y-m-d H:i",
                    "message"=>$trans->trans("Please provide a valid start date in format Y-m-d H:i")
                ])
            ];

            $rules['end'] = [
                new Assert\NotBlank(["message"=>$trans->trans("Please provide a valid end date")]),
                new Assert\NotNull(["message"=>$trans->trans("Please provide a valid end date")]),
                new Assert\DateTime([
                    "format"=>"Y-m-d H:i",
                    "message"=>$trans->trans("Please provide a valid end date in format Y-m-d H:i")
                ]),
                new Assert\GreaterThan([
                    "value"=>$data['start'],
                    "message"=>$trans->trans("End date must be greater than start date")
                ])
            ];

            $rules['mode'] = [
                new Assert\NotBlank(["message"=>$trans->trans("Please provide a mode")]),
                new Assert\NotNull(["message"=>$trans->trans("Please provide a mode")]),
                new Assert\Type([
                    "type"=>"alpha",
                    "message"=>$trans->trans("Invalid mode")
                ]),
                new Assert\Choice([
                    "choices"=>["week","month"],
                    "message"=>$trans->trans("Mode parameter should be 'week' or 'month' ")
                ])
            ];

          

            $constraint = new Assert\Collection($rules);
          
            $violations = $validator->validate($data,$constraint);
            
            if($violations->count() > 0){
                $errorMessage = "";
                foreach($violations as $violation){
                    $errorMessage.=$violation->getMessage().".";
                }
                return $this->json($errorMessage,400);
            }
            
            $start = Carbon::parse($data['start']);
            $end = Carbon::parse($data['end']);

            $result = $reportsHelper->additionalIncomeExpensePerPeriod($start,$end,$data['mode']);
            
            return $this->json($result);

        }catch(\Exception $e){
            return $this->json([
                "success"=>false,
                "message"=>$e->getMessage()
            ],400);
        }
        
    }

    /**
     * @Route("/api/reports/totalSalesPerMonth", name="api_total_sales_per_month", methods={"GET","POST"})
     * @IsGranted("ROLE_ADMIN_API")
     */
    public function totalSalesPerMonth(Request $request,
                         ReportsHelper $reportsHelper,
                         ValidatorInterface $validator,
                         TranslatorInterface $trans)
    {
        try{
           $default = Carbon::now();

           $user = $this->getUser() ?? null;
           $reportsHelper->setUser($user);

           $defaultData = [
               "start"=>"",
               "end"=>"",
               "mode"=>""
           ];

           $response = [
                [
                    "period"=>$default->format("Y-m"),
                    "total"=>0,

                ]
            ];
          
            $data = $request->getContent();
           
            if(!empty($data)){
                $data = json_decode($data,true);
            }

            if(json_last_error()!=JSON_ERROR_NONE){
                throw new \Exception($trans->trans("Invalid data passed"));
            }
            $data = array_merge($defaultData,$data);
            $rules = [];
           
            $rules['start'] = [
                new Assert\NotBlank(["message"=>$trans->trans("Please provide a valid start date")]),
                new Assert\NotNull(["message"=>$trans->trans("Please provide a valid start date")]),
                new Assert\DateTime([
                    "format"=>"Y-m-d H:i",
                    "message"=>$trans->trans("Please provide a valid start date in format Y-m-d H:i")
                ])
            ];

            $rules['end'] = [
                new Assert\NotBlank(["message"=>$trans->trans("Please provide a valid end date")]),
                new Assert\NotNull(["message"=>$trans->trans("Please provide a valid end date")]),
                new Assert\DateTime([
                    "format"=>"Y-m-d H:i",
                    "message"=>$trans->trans("Please provide a valid end date in format Y-m-d H:i")
                ]),
                new Assert\GreaterThan([
                    "value"=>$data['start'],
                    "message"=>$trans->trans("End date must be greater than start date")
                ])
            ];

            $rules['mode'] = [
                new Assert\NotBlank(["message"=>$trans->trans("Please provide a mode")]),
                new Assert\NotNull(["message"=>$trans->trans("Please provide a mode")]),
                new Assert\Type([
                    "type"=>"alpha",
                    "message"=>$trans->trans("Invalid mode")
                ]),
                new Assert\Choice([
                    "choices"=>["week","month"],
                    "message"=>$trans->trans("Mode parameter should be 'week' or 'month' ")
                ])
            ];

          

            $constraint = new Assert\Collection($rules);
          
            $violations = $validator->validate($data,$constraint);
            
            if($violations->count() > 0){
                $errorMessage = "";
                foreach($violations as $violation){
                    $errorMessage.=$violation->getMessage().".";
                }
                return $this->json($errorMessage,400);
            }
            
            $start = Carbon::parse($data['start']);
            $end = Carbon::parse($data['end']);
           
            $result = $reportsHelper->costsPerPeriod($start,$end,$data['mode']="week");
            if(!empty($result)){
                $response = [];
            }
            array_walk($result,function($item,$date) use (&$response){
                $response[] = [
                    "period"=>$item['period'],
                    "total"=>$item['total_sell']
                ];
            });
            return $this->json([
                "success"=>true,
                "result"=>$response
            ]);

        }catch(\Exception $e){
            return $this->json([
                "success"=>false,
                "result"=>[],
                "message"=>$e->getMessage()
            ],400);
        }
        
    }

    /**
     * @Route("/api/reports/totalSalesPerPeriod", name="api_sales_per_period", methods={"GET","POST"})
     * @IsGranted("ROLE_ADMIN_API")
     */
    public function totalSalesPerPeriod(Request $request,
                         ReportsHelper $reportsHelper,
                         ValidatorInterface $validator,
                         $tr)
    {
        try{
           $now = Carbon::now();
           $user = $this->getUser() ?? null;
           $reportsHelper->setUser($user);
           $defaultData = [
               "start"=>"",
               "end"=>"",
           ];

           $response = [
                [
                    $now->format("M"),
                    0
                ],
                [
                    $now->addMonth()->format("M"),
                    0
                ],
                [
                    $now->addMonth()->format("M"),
                    0
                ]
            ];
          
            $data = $request->getContent();
           
            if(!empty($data)){
                $data = json_decode($data,true);
            }

            if(json_last_error()!=JSON_ERROR_NONE){
                throw new \Exception($tr->trans("Invalid data passed"));
            }
            $data = array_merge($defaultData,$data);
            $rules = [];
           
            $rules['start'] = [
                new Assert\NotBlank(["message"=>$tr->trans("Please provide a valid start date")]),
                new Assert\NotNull(["message"=>$tr->trans("Please provide a valid start date")]),
                new Assert\DateTime([
                    "format"=>"Y-m-d H:i",
                    "message"=>$tr->trans("Please provide a valid start date in format Y-m-d H:i")
                ])
            ];

            $rules['end'] = [
                new Assert\NotBlank(["message"=>$tr->trans("Please provide a valid end date")]),
                new Assert\NotNull(["message"=>$tr->trans("Please provide a valid end date")]),
                new Assert\DateTime([
                    "format"=>"Y-m-d H:i",
                    "message"=>$tr->trans("Please provide a valid end date in format Y-m-d H:i")
                ]),
                new Assert\GreaterThan([
                    "value"=>$data['start'],
                    "message"=>$tr->trans("End date must be greater than start date")
                ])
            ];

           

            $constraint = new Assert\Collection($rules);
          
            $violations = $validator->validate($data,$constraint);
            
            if($violations->count() > 0){
                $errorMessage = "";
                foreach($violations as $violation){
                    $errorMessage.=$violation->getMessage().".";
                }
                return $this->json($errorMessage,400);
            }
            
            $start = Carbon::parse($data['start']);
            $end = Carbon::parse($data['end']);
           if(!$start->isSameYear($end)){
               $end = $start->copy()->endOfYear();
           }
            $results = $reportsHelper->totalSalesPerPeriod($start,$end);
            if(!empty($results)){
                $response = [];
            }
           foreach($results as $res){
                $response[] = [
                    $res['label'],
                    $res['total']
                ];
           }
           
            return $this->json($response);

        }catch(\Exception $e){
            return $this->json([
                "success"=>false,
                "message"=>$e->getMessage()
            ],400);
        }
        
    }

       /**
     * @Route("/api/reports/incomeVsExpensePerPeriod", name="api_income_vs_expense_per_period", methods={"GET","POST"})
     * @IsGranted("ROLE_ADMIN_API")
     */
    public function incomeVsExpensePerPeriod(Request $request,
                         ReportsHelper $reportsHelper,
                         ValidatorInterface $validator,
                         $tr)
    {
        try{
        
           $default = Carbon::now();

           $defaultData = [
               "start"=>"",
               "end"=>"",
               "include_stock"=>""
           ];

           $response = [
            "income"=>[
                "sales"=>[
                    "label"=>$tr->trans("Sales (Income)"),
                    "name"=>$tr->trans("Sales"),
                    "data"=>0
                ],
                "additional"=>[
                    "label"=>$tr->trans("Additional (Income)"),
                    "name"=>$tr->trans("Additional"),
                    "data"=>0
                ]
            ],
            "expense"=>[
                "products"=>[
                    "label"=>$tr->trans("Products (Expense)"),
                    "name"=>$tr->trans("Products"),
                    "data"=>0
                ],
                "products_in_stock"=>[
                    "label"=>$tr->trans("Products in stock (Expense)"),
                    "name"=>$tr->trans("Products in stock"),
                    "data"=>0
                ],
                "taxes"=>[
                    "label"=>$tr->trans("Taxes (Expense)"),
                    "name"=>$tr->trans("Taxes"),
                    "data"=>0
                ],
                "additional"=>[
                    "label"=>$tr->trans("Additional (Expense)"),
                    "name"=>$tr->trans("Additional"),
                    "data"=>0
                ]
            ],
            "total_income"=>[
                "label"=>$tr->trans("Income"),
                "data"=>0
            ],
            "total_expense"=>[
                "label"=>$tr->trans("Expense"),
                "data"=>0
            ]
        ];
          
            $data = $request->getContent();
           
            if(!empty($data)){
                $data = json_decode($data,true);
            }

            if(json_last_error()!=JSON_ERROR_NONE){
                throw new \Exception($tr->trans("Invalid data passed"));
            }
            $data = array_merge($defaultData,$data);
            $rules = [];
           
            $rules['start'] = [
                new Assert\NotBlank(["message"=>$tr->trans("Please provide a valid start date")]),
                new Assert\NotNull(["message"=>$tr->trans("Please provide a valid start date")]),
                new Assert\DateTime([
                    "format"=>"Y-m-d H:i",
                    "message"=>$tr->trans("Please provide a valid start date in format Y-m-d H:i")
                ])
            ];

            $rules['end'] = [
                new Assert\NotBlank(["message"=>$tr->trans("Please provide a valid end date")]),
                new Assert\NotNull(["message"=>$tr->trans("Please provide a valid end date")]),
                new Assert\DateTime([
                    "format"=>"Y-m-d H:i",
                    "message"=>$tr->trans("Please provide a valid end date in format Y-m-d H:i")
                ]),
                new Assert\GreaterThan([
                    "value"=>$data['start'],
                    "message"=>$tr->trans("End date must be greater than start date")
                ])
            ];

            $rules['include_stock'] = [
                new Assert\NotBlank(["message"=>$tr->trans("Please provide a value for the include_stock")]),
                new Assert\Type([
                    "type"=>"numeric",
                    "message"=>$tr->trans("Invalid value")
                ]),
                new Assert\Choice([
                    "choices"=>["0","1"],
                    "message"=>$tr->trans("Mode parameter should be '0' or '1' ")
                ])
            ];

            $constraint = new Assert\Collection($rules);
          
            $violations = $validator->validate($data,$constraint);
            
            if($violations->count() > 0){
                $errorMessage = "";
                foreach($violations as $violation){
                    $errorMessage.=$violation->getMessage().".";
                }
                return $this->json($errorMessage,400);
            }
            
            $start = Carbon::parse($data['start']);
            $end = Carbon::parse($data['end']);
            $includeStock = (bool) $data['include_stock'];

            $result = $reportsHelper->incomeVsExpenseDetailed($start,$end,$includeStock);
            
            return $this->json($result);

        }catch(\Exception $e){
            return $this->json([
                "success"=>false,
                "message"=>$e->getMessage()
            ],400);
        }
        
    }

 
}
