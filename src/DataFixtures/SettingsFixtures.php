<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Setting;

class SettingsFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $setting = new Setting();
        $setting->setCompanyName("Test company");
        $setting->setVatNumber("1231231");
        $setting->setCompanyPhone("1232132131231");
        $setting->setCompanyAddress("asdsa sadsa sdasad");
        $manager->persist($setting);
        
        $manager->flush();
    }
}
