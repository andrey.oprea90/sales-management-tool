<?php 

namespace App\Form\DataObjects;
use Symfony\Component\Validator\Constraints as Assert;
use App\Service\FileHelper;
use App\Entity\Attribute;

class AttributeOption{

    private function __construct(){

    }

    /**
     * @Assert\NotBlank(message="Please choose a name")
     * @Assert\Length(
     *      min = 2,
     *      max = 200,
     *      minMessage = "Option name must be at least {{ limit }} characters long",
     *      maxMessage = "Option name cannot be longer than {{ limit }} characters"
     * )
     */
    private $name;

    private $code;

    /**
     * @Assert\Choice({"0","1",0,1})
     */
    private $default;

    public function setName(?string $name):self{
        $this->name = $name;
        return $this;
    }

    public function getName():?string{

        return $this->name;
        
    }

    public function setCode(?string $code):self{
        $this->code = $code;
        return $this;
    }

    public function getCode():?string{

        return $this->code;
        
    }

    public function setDefault(int $default):self{
        $this->default = $default;
        return $this;
    }

    public function getDefault():int{

       return $this->default;
        
    }

    public static function createFromData(array $data = []):self{

        $data = array_merge([
            "name"=>null,
            "default"=>"0"
        ],$data);

        $data['default'] = (int) $data['default'];
        $obj = new self();
        $obj->setName($data['name']);
        $code = $obj->getName() ? str_replace("-","_",FileHelper::slugify($obj->getName())):null;
        $obj->setCode($code);
        $obj->setDefault($data['default']);

        return $obj;
    }

   
}