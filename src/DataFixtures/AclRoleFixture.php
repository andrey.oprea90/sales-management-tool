<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\AclRole;

class AclRoleFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $roles = [
            [
                "name"=>"Admin",
                "code"=>"admin",
                "access"=>5
            ],
            [
                "name"=>"User",
                "code"=>"user",
                "access"=>1
            ],
        ];

        foreach($roles as $role){
            
            $acl_role = new AclRole();
            $acl_role->setName($role['name']);
            $acl_role->setCode($role['code']);
            $acl_role->setAccess($role['access']);

            $manager->persist($acl_role);
        }
        $manager->flush();
    }
}
