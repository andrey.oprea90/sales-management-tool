<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Form\DataObjects\OrderItemMovement;
use Symfony\Component\Form\Extension\Core\Type as Type;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Entity\Warehouse;
use App\Repository\ProductRepository;
use App\Entity\Product;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;
use Doctrine\ORM\EntityRepository;

class OrderItemMovementFormType extends AbstractType
{
    private $tr;
    
    public function __construct($tr){
        $this->tr = $tr;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('qty',Type\TextType::class,[
                'label'=>$this->tr->trans('Qty to add')
            ])
            ->add('fromWarehouse',EntityType::class,[
                'class'=>Warehouse::class,
                'choice_label'=>'name',
                'placeholder'=>$this->tr->trans("From warehouse"),
                'label'=>$this->tr->trans('From warehouse'),
                'help'=>$this->tr->trans('Leave empty for moving from not assigned qty'),
                'empty_data'=>null,
                'query_builder'=>function(EntityRepository $er){
                       
                    return $er->createQueryBuilder("w")
                             ->andWhere("w.deleted_at IS NULL");
                             
                             
                              
                }
                
            ])
            ->add('product',Select2EntityType::class,[
                            'multiple' => false,
                            'remote_route' => 'products_ajax',
                            'class'=>Product::class,
                            'primary_key' => 'id',
                            'text_property' => 'title',
                            'minimum_input_length' => 2,
                            'page_limit' => 10,
                            'allow_clear' => true,
                            'delay' => 250,
                            'cache' => false,
                            'cache_timeout' => 60000, // if 'cache' is true
                            'language' => 'en',
                            'placeholder' => $this->tr->trans('Add products to order'),
                            'scroll'=>true,
                            'help'=>$this->tr->trans('Other product details will be set in the next step'),
                            'constraints'=>[
                                new Assert\NotBlank([
                                    'message'=>$this->tr->trans('Please select a product')
                                ])
                            ]
                        ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => OrderItemMovement::class,
        ]);
    }
}
