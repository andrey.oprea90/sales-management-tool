<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use App\Entity\Tax;
use App\Repository\TaxRepository;
use App\Form\TaxFormType;
use AndreaSprega\Bundle\BreadcrumbBundle\Annotation\Breadcrumb;

/**
 * IsGranted("ROLE_ADMIN_TAXES")
 * @Breadcrumb({"label" = "Tax classes list", "route" = "taxes" })
 */
class TaxesController extends AbstractController
{
    /**
     * @Route("/taxes/{page}", name="taxes", defaults={"page":"1"}, requirements={"page":"\d+"})
     */
    public function index(Request $request, 
                         TaxRepository $taxRepo,
                        $page
                        )
    {
        $taxes = $taxRepo->findAllPaginated($page,$this->getParameter('default_per_page'));

        return $this->render('taxes/index.html.twig', [
            'page_title' => 'Tax classes list',
            'taxes'=>$taxes
        ]);
    }

    /**
     * @Route("/taxes/add", name="taxes_add")
     */
    public function add(Request $request,
                        EntityManagerInterface $em,
                        $tr){

        try{
            $tax = new Tax();
            $form = $this->createForm(TaxFormType::class,$tax);

            $form->handleRequest($request);

            if($form->isSubmitted() && $form->isValid()){

                $em->persist($tax);
                $em->flush();

                $this->addFlash("success",$tr->trans("Tax class saved"));
                return $this->redirectToRoute("taxes");
            }
        }catch(\Exception $e){
           
            $this->addFlash("error",$e->getMessage());
            return $this->redirectToRoute("taxes");
        }
        

        return $this->render('taxes/add.html.twig',[
            'page_title'=>'Add tax class',
            'form'=>$form->createView()
        ]);
    }

    /**
     * @Route("/taxes/edit/{id}", name="taxes_edit", requirements={"id":"\d+"})
     */
    public function edit(Request $request,
                        EntityManagerInterface $em,
                        Tax $tax,
                        $tr){

        try{
           
            $form = $this->createForm(TaxFormType::class,$tax);

            $form->handleRequest($request);

            if($form->isSubmitted() && $form->isValid()){

                $em->flush();

                $this->addFlash("success",$tr->trans("Tax class edited"));
                return $this->redirectToRoute("taxes_edit",[
                    "id"=>$tax->getId()
                ]);
            }
        }catch(\Exception $e){
           
            $this->addFlash("error",$e->getMessage());
            return $this->redirectToRoute("taxes");
        }
        

        return $this->render('taxes/edit.html.twig',[
            'page_title'=>'Edit tax class',
            'form'=>$form->createView()
        ]);
    }

  

    /**
     * @Route("/taxes/delete/{id}", name="taxes_delete", requirements={"id":"\d+"})
     */
    public function delete(Request $request,
                           EntityManagerInterface $em,
                           Tax $tax,
                           $tr
                           ){

        try{
            
            $em->remove($tax);
            $em->flush();

            $this->addFlash("success",$tr->trans("Tax class deleted"));
            return $this->redirectToRoute("taxes");

        }catch(\Exception $e){

            $this->addFlash("error",$e->getMessage());
            return $this->redirectToRoute("taxes");
        }          

    }
}
