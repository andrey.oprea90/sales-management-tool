<?php

namespace App\Form;

use App\Entity\Warehouse;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type as Type;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints as Assert;

class WarehouseFormType extends AbstractType
{
    private $tr;
    
    public function __construct($tr){
        $this->tr = $tr;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',Type\TextType::class,[
                'label'=>$this->tr->trans('Name'),
                'empty_data'=>''
            ])
            ->add('details',Type\TextareaType::class,[
                'label'=>$this->tr->trans('Details'),
                'empty_data'=>''
            ])
            ->add('address',Type\TextareaType::class,[
                'label'=>$this->tr->trans('Address'),
                'empty_data'=>''
            ]);
           
        
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Warehouse::class,
        ]);
    }
}
