<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use App\Entity\Customer;
use App\Repository\CustomerRepository;
use App\Form\CustomersFormType;
use AndreaSprega\Bundle\BreadcrumbBundle\Annotation\Breadcrumb;

/**
 * @IsGranted("ROLE_ADMIN_DASHBOARD")
 * @Breadcrumb({"label" = "Customers", "route" = "customers" })
 */
class CustomersController extends AbstractController
{
    /**
     * @IsGranted("ROLE_ADMIN_CUSTOMERS")
     * @Route("/customers/{page}", name="customers", defaults={"page":"1"}, requirements={"page":"\d+"})
     */
    public function index(Request $request,
                          EntityManagerInterface $em,
                          CustomerRepository $repo,
                          $page = 1
                          )
    {
        $customers = $repo->findAllPaginated($page,$this->getParameter('default_per_page'));
        
        return $this->render('customers/index.html.twig', [
            'page' => 'Customers',
            'customers'=>$customers
        ]);
    }

    /**
     * @IsGranted("ROLE_ADMIN_CUSTOMERS")
     * @Route("/customers/restore/{page}", name="customers_restore", defaults={"page":"1"}, requirements={"page":"\d+"})
     * @Breadcrumb({"label" = "Restore"})
     */
    public function indexRestore(Request $request,
                          EntityManagerInterface $em,
                          CustomerRepository $repo,
                          $page = 1
                          )
    {
        $customers = $repo->findAllPaginated($page,$this->getParameter('default_per_page'),true);
        
        return $this->render('customers/restore.html.twig', [
            'page' => 'Restore customers',
            'customers'=>$customers
        ]);
    }

    /**
     * @IsGranted("ROLE_ADMIN_CUSTOMERS")
     * @Route("/customer/add", name="customers_add")
     */
    public function add(Request $request,
                        EntityManagerInterface $em,
                        $tr)
    {
        try{
            $customer = new Customer();

            $form = $this->createForm(CustomersFormType::class,$customer);
            $form->handleRequest($request);

            if($form->isSubmitted() && $form->isValid()){
                
                $em->persist($customer);
                $em->flush();

                $this->addFlash("success",$tr->trans("Customer saved"));
                return $this->redirectToRoute("customers");
            }

            return $this->render('customers/add.html.twig',[
                "page_title"=>"Add customer",
                "form"=>$form->createView(),
                "customer"=>$customer
            ]);

        }catch(\Exception $e){
            
            $this->addFlash("error",$e->getMessage());

            return $this->redirectToRoute("customers");
        }
        
    }

    /**
     * @Route("/customer/edit/{id}", name="customers_edit", requirements={"id":"\d+"})
     */
    public function edit(Request $request,
                        EntityManagerInterface $em,
                        Customer $customer,
                        $tr
                        )
    {
        $this->denyAccessUnlessGranted('CUSTOMER_EDIT',$customer);
        try{
            
            $form = $this->createForm(CustomersFormType::class,$customer);
            $form->handleRequest($request);

            if($form->isSubmitted() && $form->isValid()){
                
                $em->flush();

                $this->addFlash("success",$tr->trans("Customer edited"));
                
            }

            return $this->render('customers/edit.html.twig',[
                "page_title"=>"Add customer",
                "form"=>$form->createView(),
                "customer"=>$customer
            ]);

        }catch(\Exception $e){
           
            $this->addFlash("error",$e->getMessage());

            return $this->redirectToRoute("customers");
        }
    }

    /**
     * @IsGranted("ROLE_ADMIN_CUSTOMERS")
     * @Route("/customer/delete/{id}", name="customers_delete", requirements={"id":"\d+"})
     */
    public function delete(Request $request,
                        EntityManagerInterface $em,
                        Customer $customer,
                        $tr
                        )
    {
        try{
            //$em->remove($customer);
            $customer->setDeletedAt(new \DateTime());
            $em->flush();

            $this->addFlash("success",$tr->trans("Customer deleted"));

            return $this->redirectToRoute("customers");

        }catch(\Exception $e){
            $this->addFlash("error",$e->getMessage());

            return $this->redirectToRoute("customers");
        }
        
    }

    /**
     * @IsGranted("ROLE_ADMIN_CUSTOMERS")
     * @Route("/customer/restore/item/{id}", name="customers_item_restore", requirements={"id":"\d+"})
     */
    public function restore(Request $request,
                        EntityManagerInterface $em,
                        Customer $customer,
                        $tr
                        )
    {
        try{
            //$em->remove($customer);
            $customer->setDeletedAt(null);
            $em->flush();

            $this->addFlash("success",$tr->trans("Customer restored"));

            return $this->redirectToRoute("customers");

        }catch(\Exception $e){
            $this->addFlash("error",$e->getMessage());

            return $this->redirectToRoute("customers");
        }
        
    }
}
