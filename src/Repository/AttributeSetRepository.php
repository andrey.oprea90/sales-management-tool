<?php

namespace App\Repository;

use App\Entity\AttributeSet;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @method AttributeSet|null find($id, $lockMode = null, $lockVersion = null)
 * @method AttributeSet|null findOneBy(array $criteria, array $orderBy = null)
 * @method AttributeSet[]    findAll()
 * @method AttributeSet[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AttributeSetRepository extends ServiceEntityRepository
{
    private $paginator;
    
    public function __construct(RegistryInterface $registry,PaginatorInterface $paginator)
    {
        $this->paginator = $paginator;
        parent::__construct($registry, AttributeSet::class);
    }

    // /**
    //  * @return AttributeSet[] Returns an array of AttributeSet objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AttributeSet
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function findAllPaginated(int $page = 1, int $perPage = 10){

        $query = $this->createQueryBuilder('ats');

        $query->orderBy("ats.name","ASC")
                    ->getQuery();

        return $this->paginator->paginate($query,$page,$perPage);

    }
}
