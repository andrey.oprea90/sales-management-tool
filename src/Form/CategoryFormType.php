<?php

namespace App\Form;

use App\Entity\Category;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type as Type;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\File\File;


class CategoryFormType extends AbstractType
{
    private $params;

    private $tr;
    
    public function __construct(ParameterBagInterface $params,$tr){

        $this->params = $params;
        $this->tr = $tr;

    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',Type\TextType::class,[
                'label'=>$this->tr->trans('Name')
            ])
            ->add('short_description',Type\TextareaType::class,[
                'label'=>$this->tr->trans('Short description')
            ])
            ->add('main_image',Type\FileType::class,[
                'label'=>$this->tr->trans('Image'),
                'constraints'=>[
                    new Assert\File([
                        'maxSize'=>"5M",
                        'mimeTypes'=>[
                            "image/*"
                        ]
                    ])
                ]
            ])
        ;

        $builder->get('main_image')->addModelTransformer(new CallBackTransformer(
            function($data){
                $uploadsDir = $this->params->get("default_category_upload_dir");
                if(!empty($data) && is_string($data)){
                    if(is_file($uploadsDir.DIRECTORY_SEPARATOR.$data)){
                        $file = new File($uploadsDir.DIRECTORY_SEPARATOR.$data);
                        
                        return $file;
                    }
                }
            },
            function($data){
                return $data;
            }
        ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Category::class,
        ]);
    }
}
