<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SettingRepository")
 * @ORM\Table(name="settings")
 * @ORM\HasLifecycleCallbacks()
 */
class Setting
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = 4,
     *      max = 2000,
     *      minMessage = "Company name must be at least {{ limit }} characters long",
     *      maxMessage = "Company name cannot be longer than {{ limit }} characters"
     * )
     */
    private $company_name;


    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, nullable=true, options={"default":0})
     * @Assert\NotBlank()
     * @Assert\Type(
     *     type="numeric",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    private $standard_tax_rate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(
     *      min = 4,
     *      max = 2000,
     *      minMessage = "Company phone must be at least {{ limit }} characters long",
     *      maxMessage = "Company phone cannot be longer than {{ limit }} characters"
     * )
     */
    private $company_phone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Email()
     */
    private $company_email;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Assert\Length(
     *      min = 4,
     *      max = 2000,
     *      minMessage = "Company address must be at least {{ limit }} characters long",
     *      maxMessage = "Company address cannot be longer than {{ limit }} characters"
     * )
     */
    private $company_address;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Currency", cascade={"persist", "remove"})
     */
    private $currency;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $vatnumber;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(
     *      min = 2,
     *      max = 2000,
     *      minMessage = "Default invoice series must be at least {{ limit }} characters long",
     *      maxMessage = "Default invoice series cannot be longer than {{ limit }} characters"
     * )
     * @Assert\Type(
     *        type="alpha",
     *        message="Please use only letters"
     * )
     */
    private $default_invoice_series;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCompanyName(): ?string
    {
        return $this->company_name;
    }

    public function setCompanyName(string $company_name): self
    {
        $this->company_name = $company_name;

        return $this;
    }

 
    public function getStandardTaxRate()
    {
        return $this->standard_tax_rate;
    }

    public function setStandardTaxRate($standard_tax_rate): self
    {
        $this->standard_tax_rate = $standard_tax_rate;

        return $this;
    }

    public function getCompanyPhone(): ?string
    {
        return $this->company_phone;
    }

    public function setCompanyPhone(?string $company_phone): self
    {
        $this->company_phone = $company_phone;

        return $this;
    }

    public function getCompanyEmail(): ?string
    {
        return $this->company_email;
    }

    public function setCompanyEmail(?string $company_email): self
    {
        $this->company_email = $company_email;

        return $this;
    }

    public function getCompanyAddress(): ?string
    {
        return $this->company_address;
    }

    public function setCompanyAddress(?string $company_address): self
    {
        $this->company_address = $company_address;

        return $this;
    }

    public function getCurrency(): ?Currency
    {
        return $this->currency;
    }

    public function setCurrency(?Currency $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function getInvoiceData():array{
        return [
            "name"=>$this->company_name,
            "email"=>$this->company_email,
            "phone"=>$this->company_phone,
            "vat"=>$this->vatnumber,
            "address"=>wordwrap($this->company_address,80,"<br />\n")
        ];
    }

    public function getVatnumber(): ?string
    {
        return $this->vatnumber;
    }

    public function setVatnumber(?string $vatnumber): self
    {
        $this->vatnumber = $vatnumber;

        return $this;
    }

    public function getDefaultInvoiceSeries(): ?string
    {
        return $this->default_invoice_series;
    }

    public function setDefaultInvoiceSeries(?string $default_invoice_series): self
    {
        $this->default_invoice_series = $default_invoice_series;

        return $this;
    }

     /**
     * @ORM\PreFlush
     */
    public function setDefaultValues(){
        if($this->default_invoice_series){
            $this->default_invoice_series = strtoupper(preg_replace("#[^a-zA-Z]#","",$this->default_invoice_series));
        }
    }
}
