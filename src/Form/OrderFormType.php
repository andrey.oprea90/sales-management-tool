<?php

namespace App\Form;

use App\Entity\Order;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type as Type;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\CallbackTransformer;
use App\Entity\OrderItem;
use App\Entity\Product;
use App\Repository\OrderRepository;
use App\Repository\OrderItemRepository;
use App\Repository\ProductRepository;
use App\Entity\Category;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;
use App\Entity\Customer;
use App\Entity\Client;
use App\Service\OrderHelper;
use Doctrine\ORM\EntityRepository;

class OrderFormType extends AbstractType
{
    private $orderHelper;

    private $tr;

    public function __construct(OrderHelper $orderHelper,$tr){
        $this->orderHelper = $orderHelper;
        $this->tr = $tr;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
       $edit_order = !empty($options['data']) && $options['data']->getId()?$options['data']:null;

       if(!$edit_order){
                $builder
                        ->add('customer',EntityType::class,[
                            'class'=>Customer::class,
                            'choice_label'=>function($customer){
                                return $customer->getFirstName()." ".$customer->getLastName()." (".$customer->getEmail().")";
                            },
                            'placeholder'=>$this->tr->trans("Select customer"),
                            'label'=>$this->tr->trans('Customer'),
                            'help'=>$this->tr->trans('Please select only customer or client not both'),
                            'disabled'=>$edit_order,
                            'query_builder'=>function(EntityRepository $er){
                       
                                return $er->createQueryBuilder("cu")
                                         ->andWhere("cu.deleted_at IS NULL");
                                         
                                         
                                          
                            }
                        ])
                        ->add('client',EntityType::class,[
                            'class'=>Client::class,
                            'choice_label'=>function($client){
                                return $client->getName()." (".$client->getEmail().")";
                            },
                            'placeholder'=>$this->tr->trans("Select client"),
                            'label'=>$this->tr->trans('Client (Company)'),
                            'help'=>$this->tr->trans('Please select only customer or client not both'),
                            'disabled'=>$edit_order,
                            'query_builder'=>function(EntityRepository $er){
                       
                                return $er->createQueryBuilder("c")
                                         ->andWhere("c.deleted_at IS NULL");
                                         
                                         
                                          
                            }
                        ])
                        ->add('number',Type\HiddenType::class,[
                            'data'=>$this->orderHelper->generateUniqueOrderNumber()
                        ])
                        ->add('total_incl_tax',Type\HiddenType::class,[
                            'data'=>0
                        ])
                        ->add('total_excl_tax',Type\HiddenType::class,[
                            'data'=>0
                        ])
                        ->add('tax',Type\HiddenType::class,[
                            'data'=>0
                        ])
                        ->add('status',Type\HiddenType::class,[
                            'data'=>Order::STATUS_NEW
                        ])
                        // ->add('products',Select2EntityType::class,[
                        //     'multiple' => true,
                        //     'remote_route' => 'products_ajax',
                        //     'class'=>Product::class,
                        //     'primary_key' => 'id',
                        //     'text_property' => 'title',
                        //     'minimum_input_length' => 2,
                        //     'page_limit' => 10,
                        //     'allow_clear' => true,
                        //     'delay' => 250,
                        //     'cache' => false,
                        //     'cache_timeout' => 60000, // if 'cache' is true
                        //     'language' => 'en',
                        //     'placeholder' => 'Add products to order',
                        //     'scroll'=>true,
                        //     'mapped'=>false,
                        //     'help'=>'Other product details will be set in the next step',
                        //     'constraints'=>[
                        //         new Assert\NotBlank([
                        //             'message'=>'Please select at least one product'
                        //         ])
                        //     ]
                        // ])
            ;
       }else{
           $clientData = $edit_order->getClientData();
           $isCompany = $clientData['is_company'];
            $builder
                ->add('status',Type\HiddenType::class,[
                    'data'=>$edit_order->getStatus() == Order::STATUS_NEW?Order::STATUS_COMPLETE:Order::STATUS_CANCELED
                ])
                ->add('show_number',Type\TextType::class,[
                    'mapped'=>false,
                    'disabled'=>true,
                    'data'=>"#".$edit_order->getNumber(),
                    'label'=>$this->tr->trans('Number')
                ])
                ->add('show_created_at',Type\TextType::class,[
                    'mapped'=>false,
                    'disabled'=>true,
                    'data'=>$edit_order->getDateCreated()->format('d.m.Y H:i'),
                    'label'=>$this->tr->trans('Created at')
                ])
                ->add('show_status',Type\TextType::class,[
                    'mapped'=>false,
                    'disabled'=>true,
                    'data'=>ucfirst($edit_order->getStatus()),
                    'label'=>$this->tr->trans('Status')
                ])->add('show_total_excl_tax',Type\TextType::class,[
                    'mapped'=>false,
                    'disabled'=>true,
                    'data'=>$edit_order->getFormattedTotalExclTax(),
                    'label'=>$this->tr->trans('Total excl tax')
                ])->add('show_tax',Type\TextType::class,[
                    'mapped'=>false,
                    'disabled'=>true,
                    'data'=>$edit_order->getTax(),
                    'label'=>$this->tr->trans('Tax')
                ])->add('show_total_incl_tax',Type\TextType::class,[
                    'mapped'=>false,
                    'disabled'=>true,
                    'data'=>$edit_order->getFormattedTotalInclTax(),
                    'label'=>$this->tr->trans('Total incl tax')
                ])->add('show_items_ordered',Type\TextType::class,[
                    'mapped'=>false,
                    'disabled'=>true,
                    'data'=>$edit_order->getItems()->count(),
                    'label'=>$this->tr->trans('Number of items ordered')
                ]);
                $builder->add('show_is_company',Type\TextType::class,[
                    'mapped'=>false,
                    'disabled'=>true,
                    'data'=>$isCompany?"Yes":"No",
                    'label'=>$this->tr->trans('Is company')
                ])->add('show_name',Type\TextType::class,[
                    'mapped'=>false,
                    'disabled'=>true,
                    'data'=>$clientData['name'],
                    'label'=>$this->tr->trans('Name')
                ]);
                
                if($isCompany){
                    $builder->add('show_vat',Type\TextType::class,[
                        'mapped'=>false,
                        'disabled'=>true,
                        'data'=>$clientData['vat'],
                        'label'=>$this->tr->trans('VAT number')
                    ]);
                    $builder->add('show_id_card',Type\HiddenType::class,[
                        'mapped'=>false,
                        'disabled'=>true,
                        'data'=>$clientData['id_card']
                        
                    ]);
                }else{
                    $builder->add('show_vat',Type\HiddenType::class,[
                        'mapped'=>false,
                        'disabled'=>true,
                        'data'=>$clientData['vat']
                        
                    ]);
                    $builder->add('show_id_card',Type\TextType::class,[
                        'mapped'=>false,
                        'disabled'=>true,
                        'data'=>$clientData['id_card'],
                        'label'=>$this->tr->trans('ID card number')
                    ]);
                }
                $builder->add('show_email',Type\TextType::class,[
                    'mapped'=>false,
                    'disabled'=>true,
                    'data'=>$clientData['email'],
                    'label'=>$this->tr->trans('Email')
                    ])->add('show_phone',Type\TextType::class,[
                        'mapped'=>false,
                        'disabled'=>true,
                        'data'=>$clientData['phone'],
                        'label'=>$this->tr->trans('Phone')
                    ])->add('show_identification',Type\TextType::class,[
                        'mapped'=>false,
                        'disabled'=>true,
                        'data'=>$clientData['identification'],
                        'label'=>$this->tr->trans('Identification')
                    ])->add('show_details',Type\TextareaType::class,[
                        'mapped'=>false,
                        'disabled'=>true,
                        'data'=>$clientData['details'],
                        'label'=>$this->tr->trans('Details')
                    ])->add('show_shipping_address',Type\TextareaType::class,[
                        'mapped'=>false,
                        'disabled'=>true,
                        'data'=>$clientData['shipping_address'],
                        'label'=>$this->tr->trans('Shipping address')
                    ])->add('show_billing_address',Type\TextareaType::class,[
                        'mapped'=>false,
                        'disabled'=>true,
                        'data'=>$clientData['billing_address'],
                        'label'=>$this->tr->trans('Billing address')
                    ]);

       }
        
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Order::class,
        ]);
    }
}
