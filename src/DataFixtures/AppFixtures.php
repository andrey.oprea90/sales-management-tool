<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Entity\User;
use Carbon\Carbon;

class AppFixtures extends Fixture
{

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;

    }

    public function load(ObjectManager $manager)
    {
        $normal_user = new User();

        $normal_user->setFullname("Normal user");
        $normal_user->setUsername("test_user");
        $normal_user->setEmail("test_user@test.com");
        $normal_user->setPassword($this->passwordEncoder->encodePassword($normal_user,"testpass"));

        $admin_user = new User();

        $admin_user->setFullname("Admin user");
        $admin_user->setUsername("admin_user");
        $admin_user->setEmail("admin_user@test.com");
        $admin_user->setPassword($this->passwordEncoder->encodePassword($normal_user,"testpass"));
        $admin_user->setRoles(["ROLE_ADMIN"]);

        $manager->persist($admin_user);
        $manager->persist($normal_user);
        $manager->flush();
    }
}
