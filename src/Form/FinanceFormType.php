<?php

namespace App\Form;

use App\Entity\Finance;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type as Type;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\CallbackTransformer;



class FinanceFormType extends AbstractType
{
    private $tr;
    
    public function __construct($tr){
        $this->tr = $tr;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $isEdit = !empty($options['data']) && $options['data'] instanceof Finance && $options['data']->getId();

        $builder
            ->add('name',Type\TextType::class,[
                'label'=>$this->tr->trans('Name'),
                'empty_data'=>''
            ])
            ->add('cost',Type\TextType::class,[
                'label'=>$this->tr->trans('Cost'),
                'empty_data'=>''
            ])
            ->add('type',Type\ChoiceType::class,[
                'label'=>$this->tr->trans('Type'),
                'placeholder'=>$this->tr->trans('Choose Type'),
                'choices'=>[
                    'Income'=>'income',
                    'Expense'=>'expense'
                ],
                'empty_data'=>''
            ])
            ->add('monthly',Type\ChoiceType::class,[
                'label'=>$this->tr->trans('Recurrent'),
                'placeholder'=>$this->tr->trans('Is recurrent?'),
                'choices'=>[
                    'Yes'=>'1',
                    'No'=>'0'
                ],
                'help'=>$this->tr->trans('If yes will be always added no matter the date created'),
                'empty_data'=>''
            ])
            ->add('created_at',Type\TextType::class,[
                'label'=>$this->tr->trans('For date'),
                'mapped'=>false,
                'constraints'=>[
                    new Assert\Date([
                        "message"=>$this->tr->trans("Invalid date type")
                    ])
                ],
                'empty_data'=>null,
                'data'=>$isEdit?$options['data']->getCreatedAt()->format("Y-m-d"):""
                
            ])
            
        ;

       
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Finance::class,
        ]);
    }
}
