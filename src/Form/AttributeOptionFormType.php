<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type as Type;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\CallbackTransformer;
use App\Form\DataObjects\AttributeOption;
use App\Entity\Attribute;

class AttributeOptionFormType extends AbstractType
{
    private $tr;
    
    public function __construct($tr){
        $this->tr = $tr;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $isEdit = !empty($options['data']) && $options['data']->getCode()?$options['data']:null;

        $builder
            ->add('name',Type\TextType::class,[
                'label'=>$this->tr->trans('Option name')
            ])
            ->add('default',Type\ChoiceType::class,[
                'label'=>$this->tr->trans('Default selected'),
                'placeholder'=>$this->tr->trans('Is default selected?'),
                'choices'=>[
                    'Yes'=>'1',
                    'No'=>'0'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class'=>AttributeOption::class
        ]);
    }
}
