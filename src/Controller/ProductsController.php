<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use App\Entity\Product;
use App\Entity\Category;
use App\Repository\ProductRepository;
use App\Repository\CategoryRepository;
use App\Form\ProductFormType;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use App\Service\StockMovementHelper;
use App\Repository\WarehouseProductRepository;
use App\Repository\WarehouseRepository;
use App\Entity\Warehouse;
use App\Entity\WarehouseProduct;
use App\Form\StockMovementFormType;
use App\Form\DataObjects\StockMovement;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use AndreaSprega\Bundle\BreadcrumbBundle\Annotation\Breadcrumb;
use Symfony\Component\Validator\Constraints as Assert;
use App\Entity\Attribute;
use App\Entity\AttributeSet;
use App\Repository\AttributeRepository;
use App\Repository\AttributeSetRepository;
use App\Form\ProductAttributesFormType;
use App\Service\AttributesHelper;


/**
 * IsGranted('ROLE_ADMIN_PRODUCTS')
 * @Breadcrumb({"label" = "Products", "route" = "products" })
 */
class ProductsController extends AbstractController
{
    /**
     * @Route("/products/{page}", name="products", defaults={"page":"1"}, requirements={"page":"\d+"})
     */
    public function index(Request $request,
                          EntityManagerInterface $em,
                          ProductRepository $productRepo,
                          ValidatorInterface $validator,
                          $page
                         )
    {
        try{
            $filters = $this->getFiltersFromRequest($request,$validator);
        }catch(\Throwable $e){
            $filters = [];
        }
       
        $products = $productRepo->findAllPaginated($page,$this->getParameter('default_per_page'),false,$filters);
        //raw query
        // $conn = $em->getConnection();
        // $sql = "SELECT * FROM category_product";
        // $res = $conn->query($sql);
        // $res->execute();

        // dd($res->fetchAll());
        $uploadsDir = $this->getParameter('default_product_upload_dir');
        return $this->render('products/index.html.twig', [
            'page_title' => 'Products',
            'products'=>$products,
            'uploadsDir'=>$uploadsDir
        ]);
    }

     /**
     * @Route("/products/restore/{page}", name="products_restore", defaults={"page":"1"}, requirements={"page":"\d+"})
     */
    public function indexRestore(Request $request,
                          EntityManagerInterface $em,
                          ProductRepository $productRepo,
                          $page
                         )
    {
        $products = $productRepo->findAllPaginated($page,$this->getParameter('default_per_page'),true);
        //raw query
        // $conn = $em->getConnection();
        // $sql = "SELECT * FROM category_product";
        // $res = $conn->query($sql);
        // $res->execute();

        // dd($res->fetchAll());
        $uploadsDir = $this->getParameter('default_product_upload_dir');
        return $this->render('products/restore.html.twig', [
            'page_title' => 'Restore products',
            'products'=>$products,
            'uploadsDir'=>$uploadsDir
        ]);
    }


    /**
     * @Route("/products/add", name="products_add")
     */
    public function add(Request $request,
                        EntityManagerInterface $em,
                        $tr)
    {
        try{

            $product = new Product();
            $form = $this->createForm(ProductFormType::class,$product);
         
            $form->handleRequest($request);

            if($form->isSubmitted() && $form->isValid()){
                $file = $product->getMainImage();
              
                $productUploadPath = $this->getParameter("default_product_upload_dir");

                if($file instanceof UploadedFile){
                   $newName = md5(uniqid()).".".$file->guessExtension();
                   $file->move(
                    $productUploadPath,
                    $newName
                   );

                   $product->setMainImage($newName);
                }

                $em->persist($product);
                $em->flush();

                $this->addFlash("success",$tr->trans("Product saved"));
                return $this->redirectToRoute("products");
            }

            return $this->render('products/add.html.twig',[
                'page_title' => 'Add product',
                'product'=>$product,
                'form'=>$form->createView()
            ]);
               
        }catch(\Exception $e){
           
            $this->addFlash("error",$e->getMessage());

            return $this->redirectToRoute("products");
        }
        
    }

    /**
     * @Route("/products/edit/{id}", name="products_edit", requirements={"id":"\d+"})
     */
    public function edit(Request $request,
                        EntityManagerInterface $em,
                        Product $product,
                        AttributesHelper $attributesHelper,
                        AttributeRepository $attrRepo,
                        StockMovementHelper $stockHelper,
                        $tr
                        )
    {
        try{

            $form = $this->createForm(ProductFormType::class,$product);
            $imageName = $product->getMainImage();
            $form->handleRequest($request);

            $attributesHelper->setProduct($product);
            $extraAttrForm = $this->createForm(ProductAttributesFormType::class,$attributesHelper);

            if($form->isSubmitted() && $form->isValid()){
                $stockHelper->setProduct($product);
                $stockData = $stockHelper->getQtyPerWarehouseData();
               if($product->getQty() < $stockData['total_in_warehouse']){

                $this->addFlash("error",$tr->trans("Qty per product cannot be lower than total in warehouses ({$stockData['total_in_warehouse']}).Please remove from warehouses first."));
                return $this->redirectToRoute("products_edit",[
                    "id"=>$product->getId()
                ]);

               }
                $file = $product->getMainImage();
                $newName = $imageName;
                $productUploadPath = $this->getParameter("default_product_upload_dir");

                if($file instanceof UploadedFile){
                   $newName = md5(uniqid()).".".$file->guessExtension();
                   $file->move(
                    $productUploadPath,
                    $newName
                   );

                   
                }

                $extraAttrData = $request->request->get('product_attributes_form',[]);
                foreach($extraAttrData as $attrCode=>$extraAttr){
                    
                    if(!empty($extraAttr)){
                       
                        $attr = $attrRepo->findFirstByCode($attrCode);
                        
                        if($attr){
                            $insertData = [
                                "plain"=>"",
                                "selected"=>[

                                ]
                            ];
                            if($attr->isDropdown()){
                                if(is_array($extraAttr)){
                                    $insertData['selected'] = $extraAttr;
                                }else{
                                    $insertData['selected'][] = $extraAttr;
                                }
                            }else{
                                $insertData['plain'] = preg_replace('/[^\w-\/\. ]+/','',$extraAttr);
                            }  
                            
                            $attributesHelper->saveAttributeData($attr,$insertData);
                        }
                        
                    }
                }

                $product->setMainImage($newName);
                $em->persist($product);
                $em->flush();

                $this->addFlash("success",$tr->trans("Product edited"));
                return $this->redirectToRoute("products_edit",[
                    "id"=>$product->getId()
                ]);
            }

            $stockMovementAjax = $this->createForm(StockMovementFormType::class);
           

            return $this->render('products/edit.html.twig',[
                'page_title' => 'Edit product',
                'product'=>$product,
                'form'=>$form->createView(),
                'stockMovementForm'=>$stockMovementAjax->createView(),
                'extraAttrForm'=>$extraAttrForm->createView()
            ]);
               
        }catch(\Exception $e){
          
            $this->addFlash("error",$e->getMessage());

            return $this->redirectToRoute("products");
        }
    }

    /**
     * @Route("/products/delete/{id}", name="products_delete", requirements={"id":"\d+"})
     */
    public function delete(Request $request,
                        EntityManagerInterface $em,
                        Product $product,
                        Filesystem $filesystem,
                        $tr
                        )
    {
        try{
            
            // $uploadsDir = $this->getParameter('default_product_upload_dir');
            // $filePath = null;
          
            // if($product->getMainImage()){
            //     $filePath = $uploadsDir.DIRECTORY_SEPARATOR.$product->getMainImage();
               
            // }
            
            // $em->remove($product);
            // $em->flush();

            // if($filesystem->exists($filePath)){
            //     $filesystem->remove($filePath);
            // }

            $product->setDeletedAt(new \Datetime());
            $em->flush();

            $this->addFlash("success",$tr->trans("Product deleted"));
            return $this->redirectToRoute('products');
        }catch(\Exception $e){
            $this->addFlash("error",$e->getMessage());

            return $this->redirectToRoute("products");
        }
        
    }

     /**
     * @Route("/products/restore/item/{id}", name="products_item_restore", requirements={"id":"\d+"})
     * @Breadcrumb({"label" = "Restore" })
     */
    public function restore(Request $request,
                        EntityManagerInterface $em,
                        Product $product,
                        Filesystem $filesystem,
                        $tr
                        )
    {
        try{
            
            $product->setDeletedAt(null);
            $em->flush();

            $this->addFlash("success",$tr->trans("Product restored"));
            return $this->redirectToRoute('products');
        }catch(\Exception $e){
            $this->addFlash("error",$e->getMessage());

            return $this->redirectToRoute("products");
        }
        
    }

    /**
     * @Route("/products/getWarehouseStocks/{id}", name="products_warehouse_stocks")
     */
    public function getStocks(Request $request,
                                EntityManagerInterface $em,
                                Product $product,
                                ProductRepository $productRepository,
                                StockMovementHelper $helper,
                                $tr){

        try{
            if(!$request->isXmlHttpRequest()){
                throw new \Exception($tr->trans("Invalid request"));
            } 

            $helper->setProduct($product);
            $stocksData = $helper->getStocksData();
           
            $html = $this->renderView('products/ajax/_stock_table.html.twig',[
                'stocks'=>$stocksData
            ]);

            return $this->json([
                "success"=>true,
                "html"=>$html
            ]);
        }catch(\Exception $e){
            return $this->json([
                'success'=>false,
                'html'=>""
                
            ]);
        }                            
         
        
        

    }

    /**
     * @Route("/products/stocksRestore/{id}", name="products_restore_stocks")
     */
    public function restoreStocks(Request $request,
                                EntityManagerInterface $em,
                                WarehouseProduct $wp,
                                ProductRepository $productRepository,
                                StockMovementHelper $helper,
                                $tr){

        try{
            if(!$request->isXmlHttpRequest()){
               // throw new \Exception("Invalid request");
            } 

           $product = $wp->getProduct();
           $warehouse = $wp->getWarehouse();

           if(!$product){
               throw new \Exception($tr->trans("Invalid product"));
           }

           if(!$warehouse){
               throw new \Exception($tr->trans("Invalid warehouse"));
           }

           $helper->setProduct($product);
           $helper->deleteStockFromWarehouse($warehouse);

            return $this->json([
                "success"=>true,
                "message"=>$tr->trans("Stocks restored")
            ]);
        }catch(\Exception $e){
            return $this->json([
                'success'=>false,
                'message'=>$e->getMessage()
                
            ]);
        }                            
         
        
        

    }

    /**
     * @Route("/products/moveStock/{id}", name="products_move_stock")
     */
    public function moveStock(Request $request,
                              EntityManagerInterface $em,
                              Product $product,
                              ProductRepository $productRepository,
                              WarehouseRepository $warehouseRepository,
                              StockMovementHelper $helper,
                              ValidatorInterface $validator,
                              $tr
                              ){
           try{
                if(!$request->isXmlHttpRequest()){
                    throw new \Exception($tr->trans("Invalid request"));
                } 

                $stockMovementObj = new StockMovement();
                $form = $this->createForm(StockMovementFormType::class,$stockMovementObj);
                $form->handleRequest($request);
                $errorMessages = [];
                if(!$form->isValid()){
                    $errors = $validator->validate($stockMovementObj);
                    if($errors->count() > 0){
                        foreach($errors as $error){
                           $errorMessages[] = $error->getMessage();
                        }
                    }
                }

                if(!empty($errorMessages)){
                    throw new \Exception(implode(".",$errorMessages));
                }
               
               $fromWarehouse = (int) $request->request->get("stock_movement_form")["fromWarehouse"] ?? 0;
               $toWarehouse = (int) $request->request->get("stock_movement_form")["toWarehouse"] ?? 0;
               $qty = (int) $request->request->get("stock_movement_form")["qty"] ?? 0;
                
               $fromWarehouse = $warehouseRepository->find($fromWarehouse);
               $toWarehouse = $warehouseRepository->find($toWarehouse);

               $helper->setProduct($product);
   
               $helper->moveStock($fromWarehouse,$toWarehouse,$qty);

                // $warehouse1 = $warehouseRepository->find(3);
                // $warehouse2 = $warehouseRepository->find(3);

                // $helper->setProduct($product);
                // $helper->moveStock($warehouse1,$warehouse2,1);

                return $this->json([
                    "success"=>true,
                    "message"=>$tr->trans("Stock moved")
                ]);
           }catch(\Exception $e){
              
                return $this->json([
                    'success'=>false,
                    'message'=>$e->getMessage()
                    
                ]);
           }                     
          //$warehouseIds = $warehouseRepository->getAllValidIds();
        //    $warehouse1 = $warehouseRepository->find(3);
        //    $warehouse2 = $warehouseRepository->find(5);
         
        //  $wp1 = new WarehouseProduct();

        //  $wp1->setStock(2);
        //  $wp1->setWarehouse($warehouse1);
        //  $wp1->setProduct($product);

        //  $wp2 = new WarehouseProduct();

        //  $wp2->setStock(2);
        //  $wp2->setWarehouse($warehouse2);
        //  $wp2->setProduct($product);

        //  $em->persist($wp1);
        //  $em->persist($wp2);

        //  $em->flush();

        // $res = $productRepository->getWarehouseProductOrNew($product,$warehouse1);
      
        // dd($res);

     
    
    }

    /**
     * @Route("/products/ajax", name="products_ajax")
     */
    public function ajax(Request $request,
                          EntityManagerInterface $em,
                          ProductRepository $productRepo,
                          $tr
                          )
    {
        $resp_data = [
            'results'=>[

            ],
            'more'=>false
        ];
        
        $page = $request->query->get("page",1);
        $pageLimit = $request->query->get("page_limit",$this->getParameter('default_per_page'));
        $q = $request->query->get("q",'');
        $products = $productRepo->ajaxProducts($page,$pageLimit,$q);
      
        $totalPages = $products->getPageCount();
        $currentPage = $products->getCurrentPageNumber();

        $more = ((int) $currentPage) < $totalPages;

        foreach($products as $product){
            $resp_data['results'][]=[
                'id'=>$product->getId(),
                'text'=>$product->getTitle()
            ];
            $resp_data['more'] = $more;
        }
        return $this->json($resp_data);
    }

     /**
     * @Route("/products/filtersAjax", name="products_filters_ajax")
     */
    public function filtersAjax(Request $request,
                                ValidatorInterface $validator
                                ){
                                
        try{
            $data['success'] = true;
            $data['url'] = "";

            $filters = $this->getFiltersFromRequest($request,$validator);
           
            $filters = array_filter($filters,function($elem){
                return !is_null($elem) && $elem !== "";
            });

            $data['url'] = http_build_query($filters);
            
        }catch(\Exception $e){
            $data['success'] = false;
            $data['url'] = "";
        }                            
        

        return $this->json($data);
    }

    private function getFiltersFromRequest(Request $request,ValidatorInterface $validator){

        $filters = [
            "q"=>$request->query->get("q",null),
            "stock"=>$request->query->get("stock",null),
            "date"=>$request->query->get("date",null),
            "e_date"=>$request->query->get("e_date",null),
        ];

        $rules = [];
        $rules['q'] = [
            new Assert\Length([
                "min"=>2,
                "max"=>200
            ])
        ];
        $rules['stock'] = [
            new Assert\Type([
                "type"=>"numeric"
            ]),
            new Assert\Choice([
                "choices"=>["0","1"]
            ])
        ];
        $rules['date'] = [
            new Assert\DateTime([
                "format"=>"Y-m-d"
            ])
        ];
        $rules['e_date'] = [
            new Assert\DateTime([
                "format"=>"Y-m-d"
            ])
        ];
        $constraint = new Assert\Collection($rules);
        $violations = $validator->validate($filters,$constraint);
        if($violations->count() > 0){
            foreach($violations as $violation){
               $key = trim(str_replace(["[","]"],["",""],$violation->getPropertyPath()));
               if(isset($filters[$key])){
                   $filters[$key] = null;
               }
            }
        }

        if(empty($filters['date']) || empty($filters['e_date'])){
            $filters['date'] = null;
            $filters['e_date'] = null;
        }

        return $filters;
    }
}
