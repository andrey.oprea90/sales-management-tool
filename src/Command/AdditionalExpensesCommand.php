<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use Carbon\Carbon;
use App\Entity\Finance;

class AdditionalExpensesCommand extends Command
{
    protected static $defaultName = 'populate:additional-expenses';

    public function __construct(EntityManagerInterface $em){
        $this->em = $em;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Populates the additional expenses with random values')
            
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->note('Starting additional income/expenses creation process');
        $this->createFinance($output,1000);
        $io->success('Additional income/expenses creation process finished');
    }

    public function createFinance(OutputInterface $output,int $total):bool{
        $progressBar = new ProgressBar($output,$total);
        $progressBar->start();
        $days = range(1,360);

        
        try{
            for($i = 0;$i < $total;$i++){
                $day = array_rand($days);
                $date = new \DateTime("2019-01-01");
                $date->add(new \DateInterval("P{$day}D"));
                $name = $i & 1?"Expense ".$this->readableRandomString():"Income ".$this->readableRandomString();
                $type = $i & 1?"expense":"income";
                $finance = new Finance();
                $finance->setName($name);
                $finance->setCost((2 + rand(1,20)));
                $finance->setType($type);
                $finance->setMonthly(false);
                $finance->setCreatedAt($date);
                
                $this->em->persist($finance);
                $this->em->flush();
                $progressBar->advance();
            }
        }catch(\Exception $e){
           dd($e->getMessage());
            return false;
        }
       
        $progressBar->finish();
        return true;
    }

    private function readableRandomString($length = 6)
    {  
        $string     = '';
        $vowels     = array("a","e","i","o","u");  
        $consonants = array(
            'b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 
            'n', 'p', 'r', 's', 't', 'v', 'w', 'x', 'y', 'z'
        );  
        // Seed it
        srand((double) microtime() * 1000000);
        $max = $length/2;
        for ($i = 1; $i <= $max; $i++)
        {
            $string .= $consonants[rand(0,19)];
            $string .= $vowels[rand(0,4)];
        }
        return $string;
    }
}
