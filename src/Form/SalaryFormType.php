<?php

namespace App\Form;

use App\Entity\Salary;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type as Type;
use Symfony\Component\Validator\Constraints as Assert;

class SalaryFormType extends AbstractType
{
    private $tr;
    
    public function __construct($tr){
        $this->tr = $tr;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('date_created',Type\TextType::class,[
                'label'=>$this->tr->trans('Paid on'),
                'mapped'=>false,
                'constraints'=>[
                    new Assert\NotBlank([
                        "message"=>$this->tr->trans("Please select a date")
                    ]),
                    new Assert\Date([
                        "message"=>$this->tr->trans("Invalid date type")
                    ])
                ],
                'empty_data'=>null
                
                
            ])
            ->add('hours_worked',Type\TextType::class,[
                'label'=>$this->tr->trans('Worked hours')
            ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Salary::class,
        ]);
    }
}
