<?php

namespace App\Repository;

use App\Entity\Finance;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Knp\Component\Pager\PaginatorInterface;
use Carbon\Carbon;

/**
 * @method Finance|null find($id, $lockMode = null, $lockVersion = null)
 * @method Finance|null findOneBy(array $criteria, array $orderBy = null)
 * @method Finance[]    findAll()
 * @method Finance[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FinanceRepository extends ServiceEntityRepository
{
    private $paginator;
    
    public function __construct(RegistryInterface $registry, PaginatorInterface $paginator)
    {
        $this->paginator = $paginator;
        parent::__construct($registry, Finance::class);
    }

    // /**
    //  * @return Finance[] Returns an array of Finance objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Finance
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function findAllPaginated(int $page = 1, int $perPage = 10,array $filters = []){

        $filters = array_filter(array_merge([
            "name"=>null,
            "type"=>null,
            "recurrent"=>null,
            "s_date"=>null,
            "e_date"=>null
        ],$filters),function($k){
            return in_array($k,["name","type","recurrent","s_date","e_date"]);
        },ARRAY_FILTER_USE_KEY);

        $query = $this->createQueryBuilder('f');
        if(!is_null($filters['name'])){
            $query->andWhere('f.name LIKE :name');
            $query->setParameter('name',"%".$filters['name']."%");
        }
        if(!is_null($filters['type'])){
            $query->andWhere('f.type = :type');
            $query->setParameter('type',$filters['type']);
        }
        if(!is_null($filters['recurrent'])){
            $query->andWhere('f.monthly = :recurrent');
            $query->setParameter('recurrent',$filters['recurrent']);
        }
        if(!is_null($filters['s_date']) && !is_null($filters['e_date'])){
            $query->andWhere('f.created_at BETWEEN :s_date AND :e_date');
            $query->setParameter('s_date',Carbon::parse($filters['s_date'])->startOfDay()->format("Y-m-d H:i"));
            $query->setParameter('e_date',Carbon::parse($filters['e_date'])->endOfDay()->format("Y-m-d H:i"));
        } 
        $query->orderBy("f.created_at","DESC")
              ->getQuery();

        return $this->paginator->paginate($query,$page,$perPage);

    }

    public function findAllByDateInterval(Carbon $start, Carbon $end){
        $qb = $this->createQueryBuilder('f');
        return $qb->andWhere('(f.created_at BETWEEN :start AND :end AND f.monthly <> 1) OR (f.monthly <> 0)')
                    ->setParameters([
                        "start"=>$start->format("Y-m-d H:i"),
                        "end"=>$end->format("Y-m-d H:i")
                    ])
                    ->getQuery()
                    ->getResult();

    }
}
