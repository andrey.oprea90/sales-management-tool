function salesPerWarehouse(elemClass){

    var append_elem = document.querySelector("#"+elemClass);

    if(!append_elem){
        return;
    }
    $(append_elem).html("");
    var sp = spinner($(append_elem).attr("id"));

    var url = append_elem.getAttribute("data-url");
    if(!url){
        return;
    }
    var token = append_elem.getAttribute("data-tok");
    if(!token){
        return;
    }

    var start =  append_elem.getAttribute("data-start");
    var end =  append_elem.getAttribute("data-end");

    if(!start || !end){
        return;
    }

    var headers = {
        'Content-Type': 'application/json',
       //  'Content-Type': 'application/x-www-form-urlencoded',
       'Authorization': 'Bearer '+token
         
    };
    data={
        start:start,
        end:end
    };
    return fetch(url,{
        method:"POST",
        headers: headers,
        body:JSON.stringify(data)
       
       
    }).then(function(data){
        
        return data.json();
    }).then(function(response){
          
      if(response.length){
        Morris.Bar({
            element: 'hero-bar',
            data: response,
            xkey: 'warehouse',
            ykeys: ['totals'],
            labels: ['Totals'],
            barRatio: 0.4,
            xLabelMargin: 10,
            hideHover: 'auto',
            barColors: ["#3d88ba"]
        });
      }
    }).finally(function(){
        try{
            sp.stop();
        }catch(e){

        }
        
    });
  }
  function profitPerPeriod(elemClass){

    var append_elem = document.querySelector("#"+elemClass);

    if(!append_elem){
        return;
    }

    $(append_elem).html("");
    var sp = spinner($(append_elem).attr("id"));

    var url = append_elem.getAttribute("data-url");
    if(!url){
        return;
    }
    var token = append_elem.getAttribute("data-tok");
    if(!token){
        return;
    }

    var start =  append_elem.getAttribute("data-start");
    var end =  append_elem.getAttribute("data-end");
    var mode =  append_elem.getAttribute("data-mode");

    if(!start || !end){
        return;
    }

    var headers = {
        'Content-Type': 'application/json',
       //  'Content-Type': 'application/x-www-form-urlencoded',
       'Authorization': 'Bearer '+token
         
    };
    data={
        start:start,
        end:end,
        mode:mode
    };
    return fetch(url,{
        method:"POST",
        headers: headers,
        body:JSON.stringify(data)
       
       
    }).then(function(data){
        
        return data.json();
    }).then(function(response){
          
      if(response.length){
        Morris.Line({
            element: elemClass,
            data: response,
            xkey: 'period',
            xLabels: 'month',
            ykeys: ['total_sell', 'total_buy'],
            labels: ['Sell price', 'Buy price']
        });
      }
    }).finally(function(){
        try{
            sp.stop();
        }catch(e){

        }
        
    });
  }

  function salesPerPeriod(elemClass){

    var append_elem = document.querySelector("#"+elemClass);
    
    if(!append_elem){
        return;
    }

    $(append_elem).html("");
    var sp = spinner($(append_elem).attr("id"));

    var url = append_elem.getAttribute("data-url");
    if(!url){
        return;
    }
    var token = append_elem.getAttribute("data-tok");
    if(!token){
        return;
    }
  
    var start =  append_elem.getAttribute("data-start");
    var end =  append_elem.getAttribute("data-end");
   

    if(!start || !end){
        return;
    }

    var headers = {
        'Content-Type': 'application/json',
       //  'Content-Type': 'application/x-www-form-urlencoded',
       'Authorization': 'Bearer '+token
         
    };
    data={
        start:start,
        end:end
    };
    return fetch(url,{
        method:"POST",
        headers: headers,
        body:JSON.stringify(data)
       
       
    }).then(function(data){
        
        return data.json();
    }).then(function(response){
          
      if(response.length){
        $.plot("#"+elemClass, [ response ], {
            series: {
                bars: {
                    show: true,
                    barWidth: 0.6,
                    align: "center"
                }
            },
            xaxis: {
                mode: "categories",
                tickLength: 0
            }
        });
      }
    }).finally(function(){
        try{
            sp.stop();
        }catch(e){

        }
        
    });
  }

  function incomeVsExpense(elemClass,totalsClass){

    $(".income-expense-table").hide();
    var append_elem = document.querySelector("#"+elemClass);
    
    if(!append_elem){
        return;
    }
    
    $(append_elem).html("");
    var sp = spinner($(append_elem).attr("id"));
    
    var totals_elem = document.querySelector("#"+totalsClass);
    
    if(!totals_elem){
        return;
    }

    $(totals_elem).html("");
    var sp2 = spinner($(totals_elem).attr("id"));

    var url = append_elem.getAttribute("data-url");
    if(!url){
        return;
    }
    var token = append_elem.getAttribute("data-tok");
    if(!token){
        return;
    }
  
    var start =  append_elem.getAttribute("data-start");
    var end =  append_elem.getAttribute("data-end");
   

    if(!start || !end){
        return;
    }

    $("#profit-table").css({"min-height":"200px"});
    var sp3 = spinner("profit-table");
    var include_stock = append_elem.getAttribute("data-include-stock")
   
    var headers = {
        'Content-Type': 'application/json',
       //  'Content-Type': 'application/x-www-form-urlencoded',
       'Authorization': 'Bearer '+token
         
    };
    data={
        start:start,
        end:end,
        include_stock:include_stock
    };
    return fetch(url,{
        method:"POST",
        headers: headers,
        body:JSON.stringify(data)
       
       
    }).then(function(data){
        
        return data.json();
    }).then(function(response){
        detailed_data = [
          
            response.income.sales,
            response.income.additional,
            response.expense.additional,
            response.expense.products,
            response.expense.salaries,
           // response.expense.products_in_stock,
            response.expense.taxes
        ];
        if(include_stock === "1"){
            detailed_data.push(response.expense.products_in_stock);          
        }
        total_data = [
            response.total_income,
            response.total_expense

        ];
        
        $.plot('#'+elemClass, detailed_data, {
            series: {
                pie: {
                    show: true,
                    radius: 1,
                    tilt: 0.5,
                    label: {
                        show: true,
                        radius: 1,
                        formatter: labelFormatter,
                        background: {
                            opacity: 0.8
                        }
                    },
                    
                }
            },
            legend: {
                show: true
            }
        });
      
        $.plot('#'+totalsClass, total_data, {
            series: {
                pie: { 
                    show: true,
                    radius: 1,
                    label: {
                        show: true,
                        radius: 3/4,
                        formatter: labelFormatter,
                        background: { 
                            opacity: 0.5,
                            color: '#000'
                        }
                    }
                }
            },
            legend: {
                show: true
            }
        });

        $(".income-expense-table .sales-income-label").html(response.income.sales.name);
        $(".income-expense-table .sales-income-data").html(response.income.sales.data_formatted);
        $(".income-expense-table .additional-income-label").html(response.income.additional.name);
        $(".income-expense-table .additional-income-data").html(response.income.additional.data_formatted);

        $(".income-expense-table .additional-expense-label").html(response.expense.additional.name);
        $(".income-expense-table .additional-expense-data").html(response.expense.additional.data_formatted);
        $(".income-expense-table .products-expense-label").html(response.expense.products.name);
        $(".income-expense-table .products-expense-data").html(response.expense.products.data_formatted);
        $(".income-expense-table .taxes-expense-label").html(response.expense.taxes.name);
        $(".income-expense-table .taxes-expense-data").html(response.expense.taxes.data_formatted);
        $(".income-expense-table .salaries-expense-label").html(response.expense.salaries.name);
        $(".income-expense-table .salaries-expense-data").html(response.expense.salaries.data_formatted);

        if(include_stock === "1"){
            $(".income-expense-table .products-stock").show();
            $(".income-expense-table .products-stock-expense-label").html(response.expense.products_in_stock.name);
            $(".income-expense-table .products-stock-expense-data").html(response.expense.products_in_stock.data);     
        }else{
            $(".income-expense-table .products-stock").hide();
            $(".income-expense-table .products-stock-expense-label").html("");
            $(".income-expense-table .products-stock-expense-data").html(""); 
        }

        $(".income-expense-table .total-income-data").html(response.total_income.data_formatted);
        $(".income-expense-table .total-expense-data").html(response.total_expense.data_formatted);

        $(".income-expense-table .profit-data").html(response.profit.data_formatted);
    }).finally(function(){
        $(".income-expense-table").show();
        $("#profit-table").removeAttr("style");
        try{
            sp.stop();
            sp2.stop();
            sp3.stop();
        }catch(e){

        }
        
    });
  }

  function additionalIncomeExpensePerPeriod(elemClass){

    var append_elem = document.querySelector("#"+elemClass);

    if(!append_elem){
        return;
    }

    $(append_elem).html("");
    var sp = spinner($(append_elem).attr("id"));

    var tbody = $(".additional-income-expense-table tbody");
    tbody.html("");
    var sp2 = spinner(tbody.attr("id"));

    var url = append_elem.getAttribute("data-url");
    if(!url){
        return;
    }
    var token = append_elem.getAttribute("data-tok");
    if(!token){
        return;
    }

    var start =  append_elem.getAttribute("data-start");
    var end =  append_elem.getAttribute("data-end");
    var mode =  append_elem.getAttribute("data-mode");

    if(!start || !end){
        return;
    }

    var headers = {
        'Content-Type': 'application/json',
       //  'Content-Type': 'application/x-www-form-urlencoded',
       'Authorization': 'Bearer '+token
         
    };
    data={
        start:start,
        end:end,
        mode:mode
    };
    return fetch(url,{
        method:"POST",
        headers: headers,
        body:JSON.stringify(data)
       
       
    }).then(function(data){
        
        return data.json();
    }).then(function(response){
          
      if(response.result.length){
        Morris.Line({
            element: elemClass,
            data: response.result,
            xkey: 'period',
            xLabels: 'day',
            ykeys: ['total_income', 'total_expense'],
            labels: ['Income', 'Expense']
        });
      }

      
      if(response.detailed.length){
        for(var i = 0;i < response.detailed.length;i++){
            var tr = $("<tr>");
            response.detailed[i].forEach((elem)=>{
                if(elem.type == "income"){
                    var html = "<td class='income-item'>"+elem.name+"</td>";
                    html+= "<td class='income-item'>"+elem.cost+"</td>";
                    tr.prepend(html);
                }else{
                    var html = "<td class='expense-item'>"+elem.name+"</td>";
                    html+= "<td class='expense-item'>"+elem.cost+"</td>";
                    tr.append(html);
                }
            });
            tbody.append(tr);
        }
        
      }
     
        $(".additional-income-expense-table .total-additional-income-data").html(response.totals.total_income);
        $(".additional-income-expense-table .total-additional-expense-data").html(response.totals.total_expense);
   
    }).finally(function(){
        try{
            sp.stop();
            sp2.stop();
        }catch(e){

        }
        
    });
  }


// Morris Bar Chart

salesPerWarehouse("hero-bar");

 profitPerPeriod("hero-graph");

 salesPerPeriod("catchart");

 incomeVsExpense("piechart2","piechart1");

 additionalIncomeExpensePerPeriod('income-expense-graph');


 $('.start_date').datepicker({
     dateFormat:"dd.mm.yy",
     onSelect: function(date,datepicker){
       
        var startDate = moment(date,"DD.MM.YYYY");
        startDateFormatted = startDate.format("YYYY-MM-DD");
        if($(this).parent().parent().find(".end_date").val()){
            var endDate = moment($(this).parent().parent().find(".end_date").val(),"DD.MM.YYYY");
            var endDateFormatted = endDate.format("YYYY-MM-DD");
            if(startDate <= endDate){
               startDateFormatted+=" 00:00";
               endDateFormatted+=" 23:59";

               $(this).parent().parent().parent().parent().find(".custom-chart").attr("data-start",startDateFormatted);
               $(this).parent().parent().parent().parent().find(".custom-chart").attr("data-end",endDateFormatted);

               var id =  $(this).parent().parent().parent().parent().find(".custom-chart").attr("id");
              
               $("#"+id).html("");
              
               switch(id){
                   case "hero-bar":
                       salesPerWarehouse("hero-bar");
                       break;
                    case "hero-graph":
                        profitPerPeriod("hero-graph");
                        break;
                    case "catchart":
                        salesPerPeriod("catchart");
                        break;
                    case "piechart2":
                        incomeVsExpense("piechart2","piechart1");
                        break;

               }
              
            }else{
                var errorMessage = 'Start date must be lower than end date';
                $(this).parent().parent().parent().parent().find(".messages").fadeIn().html("<div class='alert alert-info'>"+errorMessage+"</div>").fadeOut(5000);
            }
        }
     }
 });

 

 $('.end_date').datepicker({
    dateFormat:"dd.mm.yy",
    onSelect: function(date,datepicker){
        var endDate = moment(date,"DD.MM.YYYY");
        var endDateFormatted = endDate.format("YYYY-MM-DD");
        if($(this).parent().parent().find(".start_date").val()){
            var startDate = moment($(this).parent().parent().find(".start_date").val(),"DD.MM.YYYY");
            var startDateFormatted = startDate.format("YYYY-MM-DD");
            if(startDate <= endDate){
                startDateFormatted+=" 00:00";
                endDateFormatted+=" 23:59";
                
                $(this).parent().parent().parent().parent().find(".custom-chart").attr("data-start",startDateFormatted);
                $(this).parent().parent().parent().parent().find(".custom-chart").attr("data-end",endDateFormatted);
                
                var id =  $(this).parent().parent().parent().parent().find(".custom-chart").attr("id");
               
                switch(id){
                    case "hero-bar":
                        salesPerWarehouse("hero-bar");
                        break;
                        case "hero-graph":
                            profitPerPeriod("hero-graph");
                            break;
                        case "catchart":
                            salesPerPeriod("catchart");
                            break;
                        case "piechart2":
                             incomeVsExpense("piechart2","piechart1");
                             break;
                }
            }else{
                var errorMessage = 'Start date must be lower than end date';
                $(this).parent().parent().parent().parent().find(".messages").fadeIn().html("<div class='alert alert-info'>"+errorMessage+"</div>").fadeOut(5000);
            }
        }
    }
});

$(".start_month").change(function(){
    var startMonth = $(this).val().trim();
    var startYear = $(this).parent().parent().find(".start_year").val().trim();

    if( (startMonth !="" && !isNaN(startMonth)) && (startYear !="" && !isNaN(startYear))){
        var yearMonth = startYear+"-"+startMonth;
        var sDate = moment(yearMonth,"YYYY-MM");
        var eDate = moment(sDate).endOf("month");

        if(sDate.isValid() && eDate.isValid()){
           var startDateFormatted = sDate.format("YYYY-MM-DD")+" "+"00:00";
           var endDateFormatted = eDate.format("YYYY-MM-DD")+" "+"23:59";
           
           $(this).parent().parent().parent().parent().find(".custom-chart").attr("data-start",startDateFormatted);
           $(this).parent().parent().parent().parent().find(".custom-chart").attr("data-end",endDateFormatted);

           additionalIncomeExpensePerPeriod('income-expense-graph');
        }
    }

});

$(".start_year").change(function(){
    var startYear = $(this).val().trim();
    var startMonth = $(this).parent().parent().find(".start_month").val().trim();

    if( (startMonth !="" && !isNaN(startMonth)) && (startYear !="" && !isNaN(startYear))){
        var yearMonth = startYear+"-"+startMonth;
        var sDate = moment(yearMonth,"YYYY-MM");
        var eDate = moment(sDate).endOf("month");

        if(sDate.isValid() && eDate.isValid()){
           var startDateFormatted = sDate.format("YYYY-MM-DD")+" "+"00:00";
           var endDateFormatted = eDate.format("YYYY-MM-DD")+" "+"23:59";
           
           $(this).parent().parent().parent().parent().find(".custom-chart").attr("data-start",startDateFormatted);
           $(this).parent().parent().parent().parent().find(".custom-chart").attr("data-end",endDateFormatted);

           additionalIncomeExpensePerPeriod('income-expense-graph');
        }
    }

});

$(".include_stock").change(function(){
    $(this).parent().parent().parent().parent().find(".custom-chart").attr("data-include-stock",$(this).val());
    incomeVsExpense("piechart2","piechart1");
});





function labelFormatter(label, series) {
    
    return "<div style='font-size:8pt; text-align:center; padding:2px; color:white;'>" + Math.round(series.percent) + "%</div>";
}