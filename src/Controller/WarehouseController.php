<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use App\Repository\WarehouseRepository;
use App\Entity\Warehouse;
use App\Form\WarehouseFormType;
use App\Repository\WarehouseProductRepository;
use App\Entity\Currency;
use AndreaSprega\Bundle\BreadcrumbBundle\Annotation\Breadcrumb;

/**
 * IsGranted("ROLE_ADMIN_WAREHOUSES")
 * @Breadcrumb({"label" = "Warehouses", "route" = "warehouse" })
 */
class WarehouseController extends AbstractController
{
    /**
     * @Route("/warehouse/{page}", name="warehouse", defaults={"page":"1"}, requirements={"page":"\d+"})
     */
    public function index(Request $request, 
                         WarehouseRepository $warehouseRepo,
                        $page
                        )
    {
        $warehouses = $warehouseRepo->findAllPaginated($page,$this->getParameter('default_per_page'));

        return $this->render('warehouse/index.html.twig', [
            'page_title' => 'Warehouses',
            'warehouses'=>$warehouses
        ]);
    }

    /**
     * @Route("/warehouse/restore/{page}", name="warehouse_restore", defaults={"page":"1"}, requirements={"page":"\d+"})
     */
    public function indexRestore(Request $request, 
                         WarehouseRepository $warehouseRepo,
                        $page
                        )
    {
        $warehouses = $warehouseRepo->findAllPaginated($page,$this->getParameter('default_per_page'),true);

        return $this->render('warehouse/restore.html.twig', [
            'page_title' => 'Restore warehouses',
            'warehouses'=>$warehouses
        ]);
    }

    

    /**
     * @Route("/warehouse/add", name="warehouse_add")
     */
    public function add(Request $request,
                        EntityManagerInterface $em,
                        $tr){

        try{
            $warehouse = new Warehouse();
            $form = $this->createForm(WarehouseFormType::class,$warehouse);

            $form->handleRequest($request);

            if($form->isSubmitted() && $form->isValid()){

                $em->persist($warehouse);
                $em->flush();

                $this->addFlash("success",$tr->trans("Warehouse saved"));
                return $this->redirectToRoute("warehouse");
            }
        }catch(\Exception $e){
           
            $this->addFlash("error",$e->getMessage());
            return $this->redirectToRoute("warehouse");
        }
        

        return $this->render('warehouse/add.html.twig',[
            'page_title'=>'Add warehouse',
            'form'=>$form->createView()
        ]);
    }

    /**
     * @Route("/warehouse/edit/{id}", name="warehouse_edit", requirements={"id":"\d+"})
     */
    public function edit(Request $request,
                        EntityManagerInterface $em,
                        Warehouse $warehouse,
                        $tr
                        ){

       try{
           
            $form = $this->createForm(WarehouseFormType::class,$warehouse);

            $form->handleRequest($request);

            if($form->isSubmitted() && $form->isValid()){

                $em->flush();

                $this->addFlash("success",$tr->trans("Warehouse edited"));
                
            }
        }catch(\Exception $e){
           
            $this->addFlash("error",$e->getMessage());
            return $this->redirectToRoute("warehouse");
        }
        

        return $this->render('warehouse/edit.html.twig',[
            'page_title'=>'Edit warehouse',
            'form'=>$form->createView(),
            'warehouse'=>$warehouse
        ]);       

    }

    /**
     * @Route("/warehouse/edit/{id}/products/{page}", name="warehouse_products", defaults={"page":"1"}, requirements={"page":"\d+","id":"\d+"})
     */
    public function warehouseProducts(Request $request,
                                      EntityManagerInterface $em,
                                      Warehouse $warehouse,
                                      WarehouseProductRepository $wpRepo,
                                      WarehouseRepository $warehouseRepo,
                                      $page
                                      ){

           $warehouseWithProducts = $wpRepo->findAllPaginated($warehouse,$page,$this->getParameter("default_per_page"));
        
        
           return $this->render("warehouse/products.html.twig",[
               "page_title"=>"Warehouse products",
               "wproducts"=>$warehouseWithProducts,
               "warehouse"=>$warehouse
           ]);
    }

    /**
     * @Route("/warehouse/delete/{id}", name="warehouse_delete", requirements={"id":"\d+"})
     */
    public function delete(Request $request,
                           EntityManagerInterface $em,
                           Warehouse $warehouse,
                           $tr
                           ){

        try{
            
            //$em->remove($warehouse);
            $warehouse->setDeletedAt(new \DateTime());
            $em->flush();

            $this->addFlash("success",$tr->trans("Warehouse deleted"));
            return $this->redirectToRoute("warehouse");

        }catch(\Exception $e){

            $this->addFlash("error",$e->getMessage());
            return $this->redirectToRoute("warehouse");
        }          

    }

     /**
     * @Route("/warehouse/restore/item/{id}", name="warehouse_item_restore", requirements={"id":"\d+"})
     */
    public function restore(Request $request,
                           EntityManagerInterface $em,
                           Warehouse $warehouse,
                           $tr
                           ){

        try{
            
            $warehouse->setDeletedAt(null);
            $em->flush();

            $this->addFlash("success",$tr->trans("Warehouse restored"));
            return $this->redirectToRoute("warehouse");

        }catch(\Exception $e){

            $this->addFlash("error",$e->getMessage());
            return $this->redirectToRoute("warehouse");
        }          

    }
}
