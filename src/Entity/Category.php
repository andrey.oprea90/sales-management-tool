<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CategoryRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Category
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = 2,
     *      max = 200,
     *      minMessage = "Category name must be at least {{ limit }} characters long",
     *      maxMessage = "Category name cannot be longer than {{ limit }} characters"
     * )
     */
    private $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Assert\Length(
     *      min = 4,
     *      max = 2000,
     *      minMessage = "Category short description must be at least {{ limit }} characters long",
     *      maxMessage = "Category short description cannot be longer than {{ limit }} characters"
     * )
     */
    private $short_description;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $main_image;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Product", inversedBy="categories", cascade={"detach"})
     */
    private $products;

    public function __construct()
    {
        $this->products = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getShortDescription(): ?string
    {
        return $this->short_description;
    }

    public function setShortDescription(?string $short_description): self
    {
        $this->short_description = $short_description;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(?\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getMainImage() 
    {
        return $this->main_image;
    }

    public function setMainImage($main_image): self
    {
        $this->main_image = $main_image;

        return $this;
    }

    /**
     * @return Collection|Product[]
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        if ($this->products->contains($product)) {
            $this->products->removeElement($product);
        }

        return $this;
    }

     /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue(){

        $this->created_at = new \DateTime();
        
    }

    public function getImage(?string $checkPath,string $uploadsFolder = 'uploads/categories'):string{
         
        $assetPath = 'uploads/placeholder.jpg';
        if(!empty($this->main_image)){
            if($checkPath){
                if(is_file($checkPath.DIRECTORY_SEPARATOR.$this->main_image)){
                    return $uploadsFolder."/".$this->main_image;
                }
            }else{
                return $uploadsFolder."/".$this->main_image;
            }
        }
        return $assetPath;
    }
}
