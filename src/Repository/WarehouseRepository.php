<?php

namespace App\Repository;

use App\Entity\Warehouse;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @method Warehouse|null find($id, $lockMode = null, $lockVersion = null)
 * @method Warehouse|null findOneBy(array $criteria, array $orderBy = null)
 * @method Warehouse[]    findAll()
 * @method Warehouse[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WarehouseRepository extends ServiceEntityRepository
{
    private $paginator;
    
    public function __construct(RegistryInterface $registry,PaginatorInterface $paginator)
    {
        $this->paginator = $paginator;
        parent::__construct($registry, Warehouse::class);
    }

    // /**
    //  * @return Warehouse[] Returns an array of Warehouse objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('w.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Warehouse
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function findAllPaginated(int $page = 1, int $perPage = 10,$onlyTrashed = false){

        $query = $this->createQueryBuilder('w');
        if($onlyTrashed){
            $query->andWhere('w.deleted_at IS NOT NULL');
        }else{
            $query->andWhere('w.deleted_at IS NULL');
        }
        $query->orderBy("w.name","ASC")
              ->getQuery();

        return $this->paginator->paginate($query,$page,$perPage);

    }

    public function getAllValidIds(){
        $ids = [];
        $res =  $this->createQueryBuilder('w')
                    ->addSelect("w.id")
                    ->getQuery()
                    ->getArrayResult();
        foreach($res as $arr){
            $ids[] = $arr['id'];
        }
        return $ids;            
    }

    public function findOneOrNull():?Warehouse{
        return $this->createQueryBuilder('w')
                    ->setMaxResults(1)
                    ->getQuery()
                    ->getOneOrNullResult();
    }

    public function getWithProducts(Warehouse $warehouse){
        return $this->createQueryBuilder("w")
                    ->leftJoin("w.products","p")
                    ->leftJoin("p.product","pr")
                    ->andWhere("w.id = :warehouse")
                    ->setParameter("warehouse",$warehouse)
                    ->addSelect("p","pr")
                    ->getQuery()
                    ->getOneOrNullResult();
    }

    
}
