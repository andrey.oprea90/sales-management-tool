<?php

namespace App\Form;

use App\Entity\Employee;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type as Type;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints as Assert;
use App\Entity\User;
use App\Entity\Position;
use App\Entity\Department;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\EntityManagerInterface;

class EmployeeFormType extends AbstractType
{
    private $em;

    private $tr;
    
    public function __construct(EntityManagerInterface $em,$tr){

        $this->em = $em;
        $this->tr = $tr;
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
       $edit_employee = !empty($options['data']) && $options['data']->getId();
       
        $builder
            ->add("token",Type\HiddenType::class,[
                'data'=>md5(uniqid())
            ])
            ->add('first_name',Type\TextType::class,[
                'empty_data'=>'',
                'required'=>true,
                'label'=>$this->tr->trans('First name')
            ])
            ->add('middle_name',Type\TextType::class,[
                'empty_data'=>'',
                'required'=>false,
                'label'=>$this->tr->trans('Middle name')
            ])
            ->add('last_name',Type\TextType::class,[
                'empty_data'=>'',
                'required'=>true,
                'label'=>$this->tr->trans('Last name')
            ])
            ->add('email',Type\TextType::class,[
                'label'=>$this->tr->trans('Email'),
                'empty_data'=>''
            ])
            ->add('phone_number',Type\TextType::class,[
                'label'=>$this->tr->trans('Phone'),
                'empty_data'=>''
            ])
            ->add('identification',Type\TextType::class,[
                'label'=>$this->tr->trans('Identification'),
                'empty_data'=>''
            ]);
            if(!$edit_employee){
                $builder->add('user',EntityType::class,[
                    'class'=>User::class,
                    'choice_label'=>'username',
                    'placeholder'=>$this->tr->trans("Select user"),
                    'label'=>$this->tr->trans('Available users'),
                    'query_builder'=>function(EntityRepository $er){
                        $valid_ids = [];
                        $query = $this->em->createQuery('SELECT u.id AS user_id FROM App:User u 
                                                    LEFT JOIN App:Employee e WITH u.id = e.user WHERE e.id IS NULL');
                        
                        $res = $query->getArrayResult();
                        foreach($res as $r){
                            $valid_ids[] = $r['user_id'];
                        }
                        return $er->createQueryBuilder("u")
                                 ->andWhere("u.id IN (:valid_ids)")
                                 ->setParameter("valid_ids",$valid_ids);
                                 
                                  
                    }
                ]);
            }else{
                $builder->add('user',EntityType::class,[
                    'class'=>User::class,
                    'choice_label'=>'username',
                    'placeholder'=>$this->tr->trans("Select user"),
                    'label'=>$this->tr->trans('Available users'),
                    'disabled'=>true
                    
                ]);
            }
            $builder->add('position',EntityType::class,[
                'empty_data'=>[],
                'class'=>Position::class,
                'choice_label'=>'name',
                'placeholder'=>$this->tr->trans('Select position'),
                'label'=>$this->tr->trans('Position')
            ])
            ->add('departments',EntityType::class,[
                'class'=>Department::class,
                'choice_label'=>'name',
                'placeholder'=>$this->tr->trans('Select department'),
                'label'=>$this->tr->trans('Department'),
                'multiple'=>true,
                
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Employee::class,
        ]);
    }
}
