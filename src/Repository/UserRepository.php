<?php

namespace App\Repository;

use App\Entity\User;
use App\Entity\Files;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Doctrine\Common\Collections\Criteria;


/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    private $paginator;
    
    public function __construct(RegistryInterface $registry,
                                PaginatorInterface $paginator
                                )
    {
        $this->paginator = $paginator;
        parent::__construct($registry, User::class);
    }

    // /**
    //  * @return User[] Returns an array of User objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    public function getPaginatedUsers(int $page = 1,int $per_page = 10,User $loggedIn = null,bool $withTrashed = false){

        $query = $this->createQueryBuilder('u')
              ->orderBy('u.username','ASC');

        if(!$withTrashed){
            $query->andWhere('u.deleted_at IS NULL');
        }
        
        if($loggedIn){
            $query->andWhere('u.id <> :id');
            $query->setParameter("id",$loggedIn->getId());
        }

        $query->getQuery();

       return $this->paginator->paginate($query,$page,$per_page);
    }

    public function getTrashedPaginatedUsers(int $page = 1,int $per_page = 10){

        $query = $this->createQueryBuilder('u')
              ->orderBy('u.username','ASC')
              ->andWhere('u.deleted_at IS NOT NULL')
              ->getQuery();

       return $this->paginator->paginate($query,$page,$per_page);
    }

    public function getUserFiles(User $user){
        $em = $this->getEntityManager();
        $query = $em->createQuery('SELECT f FROM App:User u 
                                    INNER JOIN App:Files f WITH u.id = f.entity_id WHERE u.id = :id')
                    ->setParameter("id",$user->getId());
        
        return $query->getResult();
    }



    /*
    public function findOneBySomeField($value): ?User
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
