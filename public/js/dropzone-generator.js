Dropzone.autoDiscover = false;

$(document).ready(function(){

    initializeDropzone(function(file, data) {
        fetchFiles();
        
      },function(file, data) {
        if (data.detail) {
            this.emit('error', file, data.detail);
        }
      });

    fetchFiles();

  $(".js-append-files").on("click",".delete_file",function(e){
      e.preventDefault();
      var id = e.currentTarget.getAttribute("data-id");
      if(id){
          deleteFile(id);
      }
  }); 



    $(".div-table .div-tr").each(function(){
        var td = $(this).find(".div-td");
        var max = 0;
        td.each(function(){
            var height = $(this).height();
            max = height > max?height:max;
        });
        td.css({"height":max});
    });



});